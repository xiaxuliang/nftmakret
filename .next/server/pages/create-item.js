(function() {
var exports = {};
exports.id = "pages/create-item";
exports.ids = ["pages/create-item"];
exports.modules = {

/***/ "./config.js":
/*!*******************!*\
  !*** ./config.js ***!
  \*******************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "nftmarketaddress": function() { return /* binding */ nftmarketaddress; },
/* harmony export */   "nftaddress": function() { return /* binding */ nftaddress; },
/* harmony export */   "nftmarketabi": function() { return /* binding */ nftmarketabi; },
/* harmony export */   "nftabi": function() { return /* binding */ nftabi; }
/* harmony export */ });
const nftmarketaddress = "0x9106f0AF000AB0f838b025158e5832f58Fe3a35E"; //rinkeby

const nftaddress = "0xD81ab083e835FBe721DB6B5C7749dc72E2744e57"; //rinkeby

const nftmarketabi = [{
  "inputs": [],
  "stateMutability": "nonpayable",
  "type": "constructor"
}, {
  "anonymous": false,
  "inputs": [{
    "indexed": true,
    "internalType": "uint256",
    "name": "itemId",
    "type": "uint256"
  }, {
    "indexed": true,
    "internalType": "address",
    "name": "nftContract",
    "type": "address"
  }, {
    "indexed": true,
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }, {
    "indexed": false,
    "internalType": "address",
    "name": "seller",
    "type": "address"
  }, {
    "indexed": false,
    "internalType": "address",
    "name": "owner",
    "type": "address"
  }, {
    "indexed": false,
    "internalType": "uint256",
    "name": "price",
    "type": "uint256"
  }],
  "name": "MarketItemCreated",
  "type": "event"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "nftContract",
    "type": "address"
  }, {
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }, {
    "internalType": "uint256",
    "name": "price",
    "type": "uint256"
  }],
  "name": "createMarketItem",
  "outputs": [],
  "stateMutability": "payable",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "nftContract",
    "type": "address"
  }, {
    "internalType": "uint256",
    "name": "itemId",
    "type": "uint256"
  }],
  "name": "createMarketSale",
  "outputs": [],
  "stateMutability": "payable",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "uint256",
    "name": "itemId",
    "type": "uint256"
  }],
  "name": "fetchMarketItem",
  "outputs": [{
    "components": [{
      "internalType": "uint256",
      "name": "itemId",
      "type": "uint256"
    }, {
      "internalType": "address",
      "name": "nftContract",
      "type": "address"
    }, {
      "internalType": "uint256",
      "name": "tokenId",
      "type": "uint256"
    }, {
      "internalType": "address payable",
      "name": "seller",
      "type": "address"
    }, {
      "internalType": "address payable",
      "name": "owner",
      "type": "address"
    }, {
      "internalType": "uint256",
      "name": "price",
      "type": "uint256"
    }],
    "internalType": "struct NFTMarket.MarketItem",
    "name": "",
    "type": "tuple"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "fetchMarketItems",
  "outputs": [{
    "components": [{
      "internalType": "uint256",
      "name": "itemId",
      "type": "uint256"
    }, {
      "internalType": "address",
      "name": "nftContract",
      "type": "address"
    }, {
      "internalType": "uint256",
      "name": "tokenId",
      "type": "uint256"
    }, {
      "internalType": "address payable",
      "name": "seller",
      "type": "address"
    }, {
      "internalType": "address payable",
      "name": "owner",
      "type": "address"
    }, {
      "internalType": "uint256",
      "name": "price",
      "type": "uint256"
    }],
    "internalType": "struct NFTMarket.MarketItem[]",
    "name": "",
    "type": "tuple[]"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "fetchMyNFTs",
  "outputs": [{
    "components": [{
      "internalType": "uint256",
      "name": "itemId",
      "type": "uint256"
    }, {
      "internalType": "address",
      "name": "nftContract",
      "type": "address"
    }, {
      "internalType": "uint256",
      "name": "tokenId",
      "type": "uint256"
    }, {
      "internalType": "address payable",
      "name": "seller",
      "type": "address"
    }, {
      "internalType": "address payable",
      "name": "owner",
      "type": "address"
    }, {
      "internalType": "uint256",
      "name": "price",
      "type": "uint256"
    }],
    "internalType": "struct NFTMarket.MarketItem[]",
    "name": "",
    "type": "tuple[]"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "uint256",
    "name": "marketItemId",
    "type": "uint256"
  }],
  "name": "getMarketItem",
  "outputs": [{
    "components": [{
      "internalType": "uint256",
      "name": "itemId",
      "type": "uint256"
    }, {
      "internalType": "address",
      "name": "nftContract",
      "type": "address"
    }, {
      "internalType": "uint256",
      "name": "tokenId",
      "type": "uint256"
    }, {
      "internalType": "address payable",
      "name": "seller",
      "type": "address"
    }, {
      "internalType": "address payable",
      "name": "owner",
      "type": "address"
    }, {
      "internalType": "uint256",
      "name": "price",
      "type": "uint256"
    }],
    "internalType": "struct NFTMarket.MarketItem",
    "name": "",
    "type": "tuple"
  }],
  "stateMutability": "view",
  "type": "function"
}];
const nftabi = [{
  "inputs": [{
    "internalType": "address",
    "name": "marketplaceAddress",
    "type": "address"
  }],
  "stateMutability": "nonpayable",
  "type": "constructor"
}, {
  "anonymous": false,
  "inputs": [{
    "indexed": true,
    "internalType": "address",
    "name": "owner",
    "type": "address"
  }, {
    "indexed": true,
    "internalType": "address",
    "name": "approved",
    "type": "address"
  }, {
    "indexed": true,
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }],
  "name": "Approval",
  "type": "event"
}, {
  "anonymous": false,
  "inputs": [{
    "indexed": true,
    "internalType": "address",
    "name": "owner",
    "type": "address"
  }, {
    "indexed": true,
    "internalType": "address",
    "name": "operator",
    "type": "address"
  }, {
    "indexed": false,
    "internalType": "bool",
    "name": "approved",
    "type": "bool"
  }],
  "name": "ApprovalForAll",
  "type": "event"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "to",
    "type": "address"
  }, {
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }],
  "name": "approve",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "string",
    "name": "tokenURI",
    "type": "string"
  }],
  "name": "createToken",
  "outputs": [{
    "internalType": "uint256",
    "name": "",
    "type": "uint256"
  }],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "from",
    "type": "address"
  }, {
    "internalType": "address",
    "name": "to",
    "type": "address"
  }, {
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }],
  "name": "safeTransferFrom",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "from",
    "type": "address"
  }, {
    "internalType": "address",
    "name": "to",
    "type": "address"
  }, {
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }, {
    "internalType": "bytes",
    "name": "_data",
    "type": "bytes"
  }],
  "name": "safeTransferFrom",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "operator",
    "type": "address"
  }, {
    "internalType": "bool",
    "name": "approved",
    "type": "bool"
  }],
  "name": "setApprovalForAll",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "anonymous": false,
  "inputs": [{
    "indexed": true,
    "internalType": "address",
    "name": "from",
    "type": "address"
  }, {
    "indexed": true,
    "internalType": "address",
    "name": "to",
    "type": "address"
  }, {
    "indexed": true,
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }],
  "name": "Transfer",
  "type": "event"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "from",
    "type": "address"
  }, {
    "internalType": "address",
    "name": "to",
    "type": "address"
  }, {
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }],
  "name": "transferFrom",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "owner",
    "type": "address"
  }],
  "name": "balanceOf",
  "outputs": [{
    "internalType": "uint256",
    "name": "",
    "type": "uint256"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }],
  "name": "getApproved",
  "outputs": [{
    "internalType": "address",
    "name": "",
    "type": "address"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "owner",
    "type": "address"
  }, {
    "internalType": "address",
    "name": "operator",
    "type": "address"
  }],
  "name": "isApprovedForAll",
  "outputs": [{
    "internalType": "bool",
    "name": "",
    "type": "bool"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "name",
  "outputs": [{
    "internalType": "string",
    "name": "",
    "type": "string"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }],
  "name": "ownerOf",
  "outputs": [{
    "internalType": "address",
    "name": "",
    "type": "address"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "bytes4",
    "name": "interfaceId",
    "type": "bytes4"
  }],
  "name": "supportsInterface",
  "outputs": [{
    "internalType": "bool",
    "name": "",
    "type": "bool"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "symbol",
  "outputs": [{
    "internalType": "string",
    "name": "",
    "type": "string"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }],
  "name": "tokenURI",
  "outputs": [{
    "internalType": "string",
    "name": "",
    "type": "string"
  }],
  "stateMutability": "view",
  "type": "function"
}];

/***/ }),

/***/ "./pages/create-item.js":
/*!******************************!*\
  !*** ./pages/create-item.js ***!
  \******************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ Home; }
/* harmony export */ });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var ethers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ethers */ "ethers");
/* harmony import */ var ethers__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ethers__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var ipfs_http_client__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ipfs-http-client */ "ipfs-http-client");
/* harmony import */ var ipfs_http_client__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ipfs_http_client__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var web3modal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! web3modal */ "web3modal");
/* harmony import */ var web3modal__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(web3modal__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var web3__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! web3 */ "web3");
/* harmony import */ var web3__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(web3__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../config */ "./config.js");

var _jsxFileName = "E:\\marketplace-complete\\pages\\create-item.js";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







const client = (0,ipfs_http_client__WEBPACK_IMPORTED_MODULE_3__.create)('https://ipfs.infura.io:5001/api/v0');

function Home() {
  const {
    0: fileUrl,
    1: setFileUrl
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(null);
  const {
    0: formInput,
    1: updateFormInput
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)({
    price: '',
    name: '',
    description: ''
  });
  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_4__.useRouter)();

  async function createSale(url) {
    const web3Modal = new (web3modal__WEBPACK_IMPORTED_MODULE_5___default())({
      network: "mainnet",
      cacheProvider: true
    });
    const connection = await web3Modal.connect();
    const provider = new ethers__WEBPACK_IMPORTED_MODULE_2__.ethers.providers.Web3Provider(connection);
    const signer = provider.getSigner();
    let contract = new ethers__WEBPACK_IMPORTED_MODULE_2__.ethers.Contract(_config__WEBPACK_IMPORTED_MODULE_7__.nftaddress, _config__WEBPACK_IMPORTED_MODULE_7__.nftabi, signer);
    let transaction = await contract.createToken(url);
    let tx = await transaction.wait();
    let event = tx.events[0];
    let value = event.args[2];
    let tokenId = value.toNumber();
    const price = web3__WEBPACK_IMPORTED_MODULE_6___default().utils.toWei(formInput.price, 'ether');
    const listingPrice = web3__WEBPACK_IMPORTED_MODULE_6___default().utils.toWei('0.1', 'ether');
    contract = new ethers__WEBPACK_IMPORTED_MODULE_2__.ethers.Contract(_config__WEBPACK_IMPORTED_MODULE_7__.nftmarketaddress, _config__WEBPACK_IMPORTED_MODULE_7__.nftmarketabi, signer);
    transaction = await contract.createMarketItem(_config__WEBPACK_IMPORTED_MODULE_7__.nftaddress, tokenId, price, {
      value: listingPrice
    });
    await transaction.wait();
    router.push('/');
  }

  async function onChange(e) {
    const file = e.target.files[0];

    try {
      const added = await client.add(file, {
        progress: prog => console.log(`received: ${prog}`)
      });
      const url = `https://ipfs.infura.io/ipfs/${added.path}`;
      setFileUrl(url);
    } catch (error) {
      console.log('Error uploading file: ', error);
    }
  }

  async function createMarket() {
    const {
      name,
      description,
      price
    } = formInput;
    if (!name || !description || !price || !fileUrl) return; // first, upload to IPFS

    const data = JSON.stringify({
      name,
      description,
      image: fileUrl
    });

    try {
      const added = await client.add(data);
      const url = `https://ipfs.infura.io/ipfs/${added.path}`;
      createSale(url);
    } catch (error) {
      console.log('Error uploading file: ', error);
    }
  }

  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
    className: "flex justify-center",
    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      className: "w-1/2 flex flex-col pb-12",
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
        placeholder: "NFT Name",
        className: "mt-8 border rounded p-4",
        onChange: e => updateFormInput(_objectSpread(_objectSpread({}, formInput), {}, {
          name: e.target.value
        }))
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 79,
        columnNumber: 9
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
        placeholder: "NFT Description",
        className: "mt-2 border rounded p-4",
        onChange: e => updateFormInput(_objectSpread(_objectSpread({}, formInput), {}, {
          description: e.target.value
        }))
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 84,
        columnNumber: 9
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
        placeholder: "NFT Price in Eth",
        className: "mt-2 border rounded p-4",
        onChange: e => updateFormInput(_objectSpread(_objectSpread({}, formInput), {}, {
          price: e.target.value
        }))
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 89,
        columnNumber: 9
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
        type: "file",
        name: "NFT",
        className: "my-4",
        onChange: onChange
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 94,
        columnNumber: 9
      }, this), fileUrl && /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("img", {
        className: "rounded mt-4",
        width: "350",
        src: fileUrl
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 102,
        columnNumber: 13
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
        onClick: createMarket,
        className: "mt-4 bg-blue-500 text-white rounded p-4 shadow-lg",
        children: "Create NFT"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 105,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 78,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 77,
    columnNumber: 5
  }, this);
}

/***/ }),

/***/ "ethers":
/*!*************************!*\
  !*** external "ethers" ***!
  \*************************/
/***/ (function(module) {

"use strict";
module.exports = require("ethers");;

/***/ }),

/***/ "ipfs-http-client":
/*!***********************************!*\
  !*** external "ipfs-http-client" ***!
  \***********************************/
/***/ (function(module) {

"use strict";
module.exports = require("ipfs-http-client");;

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/***/ (function(module) {

"use strict";
module.exports = require("next/router");;

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ (function(module) {

"use strict";
module.exports = require("react");;

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ (function(module) {

"use strict";
module.exports = require("react/jsx-dev-runtime");;

/***/ }),

/***/ "web3":
/*!***********************!*\
  !*** external "web3" ***!
  \***********************/
/***/ (function(module) {

"use strict";
module.exports = require("web3");;

/***/ }),

/***/ "web3modal":
/*!****************************!*\
  !*** external "web3modal" ***!
  \****************************/
/***/ (function(module) {

"use strict";
module.exports = require("web3modal");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
var __webpack_exports__ = (__webpack_exec__("./pages/create-item.js"));
module.exports = __webpack_exports__;

})();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9tYXJrZXRwbGFjZS1jb21wbGV0ZS8uL2NvbmZpZy5qcyIsIndlYnBhY2s6Ly9tYXJrZXRwbGFjZS1jb21wbGV0ZS8uL3BhZ2VzL2NyZWF0ZS1pdGVtLmpzIiwid2VicGFjazovL21hcmtldHBsYWNlLWNvbXBsZXRlL2V4dGVybmFsIFwiZXRoZXJzXCIiLCJ3ZWJwYWNrOi8vbWFya2V0cGxhY2UtY29tcGxldGUvZXh0ZXJuYWwgXCJpcGZzLWh0dHAtY2xpZW50XCIiLCJ3ZWJwYWNrOi8vbWFya2V0cGxhY2UtY29tcGxldGUvZXh0ZXJuYWwgXCJuZXh0L3JvdXRlclwiIiwid2VicGFjazovL21hcmtldHBsYWNlLWNvbXBsZXRlL2V4dGVybmFsIFwicmVhY3RcIiIsIndlYnBhY2s6Ly9tYXJrZXRwbGFjZS1jb21wbGV0ZS9leHRlcm5hbCBcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiIiwid2VicGFjazovL21hcmtldHBsYWNlLWNvbXBsZXRlL2V4dGVybmFsIFwid2ViM1wiIiwid2VicGFjazovL21hcmtldHBsYWNlLWNvbXBsZXRlL2V4dGVybmFsIFwid2ViM21vZGFsXCIiXSwibmFtZXMiOlsibmZ0bWFya2V0YWRkcmVzcyIsIm5mdGFkZHJlc3MiLCJuZnRtYXJrZXRhYmkiLCJuZnRhYmkiLCJjbGllbnQiLCJpcGZzSHR0cENsaWVudCIsIkhvbWUiLCJmaWxlVXJsIiwic2V0RmlsZVVybCIsInVzZVN0YXRlIiwiZm9ybUlucHV0IiwidXBkYXRlRm9ybUlucHV0IiwicHJpY2UiLCJuYW1lIiwiZGVzY3JpcHRpb24iLCJyb3V0ZXIiLCJ1c2VSb3V0ZXIiLCJjcmVhdGVTYWxlIiwidXJsIiwid2ViM01vZGFsIiwiV2ViM01vZGFsIiwibmV0d29yayIsImNhY2hlUHJvdmlkZXIiLCJjb25uZWN0aW9uIiwiY29ubmVjdCIsInByb3ZpZGVyIiwiZXRoZXJzIiwic2lnbmVyIiwiZ2V0U2lnbmVyIiwiY29udHJhY3QiLCJ0cmFuc2FjdGlvbiIsImNyZWF0ZVRva2VuIiwidHgiLCJ3YWl0IiwiZXZlbnQiLCJldmVudHMiLCJ2YWx1ZSIsImFyZ3MiLCJ0b2tlbklkIiwidG9OdW1iZXIiLCJ3ZWIzIiwibGlzdGluZ1ByaWNlIiwiY3JlYXRlTWFya2V0SXRlbSIsInB1c2giLCJvbkNoYW5nZSIsImUiLCJmaWxlIiwidGFyZ2V0IiwiZmlsZXMiLCJhZGRlZCIsImFkZCIsInByb2dyZXNzIiwicHJvZyIsImNvbnNvbGUiLCJsb2ciLCJwYXRoIiwiZXJyb3IiLCJjcmVhdGVNYXJrZXQiLCJkYXRhIiwiSlNPTiIsInN0cmluZ2lmeSIsImltYWdlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFPLE1BQU1BLGdCQUFnQixHQUFHLDRDQUF6QixDLENBQXVFOztBQUN2RSxNQUFNQyxVQUFVLEdBQUcsNENBQW5CLEMsQ0FBaUU7O0FBRWpFLE1BQU1DLFlBQVksR0FBRyxDQUMzQjtBQUNDLFlBQVUsRUFEWDtBQUVDLHFCQUFtQixZQUZwQjtBQUdDLFVBQVE7QUFIVCxDQUQyQixFQU0zQjtBQUNDLGVBQWEsS0FEZDtBQUVDLFlBQVUsQ0FDVDtBQUNDLGVBQVcsSUFEWjtBQUVDLG9CQUFnQixTQUZqQjtBQUdDLFlBQVEsUUFIVDtBQUlDLFlBQVE7QUFKVCxHQURTLEVBT1Q7QUFDQyxlQUFXLElBRFo7QUFFQyxvQkFBZ0IsU0FGakI7QUFHQyxZQUFRLGFBSFQ7QUFJQyxZQUFRO0FBSlQsR0FQUyxFQWFUO0FBQ0MsZUFBVyxJQURaO0FBRUMsb0JBQWdCLFNBRmpCO0FBR0MsWUFBUSxTQUhUO0FBSUMsWUFBUTtBQUpULEdBYlMsRUFtQlQ7QUFDQyxlQUFXLEtBRFo7QUFFQyxvQkFBZ0IsU0FGakI7QUFHQyxZQUFRLFFBSFQ7QUFJQyxZQUFRO0FBSlQsR0FuQlMsRUF5QlQ7QUFDQyxlQUFXLEtBRFo7QUFFQyxvQkFBZ0IsU0FGakI7QUFHQyxZQUFRLE9BSFQ7QUFJQyxZQUFRO0FBSlQsR0F6QlMsRUErQlQ7QUFDQyxlQUFXLEtBRFo7QUFFQyxvQkFBZ0IsU0FGakI7QUFHQyxZQUFRLE9BSFQ7QUFJQyxZQUFRO0FBSlQsR0EvQlMsQ0FGWDtBQXdDQyxVQUFRLG1CQXhDVDtBQXlDQyxVQUFRO0FBekNULENBTjJCLEVBaUQzQjtBQUNDLFlBQVUsQ0FDVDtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsYUFGVDtBQUdDLFlBQVE7QUFIVCxHQURTLEVBTVQ7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLFNBRlQ7QUFHQyxZQUFRO0FBSFQsR0FOUyxFQVdUO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxPQUZUO0FBR0MsWUFBUTtBQUhULEdBWFMsQ0FEWDtBQWtCQyxVQUFRLGtCQWxCVDtBQW1CQyxhQUFXLEVBbkJaO0FBb0JDLHFCQUFtQixTQXBCcEI7QUFxQkMsVUFBUTtBQXJCVCxDQWpEMkIsRUF3RTNCO0FBQ0MsWUFBVSxDQUNUO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxhQUZUO0FBR0MsWUFBUTtBQUhULEdBRFMsRUFNVDtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsUUFGVDtBQUdDLFlBQVE7QUFIVCxHQU5TLENBRFg7QUFhQyxVQUFRLGtCQWJUO0FBY0MsYUFBVyxFQWRaO0FBZUMscUJBQW1CLFNBZnBCO0FBZ0JDLFVBQVE7QUFoQlQsQ0F4RTJCLEVBMEYzQjtBQUNDLFlBQVUsQ0FDVDtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsUUFGVDtBQUdDLFlBQVE7QUFIVCxHQURTLENBRFg7QUFRQyxVQUFRLGlCQVJUO0FBU0MsYUFBVyxDQUNWO0FBQ0Msa0JBQWMsQ0FDYjtBQUNDLHNCQUFnQixTQURqQjtBQUVDLGNBQVEsUUFGVDtBQUdDLGNBQVE7QUFIVCxLQURhLEVBTWI7QUFDQyxzQkFBZ0IsU0FEakI7QUFFQyxjQUFRLGFBRlQ7QUFHQyxjQUFRO0FBSFQsS0FOYSxFQVdiO0FBQ0Msc0JBQWdCLFNBRGpCO0FBRUMsY0FBUSxTQUZUO0FBR0MsY0FBUTtBQUhULEtBWGEsRUFnQmI7QUFDQyxzQkFBZ0IsaUJBRGpCO0FBRUMsY0FBUSxRQUZUO0FBR0MsY0FBUTtBQUhULEtBaEJhLEVBcUJiO0FBQ0Msc0JBQWdCLGlCQURqQjtBQUVDLGNBQVEsT0FGVDtBQUdDLGNBQVE7QUFIVCxLQXJCYSxFQTBCYjtBQUNDLHNCQUFnQixTQURqQjtBQUVDLGNBQVEsT0FGVDtBQUdDLGNBQVE7QUFIVCxLQTFCYSxDQURmO0FBaUNDLG9CQUFnQiw2QkFqQ2pCO0FBa0NDLFlBQVEsRUFsQ1Q7QUFtQ0MsWUFBUTtBQW5DVCxHQURVLENBVFo7QUFnREMscUJBQW1CLE1BaERwQjtBQWlEQyxVQUFRO0FBakRULENBMUYyQixFQTZJM0I7QUFDQyxZQUFVLEVBRFg7QUFFQyxVQUFRLGtCQUZUO0FBR0MsYUFBVyxDQUNWO0FBQ0Msa0JBQWMsQ0FDYjtBQUNDLHNCQUFnQixTQURqQjtBQUVDLGNBQVEsUUFGVDtBQUdDLGNBQVE7QUFIVCxLQURhLEVBTWI7QUFDQyxzQkFBZ0IsU0FEakI7QUFFQyxjQUFRLGFBRlQ7QUFHQyxjQUFRO0FBSFQsS0FOYSxFQVdiO0FBQ0Msc0JBQWdCLFNBRGpCO0FBRUMsY0FBUSxTQUZUO0FBR0MsY0FBUTtBQUhULEtBWGEsRUFnQmI7QUFDQyxzQkFBZ0IsaUJBRGpCO0FBRUMsY0FBUSxRQUZUO0FBR0MsY0FBUTtBQUhULEtBaEJhLEVBcUJiO0FBQ0Msc0JBQWdCLGlCQURqQjtBQUVDLGNBQVEsT0FGVDtBQUdDLGNBQVE7QUFIVCxLQXJCYSxFQTBCYjtBQUNDLHNCQUFnQixTQURqQjtBQUVDLGNBQVEsT0FGVDtBQUdDLGNBQVE7QUFIVCxLQTFCYSxDQURmO0FBaUNDLG9CQUFnQiwrQkFqQ2pCO0FBa0NDLFlBQVEsRUFsQ1Q7QUFtQ0MsWUFBUTtBQW5DVCxHQURVLENBSFo7QUEwQ0MscUJBQW1CLE1BMUNwQjtBQTJDQyxVQUFRO0FBM0NULENBN0kyQixFQTBMM0I7QUFDQyxZQUFVLEVBRFg7QUFFQyxVQUFRLGFBRlQ7QUFHQyxhQUFXLENBQ1Y7QUFDQyxrQkFBYyxDQUNiO0FBQ0Msc0JBQWdCLFNBRGpCO0FBRUMsY0FBUSxRQUZUO0FBR0MsY0FBUTtBQUhULEtBRGEsRUFNYjtBQUNDLHNCQUFnQixTQURqQjtBQUVDLGNBQVEsYUFGVDtBQUdDLGNBQVE7QUFIVCxLQU5hLEVBV2I7QUFDQyxzQkFBZ0IsU0FEakI7QUFFQyxjQUFRLFNBRlQ7QUFHQyxjQUFRO0FBSFQsS0FYYSxFQWdCYjtBQUNDLHNCQUFnQixpQkFEakI7QUFFQyxjQUFRLFFBRlQ7QUFHQyxjQUFRO0FBSFQsS0FoQmEsRUFxQmI7QUFDQyxzQkFBZ0IsaUJBRGpCO0FBRUMsY0FBUSxPQUZUO0FBR0MsY0FBUTtBQUhULEtBckJhLEVBMEJiO0FBQ0Msc0JBQWdCLFNBRGpCO0FBRUMsY0FBUSxPQUZUO0FBR0MsY0FBUTtBQUhULEtBMUJhLENBRGY7QUFpQ0Msb0JBQWdCLCtCQWpDakI7QUFrQ0MsWUFBUSxFQWxDVDtBQW1DQyxZQUFRO0FBbkNULEdBRFUsQ0FIWjtBQTBDQyxxQkFBbUIsTUExQ3BCO0FBMkNDLFVBQVE7QUEzQ1QsQ0ExTDJCLEVBdU8zQjtBQUNDLFlBQVUsQ0FDVDtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsY0FGVDtBQUdDLFlBQVE7QUFIVCxHQURTLENBRFg7QUFRQyxVQUFRLGVBUlQ7QUFTQyxhQUFXLENBQ1Y7QUFDQyxrQkFBYyxDQUNiO0FBQ0Msc0JBQWdCLFNBRGpCO0FBRUMsY0FBUSxRQUZUO0FBR0MsY0FBUTtBQUhULEtBRGEsRUFNYjtBQUNDLHNCQUFnQixTQURqQjtBQUVDLGNBQVEsYUFGVDtBQUdDLGNBQVE7QUFIVCxLQU5hLEVBV2I7QUFDQyxzQkFBZ0IsU0FEakI7QUFFQyxjQUFRLFNBRlQ7QUFHQyxjQUFRO0FBSFQsS0FYYSxFQWdCYjtBQUNDLHNCQUFnQixpQkFEakI7QUFFQyxjQUFRLFFBRlQ7QUFHQyxjQUFRO0FBSFQsS0FoQmEsRUFxQmI7QUFDQyxzQkFBZ0IsaUJBRGpCO0FBRUMsY0FBUSxPQUZUO0FBR0MsY0FBUTtBQUhULEtBckJhLEVBMEJiO0FBQ0Msc0JBQWdCLFNBRGpCO0FBRUMsY0FBUSxPQUZUO0FBR0MsY0FBUTtBQUhULEtBMUJhLENBRGY7QUFpQ0Msb0JBQWdCLDZCQWpDakI7QUFrQ0MsWUFBUSxFQWxDVDtBQW1DQyxZQUFRO0FBbkNULEdBRFUsQ0FUWjtBQWdEQyxxQkFBbUIsTUFoRHBCO0FBaURDLFVBQVE7QUFqRFQsQ0F2TzJCLENBQXJCO0FBMlJBLE1BQU1DLE1BQU0sR0FBRyxDQUNyQjtBQUNDLFlBQVUsQ0FDVDtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsb0JBRlQ7QUFHQyxZQUFRO0FBSFQsR0FEUyxDQURYO0FBUUMscUJBQW1CLFlBUnBCO0FBU0MsVUFBUTtBQVRULENBRHFCLEVBWXJCO0FBQ0MsZUFBYSxLQURkO0FBRUMsWUFBVSxDQUNUO0FBQ0MsZUFBVyxJQURaO0FBRUMsb0JBQWdCLFNBRmpCO0FBR0MsWUFBUSxPQUhUO0FBSUMsWUFBUTtBQUpULEdBRFMsRUFPVDtBQUNDLGVBQVcsSUFEWjtBQUVDLG9CQUFnQixTQUZqQjtBQUdDLFlBQVEsVUFIVDtBQUlDLFlBQVE7QUFKVCxHQVBTLEVBYVQ7QUFDQyxlQUFXLElBRFo7QUFFQyxvQkFBZ0IsU0FGakI7QUFHQyxZQUFRLFNBSFQ7QUFJQyxZQUFRO0FBSlQsR0FiUyxDQUZYO0FBc0JDLFVBQVEsVUF0QlQ7QUF1QkMsVUFBUTtBQXZCVCxDQVpxQixFQXFDckI7QUFDQyxlQUFhLEtBRGQ7QUFFQyxZQUFVLENBQ1Q7QUFDQyxlQUFXLElBRFo7QUFFQyxvQkFBZ0IsU0FGakI7QUFHQyxZQUFRLE9BSFQ7QUFJQyxZQUFRO0FBSlQsR0FEUyxFQU9UO0FBQ0MsZUFBVyxJQURaO0FBRUMsb0JBQWdCLFNBRmpCO0FBR0MsWUFBUSxVQUhUO0FBSUMsWUFBUTtBQUpULEdBUFMsRUFhVDtBQUNDLGVBQVcsS0FEWjtBQUVDLG9CQUFnQixNQUZqQjtBQUdDLFlBQVEsVUFIVDtBQUlDLFlBQVE7QUFKVCxHQWJTLENBRlg7QUFzQkMsVUFBUSxnQkF0QlQ7QUF1QkMsVUFBUTtBQXZCVCxDQXJDcUIsRUE4RHJCO0FBQ0MsWUFBVSxDQUNUO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxJQUZUO0FBR0MsWUFBUTtBQUhULEdBRFMsRUFNVDtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsU0FGVDtBQUdDLFlBQVE7QUFIVCxHQU5TLENBRFg7QUFhQyxVQUFRLFNBYlQ7QUFjQyxhQUFXLEVBZFo7QUFlQyxxQkFBbUIsWUFmcEI7QUFnQkMsVUFBUTtBQWhCVCxDQTlEcUIsRUFnRnJCO0FBQ0MsWUFBVSxDQUNUO0FBQ0Msb0JBQWdCLFFBRGpCO0FBRUMsWUFBUSxVQUZUO0FBR0MsWUFBUTtBQUhULEdBRFMsQ0FEWDtBQVFDLFVBQVEsYUFSVDtBQVNDLGFBQVcsQ0FDVjtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsRUFGVDtBQUdDLFlBQVE7QUFIVCxHQURVLENBVFo7QUFnQkMscUJBQW1CLFlBaEJwQjtBQWlCQyxVQUFRO0FBakJULENBaEZxQixFQW1HckI7QUFDQyxZQUFVLENBQ1Q7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLE1BRlQ7QUFHQyxZQUFRO0FBSFQsR0FEUyxFQU1UO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxJQUZUO0FBR0MsWUFBUTtBQUhULEdBTlMsRUFXVDtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsU0FGVDtBQUdDLFlBQVE7QUFIVCxHQVhTLENBRFg7QUFrQkMsVUFBUSxrQkFsQlQ7QUFtQkMsYUFBVyxFQW5CWjtBQW9CQyxxQkFBbUIsWUFwQnBCO0FBcUJDLFVBQVE7QUFyQlQsQ0FuR3FCLEVBMEhyQjtBQUNDLFlBQVUsQ0FDVDtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsTUFGVDtBQUdDLFlBQVE7QUFIVCxHQURTLEVBTVQ7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLElBRlQ7QUFHQyxZQUFRO0FBSFQsR0FOUyxFQVdUO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxTQUZUO0FBR0MsWUFBUTtBQUhULEdBWFMsRUFnQlQ7QUFDQyxvQkFBZ0IsT0FEakI7QUFFQyxZQUFRLE9BRlQ7QUFHQyxZQUFRO0FBSFQsR0FoQlMsQ0FEWDtBQXVCQyxVQUFRLGtCQXZCVDtBQXdCQyxhQUFXLEVBeEJaO0FBeUJDLHFCQUFtQixZQXpCcEI7QUEwQkMsVUFBUTtBQTFCVCxDQTFIcUIsRUFzSnJCO0FBQ0MsWUFBVSxDQUNUO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxVQUZUO0FBR0MsWUFBUTtBQUhULEdBRFMsRUFNVDtBQUNDLG9CQUFnQixNQURqQjtBQUVDLFlBQVEsVUFGVDtBQUdDLFlBQVE7QUFIVCxHQU5TLENBRFg7QUFhQyxVQUFRLG1CQWJUO0FBY0MsYUFBVyxFQWRaO0FBZUMscUJBQW1CLFlBZnBCO0FBZ0JDLFVBQVE7QUFoQlQsQ0F0SnFCLEVBd0tyQjtBQUNDLGVBQWEsS0FEZDtBQUVDLFlBQVUsQ0FDVDtBQUNDLGVBQVcsSUFEWjtBQUVDLG9CQUFnQixTQUZqQjtBQUdDLFlBQVEsTUFIVDtBQUlDLFlBQVE7QUFKVCxHQURTLEVBT1Q7QUFDQyxlQUFXLElBRFo7QUFFQyxvQkFBZ0IsU0FGakI7QUFHQyxZQUFRLElBSFQ7QUFJQyxZQUFRO0FBSlQsR0FQUyxFQWFUO0FBQ0MsZUFBVyxJQURaO0FBRUMsb0JBQWdCLFNBRmpCO0FBR0MsWUFBUSxTQUhUO0FBSUMsWUFBUTtBQUpULEdBYlMsQ0FGWDtBQXNCQyxVQUFRLFVBdEJUO0FBdUJDLFVBQVE7QUF2QlQsQ0F4S3FCLEVBaU1yQjtBQUNDLFlBQVUsQ0FDVDtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsTUFGVDtBQUdDLFlBQVE7QUFIVCxHQURTLEVBTVQ7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLElBRlQ7QUFHQyxZQUFRO0FBSFQsR0FOUyxFQVdUO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxTQUZUO0FBR0MsWUFBUTtBQUhULEdBWFMsQ0FEWDtBQWtCQyxVQUFRLGNBbEJUO0FBbUJDLGFBQVcsRUFuQlo7QUFvQkMscUJBQW1CLFlBcEJwQjtBQXFCQyxVQUFRO0FBckJULENBak1xQixFQXdOckI7QUFDQyxZQUFVLENBQ1Q7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLE9BRlQ7QUFHQyxZQUFRO0FBSFQsR0FEUyxDQURYO0FBUUMsVUFBUSxXQVJUO0FBU0MsYUFBVyxDQUNWO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxFQUZUO0FBR0MsWUFBUTtBQUhULEdBRFUsQ0FUWjtBQWdCQyxxQkFBbUIsTUFoQnBCO0FBaUJDLFVBQVE7QUFqQlQsQ0F4TnFCLEVBMk9yQjtBQUNDLFlBQVUsQ0FDVDtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsU0FGVDtBQUdDLFlBQVE7QUFIVCxHQURTLENBRFg7QUFRQyxVQUFRLGFBUlQ7QUFTQyxhQUFXLENBQ1Y7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLEVBRlQ7QUFHQyxZQUFRO0FBSFQsR0FEVSxDQVRaO0FBZ0JDLHFCQUFtQixNQWhCcEI7QUFpQkMsVUFBUTtBQWpCVCxDQTNPcUIsRUE4UHJCO0FBQ0MsWUFBVSxDQUNUO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxPQUZUO0FBR0MsWUFBUTtBQUhULEdBRFMsRUFNVDtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsVUFGVDtBQUdDLFlBQVE7QUFIVCxHQU5TLENBRFg7QUFhQyxVQUFRLGtCQWJUO0FBY0MsYUFBVyxDQUNWO0FBQ0Msb0JBQWdCLE1BRGpCO0FBRUMsWUFBUSxFQUZUO0FBR0MsWUFBUTtBQUhULEdBRFUsQ0FkWjtBQXFCQyxxQkFBbUIsTUFyQnBCO0FBc0JDLFVBQVE7QUF0QlQsQ0E5UHFCLEVBc1JyQjtBQUNDLFlBQVUsRUFEWDtBQUVDLFVBQVEsTUFGVDtBQUdDLGFBQVcsQ0FDVjtBQUNDLG9CQUFnQixRQURqQjtBQUVDLFlBQVEsRUFGVDtBQUdDLFlBQVE7QUFIVCxHQURVLENBSFo7QUFVQyxxQkFBbUIsTUFWcEI7QUFXQyxVQUFRO0FBWFQsQ0F0UnFCLEVBbVNyQjtBQUNDLFlBQVUsQ0FDVDtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsU0FGVDtBQUdDLFlBQVE7QUFIVCxHQURTLENBRFg7QUFRQyxVQUFRLFNBUlQ7QUFTQyxhQUFXLENBQ1Y7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLEVBRlQ7QUFHQyxZQUFRO0FBSFQsR0FEVSxDQVRaO0FBZ0JDLHFCQUFtQixNQWhCcEI7QUFpQkMsVUFBUTtBQWpCVCxDQW5TcUIsRUFzVHJCO0FBQ0MsWUFBVSxDQUNUO0FBQ0Msb0JBQWdCLFFBRGpCO0FBRUMsWUFBUSxhQUZUO0FBR0MsWUFBUTtBQUhULEdBRFMsQ0FEWDtBQVFDLFVBQVEsbUJBUlQ7QUFTQyxhQUFXLENBQ1Y7QUFDQyxvQkFBZ0IsTUFEakI7QUFFQyxZQUFRLEVBRlQ7QUFHQyxZQUFRO0FBSFQsR0FEVSxDQVRaO0FBZ0JDLHFCQUFtQixNQWhCcEI7QUFpQkMsVUFBUTtBQWpCVCxDQXRUcUIsRUF5VXJCO0FBQ0MsWUFBVSxFQURYO0FBRUMsVUFBUSxRQUZUO0FBR0MsYUFBVyxDQUNWO0FBQ0Msb0JBQWdCLFFBRGpCO0FBRUMsWUFBUSxFQUZUO0FBR0MsWUFBUTtBQUhULEdBRFUsQ0FIWjtBQVVDLHFCQUFtQixNQVZwQjtBQVdDLFVBQVE7QUFYVCxDQXpVcUIsRUFzVnJCO0FBQ0MsWUFBVSxDQUNUO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxTQUZUO0FBR0MsWUFBUTtBQUhULEdBRFMsQ0FEWDtBQVFDLFVBQVEsVUFSVDtBQVNDLGFBQVcsQ0FDVjtBQUNDLG9CQUFnQixRQURqQjtBQUVDLFlBQVEsRUFGVDtBQUdDLFlBQVE7QUFIVCxHQURVLENBVFo7QUFnQkMscUJBQW1CLE1BaEJwQjtBQWlCQyxVQUFRO0FBakJULENBdFZxQixDQUFmLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzlSUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQSxNQUFNQyxNQUFNLEdBQUdDLHdEQUFjLENBQUMsb0NBQUQsQ0FBN0I7QUFFQTtBQUtlLFNBQVNDLElBQVQsR0FBZ0I7QUFDN0IsUUFBTTtBQUFBLE9BQUNDLE9BQUQ7QUFBQSxPQUFVQztBQUFWLE1BQXdCQywrQ0FBUSxDQUFDLElBQUQsQ0FBdEM7QUFDQSxRQUFNO0FBQUEsT0FBQ0MsU0FBRDtBQUFBLE9BQVlDO0FBQVosTUFBK0JGLCtDQUFRLENBQUM7QUFBRUcsU0FBSyxFQUFFLEVBQVQ7QUFBYUMsUUFBSSxFQUFFLEVBQW5CO0FBQXVCQyxlQUFXLEVBQUU7QUFBcEMsR0FBRCxDQUE3QztBQUNBLFFBQU1DLE1BQU0sR0FBR0Msc0RBQVMsRUFBeEI7O0FBRUEsaUJBQWVDLFVBQWYsQ0FBMEJDLEdBQTFCLEVBQStCO0FBQzdCLFVBQU1DLFNBQVMsR0FBRyxJQUFJQyxrREFBSixDQUFjO0FBQzlCQyxhQUFPLEVBQUUsU0FEcUI7QUFFOUJDLG1CQUFhLEVBQUU7QUFGZSxLQUFkLENBQWxCO0FBSUEsVUFBTUMsVUFBVSxHQUFHLE1BQU1KLFNBQVMsQ0FBQ0ssT0FBVixFQUF6QjtBQUNBLFVBQU1DLFFBQVEsR0FBRyxJQUFJQyxpRUFBSixDQUFrQ0gsVUFBbEMsQ0FBakI7QUFDQSxVQUFNSSxNQUFNLEdBQUdGLFFBQVEsQ0FBQ0csU0FBVCxFQUFmO0FBRUEsUUFBSUMsUUFBUSxHQUFHLElBQUlILG1EQUFKLENBQW9CekIsK0NBQXBCLEVBQStCRSwyQ0FBL0IsRUFBdUN3QixNQUF2QyxDQUFmO0FBQ0EsUUFBSUcsV0FBVyxHQUFHLE1BQU1ELFFBQVEsQ0FBQ0UsV0FBVCxDQUFxQmIsR0FBckIsQ0FBeEI7QUFDQSxRQUFJYyxFQUFFLEdBQUcsTUFBTUYsV0FBVyxDQUFDRyxJQUFaLEVBQWY7QUFDQSxRQUFJQyxLQUFLLEdBQUdGLEVBQUUsQ0FBQ0csTUFBSCxDQUFVLENBQVYsQ0FBWjtBQUNBLFFBQUlDLEtBQUssR0FBR0YsS0FBSyxDQUFDRyxJQUFOLENBQVcsQ0FBWCxDQUFaO0FBQ0EsUUFBSUMsT0FBTyxHQUFHRixLQUFLLENBQUNHLFFBQU4sRUFBZDtBQUNBLFVBQU0zQixLQUFLLEdBQUc0Qix1REFBQSxDQUFpQjlCLFNBQVMsQ0FBQ0UsS0FBM0IsRUFBa0MsT0FBbEMsQ0FBZDtBQUVBLFVBQU02QixZQUFZLEdBQUdELHVEQUFBLENBQWlCLEtBQWpCLEVBQXdCLE9BQXhCLENBQXJCO0FBRUFYLFlBQVEsR0FBRyxJQUFJSCxtREFBSixDQUFvQjFCLHFEQUFwQixFQUFzQ0UsaURBQXRDLEVBQW9EeUIsTUFBcEQsQ0FBWDtBQUNBRyxlQUFXLEdBQUcsTUFBTUQsUUFBUSxDQUFDYSxnQkFBVCxDQUEwQnpDLCtDQUExQixFQUFzQ3FDLE9BQXRDLEVBQStDMUIsS0FBL0MsRUFBc0Q7QUFBRXdCLFdBQUssRUFBRUs7QUFBVCxLQUF0RCxDQUFwQjtBQUVBLFVBQU1YLFdBQVcsQ0FBQ0csSUFBWixFQUFOO0FBQ0FsQixVQUFNLENBQUM0QixJQUFQLENBQVksR0FBWjtBQUNEOztBQUNELGlCQUFlQyxRQUFmLENBQXdCQyxDQUF4QixFQUEyQjtBQUN6QixVQUFNQyxJQUFJLEdBQUdELENBQUMsQ0FBQ0UsTUFBRixDQUFTQyxLQUFULENBQWUsQ0FBZixDQUFiOztBQUNBLFFBQUk7QUFDRixZQUFNQyxLQUFLLEdBQUcsTUFBTTdDLE1BQU0sQ0FBQzhDLEdBQVAsQ0FDbEJKLElBRGtCLEVBRWxCO0FBQ0VLLGdCQUFRLEVBQUdDLElBQUQsSUFBVUMsT0FBTyxDQUFDQyxHQUFSLENBQWEsYUFBWUYsSUFBSyxFQUE5QjtBQUR0QixPQUZrQixDQUFwQjtBQU1BLFlBQU1sQyxHQUFHLEdBQUksK0JBQThCK0IsS0FBSyxDQUFDTSxJQUFLLEVBQXREO0FBQ0EvQyxnQkFBVSxDQUFDVSxHQUFELENBQVY7QUFDRCxLQVRELENBU0UsT0FBT3NDLEtBQVAsRUFBYztBQUNkSCxhQUFPLENBQUNDLEdBQVIsQ0FBWSx3QkFBWixFQUFzQ0UsS0FBdEM7QUFDRDtBQUNGOztBQUNELGlCQUFlQyxZQUFmLEdBQThCO0FBQzVCLFVBQU07QUFBRTVDLFVBQUY7QUFBUUMsaUJBQVI7QUFBcUJGO0FBQXJCLFFBQStCRixTQUFyQztBQUNBLFFBQUksQ0FBQ0csSUFBRCxJQUFTLENBQUNDLFdBQVYsSUFBeUIsQ0FBQ0YsS0FBMUIsSUFBbUMsQ0FBQ0wsT0FBeEMsRUFBaUQsT0FGckIsQ0FHNUI7O0FBQ0EsVUFBTW1ELElBQUksR0FBR0MsSUFBSSxDQUFDQyxTQUFMLENBQWU7QUFDMUIvQyxVQUQwQjtBQUNwQkMsaUJBRG9CO0FBQ1ArQyxXQUFLLEVBQUV0RDtBQURBLEtBQWYsQ0FBYjs7QUFHQSxRQUFJO0FBQ0YsWUFBTTBDLEtBQUssR0FBRyxNQUFNN0MsTUFBTSxDQUFDOEMsR0FBUCxDQUFXUSxJQUFYLENBQXBCO0FBQ0EsWUFBTXhDLEdBQUcsR0FBSSwrQkFBOEIrQixLQUFLLENBQUNNLElBQUssRUFBdEQ7QUFDQXRDLGdCQUFVLENBQUNDLEdBQUQsQ0FBVjtBQUNELEtBSkQsQ0FJRSxPQUFPc0MsS0FBUCxFQUFjO0FBQ2RILGFBQU8sQ0FBQ0MsR0FBUixDQUFZLHdCQUFaLEVBQXNDRSxLQUF0QztBQUNEO0FBQ0Y7O0FBRUQsc0JBQ0U7QUFBSyxhQUFTLEVBQUMscUJBQWY7QUFBQSwyQkFDRTtBQUFLLGVBQVMsRUFBQywyQkFBZjtBQUFBLDhCQUNFO0FBQ0UsbUJBQVcsRUFBQyxVQURkO0FBRUUsaUJBQVMsRUFBQyx5QkFGWjtBQUdFLGdCQUFRLEVBQUVYLENBQUMsSUFBSWxDLGVBQWUsaUNBQU1ELFNBQU47QUFBaUJHLGNBQUksRUFBRWdDLENBQUMsQ0FBQ0UsTUFBRixDQUFTWDtBQUFoQztBQUhoQztBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREYsZUFNRTtBQUNFLG1CQUFXLEVBQUMsaUJBRGQ7QUFFRSxpQkFBUyxFQUFDLHlCQUZaO0FBR0UsZ0JBQVEsRUFBRVMsQ0FBQyxJQUFJbEMsZUFBZSxpQ0FBTUQsU0FBTjtBQUFpQkkscUJBQVcsRUFBRStCLENBQUMsQ0FBQ0UsTUFBRixDQUFTWDtBQUF2QztBQUhoQztBQUFBO0FBQUE7QUFBQTtBQUFBLGNBTkYsZUFXRTtBQUNFLG1CQUFXLEVBQUMsa0JBRGQ7QUFFRSxpQkFBUyxFQUFDLHlCQUZaO0FBR0UsZ0JBQVEsRUFBRVMsQ0FBQyxJQUFJbEMsZUFBZSxpQ0FBTUQsU0FBTjtBQUFpQkUsZUFBSyxFQUFFaUMsQ0FBQyxDQUFDRSxNQUFGLENBQVNYO0FBQWpDO0FBSGhDO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FYRixlQWdCRTtBQUNFLFlBQUksRUFBQyxNQURQO0FBRUUsWUFBSSxFQUFDLEtBRlA7QUFHRSxpQkFBUyxFQUFDLE1BSFo7QUFJRSxnQkFBUSxFQUFFUTtBQUpaO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FoQkYsRUF1QklyQyxPQUFPLGlCQUNMO0FBQUssaUJBQVMsRUFBQyxjQUFmO0FBQThCLGFBQUssRUFBQyxLQUFwQztBQUEwQyxXQUFHLEVBQUVBO0FBQS9DO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0F4Qk4sZUEyQkU7QUFBUSxlQUFPLEVBQUVrRCxZQUFqQjtBQUErQixpQkFBUyxFQUFDLG1EQUF6QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQTNCRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREY7QUFtQ0QsQzs7Ozs7Ozs7Ozs7QUM5R0Qsb0M7Ozs7Ozs7Ozs7O0FDQUEsOEM7Ozs7Ozs7Ozs7O0FDQUEseUM7Ozs7Ozs7Ozs7O0FDQUEsbUM7Ozs7Ozs7Ozs7O0FDQUEsbUQ7Ozs7Ozs7Ozs7O0FDQUEsa0M7Ozs7Ozs7Ozs7O0FDQUEsdUMiLCJmaWxlIjoicGFnZXMvY3JlYXRlLWl0ZW0uanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY29uc3QgbmZ0bWFya2V0YWRkcmVzcyA9IFwiMHg5MTA2ZjBBRjAwMEFCMGY4MzhiMDI1MTU4ZTU4MzJmNThGZTNhMzVFXCIgIC8vcmlua2VieVxuZXhwb3J0IGNvbnN0IG5mdGFkZHJlc3MgPSBcIjB4RDgxYWIwODNlODM1RkJlNzIxREI2QjVDNzc0OWRjNzJFMjc0NGU1N1wiICAvL3JpbmtlYnlcblxuZXhwb3J0IGNvbnN0IG5mdG1hcmtldGFiaSA9IFtcblx0e1xuXHRcdFwiaW5wdXRzXCI6IFtdLFxuXHRcdFwic3RhdGVNdXRhYmlsaXR5XCI6IFwibm9ucGF5YWJsZVwiLFxuXHRcdFwidHlwZVwiOiBcImNvbnN0cnVjdG9yXCJcblx0fSxcblx0e1xuXHRcdFwiYW5vbnltb3VzXCI6IGZhbHNlLFxuXHRcdFwiaW5wdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbmRleGVkXCI6IHRydWUsXG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJpdGVtSWRcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRcImluZGV4ZWRcIjogdHJ1ZSxcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIm5mdENvbnRyYWN0XCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0XCJpbmRleGVkXCI6IHRydWUsXG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJ0b2tlbklkXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0XCJpbmRleGVkXCI6IGZhbHNlLFxuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwic2VsbGVyXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0XCJpbmRleGVkXCI6IGZhbHNlLFxuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwib3duZXJcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRcImluZGV4ZWRcIjogZmFsc2UsXG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJwcmljZVwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwibmFtZVwiOiBcIk1hcmtldEl0ZW1DcmVhdGVkXCIsXG5cdFx0XCJ0eXBlXCI6IFwiZXZlbnRcIlxuXHR9LFxuXHR7XG5cdFx0XCJpbnB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwibmZ0Q29udHJhY3RcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwidG9rZW5JZFwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJwcmljZVwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwibmFtZVwiOiBcImNyZWF0ZU1hcmtldEl0ZW1cIixcblx0XHRcIm91dHB1dHNcIjogW10sXG5cdFx0XCJzdGF0ZU11dGFiaWxpdHlcIjogXCJwYXlhYmxlXCIsXG5cdFx0XCJ0eXBlXCI6IFwiZnVuY3Rpb25cIlxuXHR9LFxuXHR7XG5cdFx0XCJpbnB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwibmZ0Q29udHJhY3RcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwiaXRlbUlkXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJuYW1lXCI6IFwiY3JlYXRlTWFya2V0U2FsZVwiLFxuXHRcdFwib3V0cHV0c1wiOiBbXSxcblx0XHRcInN0YXRlTXV0YWJpbGl0eVwiOiBcInBheWFibGVcIixcblx0XHRcInR5cGVcIjogXCJmdW5jdGlvblwiXG5cdH0sXG5cdHtcblx0XHRcImlucHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJpdGVtSWRcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcIm5hbWVcIjogXCJmZXRjaE1hcmtldEl0ZW1cIixcblx0XHRcIm91dHB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImNvbXBvbmVudHNcIjogW1xuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcdFx0XCJuYW1lXCI6IFwiaXRlbUlkXCIsXG5cdFx0XHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcdFx0XCJuYW1lXCI6IFwibmZ0Q29udHJhY3RcIixcblx0XHRcdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFx0XHRcIm5hbWVcIjogXCJ0b2tlbklkXCIsXG5cdFx0XHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzcyBwYXlhYmxlXCIsXG5cdFx0XHRcdFx0XHRcIm5hbWVcIjogXCJzZWxsZXJcIixcblx0XHRcdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzIHBheWFibGVcIixcblx0XHRcdFx0XHRcdFwibmFtZVwiOiBcIm93bmVyXCIsXG5cdFx0XHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcdFx0XCJuYW1lXCI6IFwicHJpY2VcIixcblx0XHRcdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0XHRcdH1cblx0XHRcdFx0XSxcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJzdHJ1Y3QgTkZUTWFya2V0Lk1hcmtldEl0ZW1cIixcblx0XHRcdFx0XCJuYW1lXCI6IFwiXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInR1cGxlXCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwic3RhdGVNdXRhYmlsaXR5XCI6IFwidmlld1wiLFxuXHRcdFwidHlwZVwiOiBcImZ1bmN0aW9uXCJcblx0fSxcblx0e1xuXHRcdFwiaW5wdXRzXCI6IFtdLFxuXHRcdFwibmFtZVwiOiBcImZldGNoTWFya2V0SXRlbXNcIixcblx0XHRcIm91dHB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImNvbXBvbmVudHNcIjogW1xuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcdFx0XCJuYW1lXCI6IFwiaXRlbUlkXCIsXG5cdFx0XHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcdFx0XCJuYW1lXCI6IFwibmZ0Q29udHJhY3RcIixcblx0XHRcdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFx0XHRcIm5hbWVcIjogXCJ0b2tlbklkXCIsXG5cdFx0XHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzcyBwYXlhYmxlXCIsXG5cdFx0XHRcdFx0XHRcIm5hbWVcIjogXCJzZWxsZXJcIixcblx0XHRcdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzIHBheWFibGVcIixcblx0XHRcdFx0XHRcdFwibmFtZVwiOiBcIm93bmVyXCIsXG5cdFx0XHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcdFx0XCJuYW1lXCI6IFwicHJpY2VcIixcblx0XHRcdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0XHRcdH1cblx0XHRcdFx0XSxcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJzdHJ1Y3QgTkZUTWFya2V0Lk1hcmtldEl0ZW1bXVwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwidHVwbGVbXVwiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcInN0YXRlTXV0YWJpbGl0eVwiOiBcInZpZXdcIixcblx0XHRcInR5cGVcIjogXCJmdW5jdGlvblwiXG5cdH0sXG5cdHtcblx0XHRcImlucHV0c1wiOiBbXSxcblx0XHRcIm5hbWVcIjogXCJmZXRjaE15TkZUc1wiLFxuXHRcdFwib3V0cHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiY29tcG9uZW50c1wiOiBbXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFx0XHRcIm5hbWVcIjogXCJpdGVtSWRcIixcblx0XHRcdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFx0XHRcIm5hbWVcIjogXCJuZnRDb250cmFjdFwiLFxuXHRcdFx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XHRcdFwibmFtZVwiOiBcInRva2VuSWRcIixcblx0XHRcdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzIHBheWFibGVcIixcblx0XHRcdFx0XHRcdFwibmFtZVwiOiBcInNlbGxlclwiLFxuXHRcdFx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3MgcGF5YWJsZVwiLFxuXHRcdFx0XHRcdFx0XCJuYW1lXCI6IFwib3duZXJcIixcblx0XHRcdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFx0XHRcIm5hbWVcIjogXCJwcmljZVwiLFxuXHRcdFx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRdLFxuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInN0cnVjdCBORlRNYXJrZXQuTWFya2V0SXRlbVtdXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIlwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJ0dXBsZVtdXCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwic3RhdGVNdXRhYmlsaXR5XCI6IFwidmlld1wiLFxuXHRcdFwidHlwZVwiOiBcImZ1bmN0aW9uXCJcblx0fSxcblx0e1xuXHRcdFwiaW5wdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIm1hcmtldEl0ZW1JZFwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwibmFtZVwiOiBcImdldE1hcmtldEl0ZW1cIixcblx0XHRcIm91dHB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImNvbXBvbmVudHNcIjogW1xuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcdFx0XCJuYW1lXCI6IFwiaXRlbUlkXCIsXG5cdFx0XHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcdFx0XCJuYW1lXCI6IFwibmZ0Q29udHJhY3RcIixcblx0XHRcdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFx0XHRcIm5hbWVcIjogXCJ0b2tlbklkXCIsXG5cdFx0XHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzcyBwYXlhYmxlXCIsXG5cdFx0XHRcdFx0XHRcIm5hbWVcIjogXCJzZWxsZXJcIixcblx0XHRcdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzIHBheWFibGVcIixcblx0XHRcdFx0XHRcdFwibmFtZVwiOiBcIm93bmVyXCIsXG5cdFx0XHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcdFx0XCJuYW1lXCI6IFwicHJpY2VcIixcblx0XHRcdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0XHRcdH1cblx0XHRcdFx0XSxcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJzdHJ1Y3QgTkZUTWFya2V0Lk1hcmtldEl0ZW1cIixcblx0XHRcdFx0XCJuYW1lXCI6IFwiXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInR1cGxlXCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwic3RhdGVNdXRhYmlsaXR5XCI6IFwidmlld1wiLFxuXHRcdFwidHlwZVwiOiBcImZ1bmN0aW9uXCJcblx0fVxuXVxuZXhwb3J0IGNvbnN0IG5mdGFiaSA9IFtcblx0e1xuXHRcdFwiaW5wdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIm1hcmtldHBsYWNlQWRkcmVzc1wiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwic3RhdGVNdXRhYmlsaXR5XCI6IFwibm9ucGF5YWJsZVwiLFxuXHRcdFwidHlwZVwiOiBcImNvbnN0cnVjdG9yXCJcblx0fSxcblx0e1xuXHRcdFwiYW5vbnltb3VzXCI6IGZhbHNlLFxuXHRcdFwiaW5wdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbmRleGVkXCI6IHRydWUsXG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJvd25lclwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW5kZXhlZFwiOiB0cnVlLFxuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwiYXBwcm92ZWRcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRcImluZGV4ZWRcIjogdHJ1ZSxcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcInRva2VuSWRcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcIm5hbWVcIjogXCJBcHByb3ZhbFwiLFxuXHRcdFwidHlwZVwiOiBcImV2ZW50XCJcblx0fSxcblx0e1xuXHRcdFwiYW5vbnltb3VzXCI6IGZhbHNlLFxuXHRcdFwiaW5wdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbmRleGVkXCI6IHRydWUsXG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJvd25lclwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW5kZXhlZFwiOiB0cnVlLFxuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwib3BlcmF0b3JcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRcImluZGV4ZWRcIjogZmFsc2UsXG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYm9vbFwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJhcHByb3ZlZFwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJib29sXCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwibmFtZVwiOiBcIkFwcHJvdmFsRm9yQWxsXCIsXG5cdFx0XCJ0eXBlXCI6IFwiZXZlbnRcIlxuXHR9LFxuXHR7XG5cdFx0XCJpbnB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwidG9cIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwidG9rZW5JZFwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwibmFtZVwiOiBcImFwcHJvdmVcIixcblx0XHRcIm91dHB1dHNcIjogW10sXG5cdFx0XCJzdGF0ZU11dGFiaWxpdHlcIjogXCJub25wYXlhYmxlXCIsXG5cdFx0XCJ0eXBlXCI6IFwiZnVuY3Rpb25cIlxuXHR9LFxuXHR7XG5cdFx0XCJpbnB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInN0cmluZ1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJ0b2tlblVSSVwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJzdHJpbmdcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJuYW1lXCI6IFwiY3JlYXRlVG9rZW5cIixcblx0XHRcIm91dHB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwiXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJzdGF0ZU11dGFiaWxpdHlcIjogXCJub25wYXlhYmxlXCIsXG5cdFx0XCJ0eXBlXCI6IFwiZnVuY3Rpb25cIlxuXHR9LFxuXHR7XG5cdFx0XCJpbnB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwiZnJvbVwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJ0b1wiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJ0b2tlbklkXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJuYW1lXCI6IFwic2FmZVRyYW5zZmVyRnJvbVwiLFxuXHRcdFwib3V0cHV0c1wiOiBbXSxcblx0XHRcInN0YXRlTXV0YWJpbGl0eVwiOiBcIm5vbnBheWFibGVcIixcblx0XHRcInR5cGVcIjogXCJmdW5jdGlvblwiXG5cdH0sXG5cdHtcblx0XHRcImlucHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJmcm9tXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcInRvXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcInRva2VuSWRcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImJ5dGVzXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIl9kYXRhXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImJ5dGVzXCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwibmFtZVwiOiBcInNhZmVUcmFuc2ZlckZyb21cIixcblx0XHRcIm91dHB1dHNcIjogW10sXG5cdFx0XCJzdGF0ZU11dGFiaWxpdHlcIjogXCJub25wYXlhYmxlXCIsXG5cdFx0XCJ0eXBlXCI6IFwiZnVuY3Rpb25cIlxuXHR9LFxuXHR7XG5cdFx0XCJpbnB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwib3BlcmF0b3JcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImJvb2xcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwiYXBwcm92ZWRcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYm9vbFwiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcIm5hbWVcIjogXCJzZXRBcHByb3ZhbEZvckFsbFwiLFxuXHRcdFwib3V0cHV0c1wiOiBbXSxcblx0XHRcInN0YXRlTXV0YWJpbGl0eVwiOiBcIm5vbnBheWFibGVcIixcblx0XHRcInR5cGVcIjogXCJmdW5jdGlvblwiXG5cdH0sXG5cdHtcblx0XHRcImFub255bW91c1wiOiBmYWxzZSxcblx0XHRcImlucHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW5kZXhlZFwiOiB0cnVlLFxuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwiZnJvbVwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW5kZXhlZFwiOiB0cnVlLFxuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwidG9cIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRcImluZGV4ZWRcIjogdHJ1ZSxcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcInRva2VuSWRcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcIm5hbWVcIjogXCJUcmFuc2ZlclwiLFxuXHRcdFwidHlwZVwiOiBcImV2ZW50XCJcblx0fSxcblx0e1xuXHRcdFwiaW5wdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcImZyb21cIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwidG9cIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwidG9rZW5JZFwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwibmFtZVwiOiBcInRyYW5zZmVyRnJvbVwiLFxuXHRcdFwib3V0cHV0c1wiOiBbXSxcblx0XHRcInN0YXRlTXV0YWJpbGl0eVwiOiBcIm5vbnBheWFibGVcIixcblx0XHRcInR5cGVcIjogXCJmdW5jdGlvblwiXG5cdH0sXG5cdHtcblx0XHRcImlucHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJvd25lclwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwibmFtZVwiOiBcImJhbGFuY2VPZlwiLFxuXHRcdFwib3V0cHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcInN0YXRlTXV0YWJpbGl0eVwiOiBcInZpZXdcIixcblx0XHRcInR5cGVcIjogXCJmdW5jdGlvblwiXG5cdH0sXG5cdHtcblx0XHRcImlucHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJ0b2tlbklkXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJuYW1lXCI6IFwiZ2V0QXBwcm92ZWRcIixcblx0XHRcIm91dHB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwiXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJzdGF0ZU11dGFiaWxpdHlcIjogXCJ2aWV3XCIsXG5cdFx0XCJ0eXBlXCI6IFwiZnVuY3Rpb25cIlxuXHR9LFxuXHR7XG5cdFx0XCJpbnB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwib3duZXJcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwib3BlcmF0b3JcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcIm5hbWVcIjogXCJpc0FwcHJvdmVkRm9yQWxsXCIsXG5cdFx0XCJvdXRwdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJib29sXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIlwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJib29sXCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwic3RhdGVNdXRhYmlsaXR5XCI6IFwidmlld1wiLFxuXHRcdFwidHlwZVwiOiBcImZ1bmN0aW9uXCJcblx0fSxcblx0e1xuXHRcdFwiaW5wdXRzXCI6IFtdLFxuXHRcdFwibmFtZVwiOiBcIm5hbWVcIixcblx0XHRcIm91dHB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInN0cmluZ1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwic3RyaW5nXCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwic3RhdGVNdXRhYmlsaXR5XCI6IFwidmlld1wiLFxuXHRcdFwidHlwZVwiOiBcImZ1bmN0aW9uXCJcblx0fSxcblx0e1xuXHRcdFwiaW5wdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcInRva2VuSWRcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcIm5hbWVcIjogXCJvd25lck9mXCIsXG5cdFx0XCJvdXRwdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIlwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwic3RhdGVNdXRhYmlsaXR5XCI6IFwidmlld1wiLFxuXHRcdFwidHlwZVwiOiBcImZ1bmN0aW9uXCJcblx0fSxcblx0e1xuXHRcdFwiaW5wdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJieXRlczRcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwiaW50ZXJmYWNlSWRcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYnl0ZXM0XCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwibmFtZVwiOiBcInN1cHBvcnRzSW50ZXJmYWNlXCIsXG5cdFx0XCJvdXRwdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJib29sXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIlwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJib29sXCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwic3RhdGVNdXRhYmlsaXR5XCI6IFwidmlld1wiLFxuXHRcdFwidHlwZVwiOiBcImZ1bmN0aW9uXCJcblx0fSxcblx0e1xuXHRcdFwiaW5wdXRzXCI6IFtdLFxuXHRcdFwibmFtZVwiOiBcInN5bWJvbFwiLFxuXHRcdFwib3V0cHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwic3RyaW5nXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIlwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJzdHJpbmdcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJzdGF0ZU11dGFiaWxpdHlcIjogXCJ2aWV3XCIsXG5cdFx0XCJ0eXBlXCI6IFwiZnVuY3Rpb25cIlxuXHR9LFxuXHR7XG5cdFx0XCJpbnB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwidG9rZW5JZFwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwibmFtZVwiOiBcInRva2VuVVJJXCIsXG5cdFx0XCJvdXRwdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJzdHJpbmdcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwiXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInN0cmluZ1wiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcInN0YXRlTXV0YWJpbGl0eVwiOiBcInZpZXdcIixcblx0XHRcInR5cGVcIjogXCJmdW5jdGlvblwiXG5cdH1cbl0iLCJpbXBvcnQgeyB1c2VTdGF0ZSB9IGZyb20gJ3JlYWN0J1xuaW1wb3J0IHsgZXRoZXJzIH0gZnJvbSAnZXRoZXJzJ1xuaW1wb3J0IHsgY3JlYXRlIGFzIGlwZnNIdHRwQ2xpZW50IH0gZnJvbSAnaXBmcy1odHRwLWNsaWVudCdcbmltcG9ydCB7IHVzZVJvdXRlciB9IGZyb20gJ25leHQvcm91dGVyJ1xuaW1wb3J0IFdlYjNNb2RhbCBmcm9tICd3ZWIzbW9kYWwnXG5pbXBvcnQgd2ViMyBmcm9tICd3ZWIzJ1xuXG5jb25zdCBjbGllbnQgPSBpcGZzSHR0cENsaWVudCgnaHR0cHM6Ly9pcGZzLmluZnVyYS5pbzo1MDAxL2FwaS92MCcpXG5cbmltcG9ydCB7XG4gIG5mdG1hcmtldGFkZHJlc3MsIG5mdGFkZHJlc3MsbmZ0YWJpLG5mdG1hcmtldGFiaVxufSBmcm9tICcuLi9jb25maWcnXG5cblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gSG9tZSgpIHtcbiAgY29uc3QgW2ZpbGVVcmwsIHNldEZpbGVVcmxdID0gdXNlU3RhdGUobnVsbClcbiAgY29uc3QgW2Zvcm1JbnB1dCwgdXBkYXRlRm9ybUlucHV0XSA9IHVzZVN0YXRlKHsgcHJpY2U6ICcnLCBuYW1lOiAnJywgZGVzY3JpcHRpb246ICcnIH0pXG4gIGNvbnN0IHJvdXRlciA9IHVzZVJvdXRlcigpXG5cbiAgYXN5bmMgZnVuY3Rpb24gY3JlYXRlU2FsZSh1cmwpIHtcbiAgICBjb25zdCB3ZWIzTW9kYWwgPSBuZXcgV2ViM01vZGFsKHtcbiAgICAgIG5ldHdvcms6IFwibWFpbm5ldFwiLFxuICAgICAgY2FjaGVQcm92aWRlcjogdHJ1ZSxcbiAgICB9KTtcbiAgICBjb25zdCBjb25uZWN0aW9uID0gYXdhaXQgd2ViM01vZGFsLmNvbm5lY3QoKVxuICAgIGNvbnN0IHByb3ZpZGVyID0gbmV3IGV0aGVycy5wcm92aWRlcnMuV2ViM1Byb3ZpZGVyKGNvbm5lY3Rpb24pICAgIFxuICAgIGNvbnN0IHNpZ25lciA9IHByb3ZpZGVyLmdldFNpZ25lcigpXG4gICAgXG4gICAgbGV0IGNvbnRyYWN0ID0gbmV3IGV0aGVycy5Db250cmFjdChuZnRhZGRyZXNzLG5mdGFiaSwgc2lnbmVyKVxuICAgIGxldCB0cmFuc2FjdGlvbiA9IGF3YWl0IGNvbnRyYWN0LmNyZWF0ZVRva2VuKHVybClcbiAgICBsZXQgdHggPSBhd2FpdCB0cmFuc2FjdGlvbi53YWl0KClcbiAgICBsZXQgZXZlbnQgPSB0eC5ldmVudHNbMF1cbiAgICBsZXQgdmFsdWUgPSBldmVudC5hcmdzWzJdXG4gICAgbGV0IHRva2VuSWQgPSB2YWx1ZS50b051bWJlcigpXG4gICAgY29uc3QgcHJpY2UgPSB3ZWIzLnV0aWxzLnRvV2VpKGZvcm1JbnB1dC5wcmljZSwgJ2V0aGVyJylcbiAgXG4gICAgY29uc3QgbGlzdGluZ1ByaWNlID0gd2ViMy51dGlscy50b1dlaSgnMC4xJywgJ2V0aGVyJylcblxuICAgIGNvbnRyYWN0ID0gbmV3IGV0aGVycy5Db250cmFjdChuZnRtYXJrZXRhZGRyZXNzLCBuZnRtYXJrZXRhYmksIHNpZ25lcilcbiAgICB0cmFuc2FjdGlvbiA9IGF3YWl0IGNvbnRyYWN0LmNyZWF0ZU1hcmtldEl0ZW0obmZ0YWRkcmVzcywgdG9rZW5JZCwgcHJpY2UsIHsgdmFsdWU6IGxpc3RpbmdQcmljZSB9KVxuICAgIFxuICAgIGF3YWl0IHRyYW5zYWN0aW9uLndhaXQoKVxuICAgIHJvdXRlci5wdXNoKCcvJylcbiAgfVxuICBhc3luYyBmdW5jdGlvbiBvbkNoYW5nZShlKSB7XG4gICAgY29uc3QgZmlsZSA9IGUudGFyZ2V0LmZpbGVzWzBdO1xuICAgIHRyeSB7XG4gICAgICBjb25zdCBhZGRlZCA9IGF3YWl0IGNsaWVudC5hZGQoXG4gICAgICAgIGZpbGUsXG4gICAgICAgIHtcbiAgICAgICAgICBwcm9ncmVzczogKHByb2cpID0+IGNvbnNvbGUubG9nKGByZWNlaXZlZDogJHtwcm9nfWApXG4gICAgICAgIH1cbiAgICAgIClcbiAgICAgIGNvbnN0IHVybCA9IGBodHRwczovL2lwZnMuaW5mdXJhLmlvL2lwZnMvJHthZGRlZC5wYXRofWBcbiAgICAgIHNldEZpbGVVcmwodXJsKVxuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICBjb25zb2xlLmxvZygnRXJyb3IgdXBsb2FkaW5nIGZpbGU6ICcsIGVycm9yKTtcbiAgICB9ICBcbiAgfVxuICBhc3luYyBmdW5jdGlvbiBjcmVhdGVNYXJrZXQoKSB7XG4gICAgY29uc3QgeyBuYW1lLCBkZXNjcmlwdGlvbiwgcHJpY2UgfSA9IGZvcm1JbnB1dFxuICAgIGlmICghbmFtZSB8fCAhZGVzY3JpcHRpb24gfHwgIXByaWNlIHx8ICFmaWxlVXJsKSByZXR1cm5cbiAgICAvLyBmaXJzdCwgdXBsb2FkIHRvIElQRlNcbiAgICBjb25zdCBkYXRhID0gSlNPTi5zdHJpbmdpZnkoe1xuICAgICAgbmFtZSwgZGVzY3JpcHRpb24sIGltYWdlOiBmaWxlVXJsXG4gICAgfSlcbiAgICB0cnkge1xuICAgICAgY29uc3QgYWRkZWQgPSBhd2FpdCBjbGllbnQuYWRkKGRhdGEpXG4gICAgICBjb25zdCB1cmwgPSBgaHR0cHM6Ly9pcGZzLmluZnVyYS5pby9pcGZzLyR7YWRkZWQucGF0aH1gXG4gICAgICBjcmVhdGVTYWxlKHVybClcbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgY29uc29sZS5sb2coJ0Vycm9yIHVwbG9hZGluZyBmaWxlOiAnLCBlcnJvcik7XG4gICAgfSAgXG4gIH1cblxuICByZXR1cm4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPVwiZmxleCBqdXN0aWZ5LWNlbnRlclwiPlxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJ3LTEvMiBmbGV4IGZsZXgtY29sIHBiLTEyXCI+XG4gICAgICAgIDxpbnB1dCBcbiAgICAgICAgICBwbGFjZWhvbGRlcj1cIk5GVCBOYW1lXCJcbiAgICAgICAgICBjbGFzc05hbWU9XCJtdC04IGJvcmRlciByb3VuZGVkIHAtNFwiXG4gICAgICAgICAgb25DaGFuZ2U9e2UgPT4gdXBkYXRlRm9ybUlucHV0KHsgLi4uZm9ybUlucHV0LCBuYW1lOiBlLnRhcmdldC52YWx1ZSB9KX1cbiAgICAgICAgLz5cbiAgICAgICAgPGlucHV0XG4gICAgICAgICAgcGxhY2Vob2xkZXI9XCJORlQgRGVzY3JpcHRpb25cIlxuICAgICAgICAgIGNsYXNzTmFtZT1cIm10LTIgYm9yZGVyIHJvdW5kZWQgcC00XCJcbiAgICAgICAgICBvbkNoYW5nZT17ZSA9PiB1cGRhdGVGb3JtSW5wdXQoeyAuLi5mb3JtSW5wdXQsIGRlc2NyaXB0aW9uOiBlLnRhcmdldC52YWx1ZSB9KX1cbiAgICAgICAgLz5cbiAgICAgICAgPGlucHV0XG4gICAgICAgICAgcGxhY2Vob2xkZXI9XCJORlQgUHJpY2UgaW4gRXRoXCJcbiAgICAgICAgICBjbGFzc05hbWU9XCJtdC0yIGJvcmRlciByb3VuZGVkIHAtNFwiXG4gICAgICAgICAgb25DaGFuZ2U9e2UgPT4gdXBkYXRlRm9ybUlucHV0KHsgLi4uZm9ybUlucHV0LCBwcmljZTogZS50YXJnZXQudmFsdWUgfSl9XG4gICAgICAgIC8+XG4gICAgICAgIDxpbnB1dFxuICAgICAgICAgIHR5cGU9XCJmaWxlXCJcbiAgICAgICAgICBuYW1lPVwiTkZUXCJcbiAgICAgICAgICBjbGFzc05hbWU9XCJteS00XCJcbiAgICAgICAgICBvbkNoYW5nZT17b25DaGFuZ2V9XG4gICAgICAgIC8+XG4gICAgICAgIHtcbiAgICAgICAgICBmaWxlVXJsICYmIChcbiAgICAgICAgICAgIDxpbWcgY2xhc3NOYW1lPVwicm91bmRlZCBtdC00XCIgd2lkdGg9XCIzNTBcIiBzcmM9e2ZpbGVVcmx9IC8+XG4gICAgICAgICAgKVxuICAgICAgICB9XG4gICAgICAgIDxidXR0b24gb25DbGljaz17Y3JlYXRlTWFya2V0fSBjbGFzc05hbWU9XCJtdC00IGJnLWJsdWUtNTAwIHRleHQtd2hpdGUgcm91bmRlZCBwLTQgc2hhZG93LWxnXCI+XG4gICAgICAgICAgQ3JlYXRlIE5GVFxuICAgICAgICA8L2J1dHRvbj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApXG59XG4iLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJldGhlcnNcIik7OyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImlwZnMtaHR0cC1jbGllbnRcIik7OyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIm5leHQvcm91dGVyXCIpOzsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdFwiKTs7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3QvanN4LWRldi1ydW50aW1lXCIpOzsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJ3ZWIzXCIpOzsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJ3ZWIzbW9kYWxcIik7OyJdLCJzb3VyY2VSb290IjoiIn0=