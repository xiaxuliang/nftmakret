(function() {
var exports = {};
exports.id = "pages/index";
exports.ids = ["pages/index"];
exports.modules = {

/***/ "./config.js":
/*!*******************!*\
  !*** ./config.js ***!
  \*******************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "nftmarketaddress": function() { return /* binding */ nftmarketaddress; },
/* harmony export */   "nftaddress": function() { return /* binding */ nftaddress; },
/* harmony export */   "nftmarketabi": function() { return /* binding */ nftmarketabi; },
/* harmony export */   "nftabi": function() { return /* binding */ nftabi; }
/* harmony export */ });
const nftmarketaddress = "0x9106f0AF000AB0f838b025158e5832f58Fe3a35E"; //rinkeby

const nftaddress = "0xD81ab083e835FBe721DB6B5C7749dc72E2744e57"; //rinkeby

const nftmarketabi = [{
  "inputs": [],
  "stateMutability": "nonpayable",
  "type": "constructor"
}, {
  "anonymous": false,
  "inputs": [{
    "indexed": true,
    "internalType": "uint256",
    "name": "itemId",
    "type": "uint256"
  }, {
    "indexed": true,
    "internalType": "address",
    "name": "nftContract",
    "type": "address"
  }, {
    "indexed": true,
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }, {
    "indexed": false,
    "internalType": "address",
    "name": "seller",
    "type": "address"
  }, {
    "indexed": false,
    "internalType": "address",
    "name": "owner",
    "type": "address"
  }, {
    "indexed": false,
    "internalType": "uint256",
    "name": "price",
    "type": "uint256"
  }],
  "name": "MarketItemCreated",
  "type": "event"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "nftContract",
    "type": "address"
  }, {
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }, {
    "internalType": "uint256",
    "name": "price",
    "type": "uint256"
  }],
  "name": "createMarketItem",
  "outputs": [],
  "stateMutability": "payable",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "nftContract",
    "type": "address"
  }, {
    "internalType": "uint256",
    "name": "itemId",
    "type": "uint256"
  }],
  "name": "createMarketSale",
  "outputs": [],
  "stateMutability": "payable",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "uint256",
    "name": "itemId",
    "type": "uint256"
  }],
  "name": "fetchMarketItem",
  "outputs": [{
    "components": [{
      "internalType": "uint256",
      "name": "itemId",
      "type": "uint256"
    }, {
      "internalType": "address",
      "name": "nftContract",
      "type": "address"
    }, {
      "internalType": "uint256",
      "name": "tokenId",
      "type": "uint256"
    }, {
      "internalType": "address payable",
      "name": "seller",
      "type": "address"
    }, {
      "internalType": "address payable",
      "name": "owner",
      "type": "address"
    }, {
      "internalType": "uint256",
      "name": "price",
      "type": "uint256"
    }],
    "internalType": "struct NFTMarket.MarketItem",
    "name": "",
    "type": "tuple"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "fetchMarketItems",
  "outputs": [{
    "components": [{
      "internalType": "uint256",
      "name": "itemId",
      "type": "uint256"
    }, {
      "internalType": "address",
      "name": "nftContract",
      "type": "address"
    }, {
      "internalType": "uint256",
      "name": "tokenId",
      "type": "uint256"
    }, {
      "internalType": "address payable",
      "name": "seller",
      "type": "address"
    }, {
      "internalType": "address payable",
      "name": "owner",
      "type": "address"
    }, {
      "internalType": "uint256",
      "name": "price",
      "type": "uint256"
    }],
    "internalType": "struct NFTMarket.MarketItem[]",
    "name": "",
    "type": "tuple[]"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "fetchMyNFTs",
  "outputs": [{
    "components": [{
      "internalType": "uint256",
      "name": "itemId",
      "type": "uint256"
    }, {
      "internalType": "address",
      "name": "nftContract",
      "type": "address"
    }, {
      "internalType": "uint256",
      "name": "tokenId",
      "type": "uint256"
    }, {
      "internalType": "address payable",
      "name": "seller",
      "type": "address"
    }, {
      "internalType": "address payable",
      "name": "owner",
      "type": "address"
    }, {
      "internalType": "uint256",
      "name": "price",
      "type": "uint256"
    }],
    "internalType": "struct NFTMarket.MarketItem[]",
    "name": "",
    "type": "tuple[]"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "uint256",
    "name": "marketItemId",
    "type": "uint256"
  }],
  "name": "getMarketItem",
  "outputs": [{
    "components": [{
      "internalType": "uint256",
      "name": "itemId",
      "type": "uint256"
    }, {
      "internalType": "address",
      "name": "nftContract",
      "type": "address"
    }, {
      "internalType": "uint256",
      "name": "tokenId",
      "type": "uint256"
    }, {
      "internalType": "address payable",
      "name": "seller",
      "type": "address"
    }, {
      "internalType": "address payable",
      "name": "owner",
      "type": "address"
    }, {
      "internalType": "uint256",
      "name": "price",
      "type": "uint256"
    }],
    "internalType": "struct NFTMarket.MarketItem",
    "name": "",
    "type": "tuple"
  }],
  "stateMutability": "view",
  "type": "function"
}];
const nftabi = [{
  "inputs": [{
    "internalType": "address",
    "name": "marketplaceAddress",
    "type": "address"
  }],
  "stateMutability": "nonpayable",
  "type": "constructor"
}, {
  "anonymous": false,
  "inputs": [{
    "indexed": true,
    "internalType": "address",
    "name": "owner",
    "type": "address"
  }, {
    "indexed": true,
    "internalType": "address",
    "name": "approved",
    "type": "address"
  }, {
    "indexed": true,
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }],
  "name": "Approval",
  "type": "event"
}, {
  "anonymous": false,
  "inputs": [{
    "indexed": true,
    "internalType": "address",
    "name": "owner",
    "type": "address"
  }, {
    "indexed": true,
    "internalType": "address",
    "name": "operator",
    "type": "address"
  }, {
    "indexed": false,
    "internalType": "bool",
    "name": "approved",
    "type": "bool"
  }],
  "name": "ApprovalForAll",
  "type": "event"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "to",
    "type": "address"
  }, {
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }],
  "name": "approve",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "string",
    "name": "tokenURI",
    "type": "string"
  }],
  "name": "createToken",
  "outputs": [{
    "internalType": "uint256",
    "name": "",
    "type": "uint256"
  }],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "from",
    "type": "address"
  }, {
    "internalType": "address",
    "name": "to",
    "type": "address"
  }, {
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }],
  "name": "safeTransferFrom",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "from",
    "type": "address"
  }, {
    "internalType": "address",
    "name": "to",
    "type": "address"
  }, {
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }, {
    "internalType": "bytes",
    "name": "_data",
    "type": "bytes"
  }],
  "name": "safeTransferFrom",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "operator",
    "type": "address"
  }, {
    "internalType": "bool",
    "name": "approved",
    "type": "bool"
  }],
  "name": "setApprovalForAll",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "anonymous": false,
  "inputs": [{
    "indexed": true,
    "internalType": "address",
    "name": "from",
    "type": "address"
  }, {
    "indexed": true,
    "internalType": "address",
    "name": "to",
    "type": "address"
  }, {
    "indexed": true,
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }],
  "name": "Transfer",
  "type": "event"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "from",
    "type": "address"
  }, {
    "internalType": "address",
    "name": "to",
    "type": "address"
  }, {
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }],
  "name": "transferFrom",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "owner",
    "type": "address"
  }],
  "name": "balanceOf",
  "outputs": [{
    "internalType": "uint256",
    "name": "",
    "type": "uint256"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }],
  "name": "getApproved",
  "outputs": [{
    "internalType": "address",
    "name": "",
    "type": "address"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "owner",
    "type": "address"
  }, {
    "internalType": "address",
    "name": "operator",
    "type": "address"
  }],
  "name": "isApprovedForAll",
  "outputs": [{
    "internalType": "bool",
    "name": "",
    "type": "bool"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "name",
  "outputs": [{
    "internalType": "string",
    "name": "",
    "type": "string"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }],
  "name": "ownerOf",
  "outputs": [{
    "internalType": "address",
    "name": "",
    "type": "address"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "bytes4",
    "name": "interfaceId",
    "type": "bytes4"
  }],
  "name": "supportsInterface",
  "outputs": [{
    "internalType": "bool",
    "name": "",
    "type": "bool"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "symbol",
  "outputs": [{
    "internalType": "string",
    "name": "",
    "type": "string"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }],
  "name": "tokenURI",
  "outputs": [{
    "internalType": "string",
    "name": "",
    "type": "string"
  }],
  "stateMutability": "view",
  "type": "function"
}];

/***/ }),

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ Home; }
/* harmony export */ });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var ethers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ethers */ "ethers");
/* harmony import */ var ethers__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(ethers__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var web3__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! web3 */ "web3");
/* harmony import */ var web3__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(web3__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var web3modal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! web3modal */ "web3modal");
/* harmony import */ var web3modal__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(web3modal__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var fortmatic__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! fortmatic */ "fortmatic");
/* harmony import */ var fortmatic__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(fortmatic__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _walletconnect_web3_provider__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @walletconnect/web3-provider */ "@walletconnect/web3-provider");
/* harmony import */ var _walletconnect_web3_provider__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_walletconnect_web3_provider__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../config */ "./config.js");

var _jsxFileName = "E:\\marketplace-complete\\pages\\index.js";








function Home() {
  const {
    0: nfts,
    1: setNfts
  } = (0,react__WEBPACK_IMPORTED_MODULE_2__.useState)([]);
  const {
    0: loaded,
    1: setLoaded
  } = (0,react__WEBPACK_IMPORTED_MODULE_2__.useState)('not-loaded');
  (0,react__WEBPACK_IMPORTED_MODULE_2__.useEffect)(() => {
    loadNFTs();
  }, []);

  async function loadNFTs() {
    const providerOptions = {
      fortmatic: {
        package: (fortmatic__WEBPACK_IMPORTED_MODULE_6___default()),
        options: {
          // Mikko's TESTNET api key
          key: "pk_test_391E26A3B43A3350"
        }
      },
      walletconnect: {
        package: (_walletconnect_web3_provider__WEBPACK_IMPORTED_MODULE_7___default()),
        // required
        options: {
          infuraId: "INFURA_ID" // required

        }
      }
    };
    const web3Modal = new (web3modal__WEBPACK_IMPORTED_MODULE_5___default())({
      network: "mainnet",
      cacheProvider: true,
      disableInjectedProvider: false,
      providerOptions: providerOptions // required

    });
    const connection = await web3Modal.connect();
    const provider = new ethers__WEBPACK_IMPORTED_MODULE_1__.ethers.providers.Web3Provider(connection);
    const tokenContract = new ethers__WEBPACK_IMPORTED_MODULE_1__.ethers.Contract(_config__WEBPACK_IMPORTED_MODULE_8__.nftaddress, _config__WEBPACK_IMPORTED_MODULE_8__.nftabi, provider);
    const marketContract = new ethers__WEBPACK_IMPORTED_MODULE_1__.ethers.Contract(_config__WEBPACK_IMPORTED_MODULE_8__.nftmarketaddress, _config__WEBPACK_IMPORTED_MODULE_8__.nftmarketabi, provider);
    const data = await marketContract.fetchMarketItems();
    const items = await Promise.all(data.map(async i => {
      const tokenUri = await tokenContract.tokenURI(i.tokenId);
      const meta = await axios__WEBPACK_IMPORTED_MODULE_3___default().get(tokenUri);
      let price = web3__WEBPACK_IMPORTED_MODULE_4___default().utils.fromWei(i.price.toString(), 'ether');
      let item = {
        price,
        tokenId: i.tokenId.toNumber(),
        seller: i.seller,
        owner: i.owner,
        image: meta.data.image,
        name: meta.data.name,
        description: meta.data.description
      };
      return item;
    }));
    console.log('items: ', items);
    setNfts(items);
    setLoaded('loaded');
  }

  async function buyNft(nft) {
    const web3Modal = new (web3modal__WEBPACK_IMPORTED_MODULE_5___default())({
      network: "mainnet",
      cacheProvider: true
    });
    const connection = await web3Modal.connect();
    const provider = new ethers__WEBPACK_IMPORTED_MODULE_1__.ethers.providers.Web3Provider(connection);
    const signer = provider.getSigner();
    const contract = new ethers__WEBPACK_IMPORTED_MODULE_1__.ethers.Contract(_config__WEBPACK_IMPORTED_MODULE_8__.nftmarketaddress, _config__WEBPACK_IMPORTED_MODULE_8__.nftmarketabi, signer);
    const price = web3__WEBPACK_IMPORTED_MODULE_4___default().utils.toWei(nft.price.toString(), 'ether');
    console.log('price: ', price);
    const transaction = await contract.createMarketSale(_config__WEBPACK_IMPORTED_MODULE_8__.nftaddress, nft.tokenId, {
      value: price
    });
    await transaction.wait();
    loadNFTs();
  }

  if (loaded === 'loaded' && !nfts.length) return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h1", {
    className: "p-20 text-4xl",
    children: "No NFTs!"
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 90,
    columnNumber: 52
  }, this);
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
    className: "flex justify-center",
    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      style: {
        width: 900
      },
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "grid grid-cols-2 gap-4 pt-8",
        children: nfts.map((nft, i) => /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "border p-4 shadow",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("img", {
            src: nft.image,
            className: "rounded"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 98,
            columnNumber: 17
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
            className: "text-2xl my-4 font-bold",
            children: ["Price: ", nft.price]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 99,
            columnNumber: 17
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
            className: "text-2xl my-4 font-bold",
            children: ["Name: ", nft.name]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 100,
            columnNumber: 17
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
            className: "text-2xl my-4 font-bold",
            children: ["Description: ", nft.description]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 101,
            columnNumber: 17
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
            className: "bg-green-600 text-white py-2 px-12 rounded",
            onClick: () => buyNft(nft),
            children: "Buy NFT"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 102,
            columnNumber: 17
          }, this)]
        }, i, true, {
          fileName: _jsxFileName,
          lineNumber: 97,
          columnNumber: 15
        }, this))
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 94,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 93,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 92,
    columnNumber: 5
  }, this);
}

/***/ }),

/***/ "@walletconnect/web3-provider":
/*!***********************************************!*\
  !*** external "@walletconnect/web3-provider" ***!
  \***********************************************/
/***/ (function(module) {

"use strict";
module.exports = require("@walletconnect/web3-provider");;

/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/***/ (function(module) {

"use strict";
module.exports = require("axios");;

/***/ }),

/***/ "ethers":
/*!*************************!*\
  !*** external "ethers" ***!
  \*************************/
/***/ (function(module) {

"use strict";
module.exports = require("ethers");;

/***/ }),

/***/ "fortmatic":
/*!****************************!*\
  !*** external "fortmatic" ***!
  \****************************/
/***/ (function(module) {

"use strict";
module.exports = require("fortmatic");;

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ (function(module) {

"use strict";
module.exports = require("react");;

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ (function(module) {

"use strict";
module.exports = require("react/jsx-dev-runtime");;

/***/ }),

/***/ "web3":
/*!***********************!*\
  !*** external "web3" ***!
  \***********************/
/***/ (function(module) {

"use strict";
module.exports = require("web3");;

/***/ }),

/***/ "web3modal":
/*!****************************!*\
  !*** external "web3modal" ***!
  \****************************/
/***/ (function(module) {

"use strict";
module.exports = require("web3modal");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
var __webpack_exports__ = (__webpack_exec__("./pages/index.js"));
module.exports = __webpack_exports__;

})();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9tYXJrZXRwbGFjZS1jb21wbGV0ZS8uL2NvbmZpZy5qcyIsIndlYnBhY2s6Ly9tYXJrZXRwbGFjZS1jb21wbGV0ZS8uL3BhZ2VzL2luZGV4LmpzIiwid2VicGFjazovL21hcmtldHBsYWNlLWNvbXBsZXRlL2V4dGVybmFsIFwiQHdhbGxldGNvbm5lY3Qvd2ViMy1wcm92aWRlclwiIiwid2VicGFjazovL21hcmtldHBsYWNlLWNvbXBsZXRlL2V4dGVybmFsIFwiYXhpb3NcIiIsIndlYnBhY2s6Ly9tYXJrZXRwbGFjZS1jb21wbGV0ZS9leHRlcm5hbCBcImV0aGVyc1wiIiwid2VicGFjazovL21hcmtldHBsYWNlLWNvbXBsZXRlL2V4dGVybmFsIFwiZm9ydG1hdGljXCIiLCJ3ZWJwYWNrOi8vbWFya2V0cGxhY2UtY29tcGxldGUvZXh0ZXJuYWwgXCJyZWFjdFwiIiwid2VicGFjazovL21hcmtldHBsYWNlLWNvbXBsZXRlL2V4dGVybmFsIFwicmVhY3QvanN4LWRldi1ydW50aW1lXCIiLCJ3ZWJwYWNrOi8vbWFya2V0cGxhY2UtY29tcGxldGUvZXh0ZXJuYWwgXCJ3ZWIzXCIiLCJ3ZWJwYWNrOi8vbWFya2V0cGxhY2UtY29tcGxldGUvZXh0ZXJuYWwgXCJ3ZWIzbW9kYWxcIiJdLCJuYW1lcyI6WyJuZnRtYXJrZXRhZGRyZXNzIiwibmZ0YWRkcmVzcyIsIm5mdG1hcmtldGFiaSIsIm5mdGFiaSIsIkhvbWUiLCJuZnRzIiwic2V0TmZ0cyIsInVzZVN0YXRlIiwibG9hZGVkIiwic2V0TG9hZGVkIiwidXNlRWZmZWN0IiwibG9hZE5GVHMiLCJwcm92aWRlck9wdGlvbnMiLCJmb3J0bWF0aWMiLCJwYWNrYWdlIiwiRm9ydG1hdGljIiwib3B0aW9ucyIsImtleSIsIndhbGxldGNvbm5lY3QiLCJXYWxsZXRDb25uZWN0UHJvdmlkZXIiLCJpbmZ1cmFJZCIsIndlYjNNb2RhbCIsIldlYjNNb2RhbCIsIm5ldHdvcmsiLCJjYWNoZVByb3ZpZGVyIiwiZGlzYWJsZUluamVjdGVkUHJvdmlkZXIiLCJjb25uZWN0aW9uIiwiY29ubmVjdCIsInByb3ZpZGVyIiwiZXRoZXJzIiwidG9rZW5Db250cmFjdCIsIm1hcmtldENvbnRyYWN0IiwiZGF0YSIsImZldGNoTWFya2V0SXRlbXMiLCJpdGVtcyIsIlByb21pc2UiLCJhbGwiLCJtYXAiLCJpIiwidG9rZW5VcmkiLCJ0b2tlblVSSSIsInRva2VuSWQiLCJtZXRhIiwiYXhpb3MiLCJwcmljZSIsIndlYjMiLCJ0b1N0cmluZyIsIml0ZW0iLCJ0b051bWJlciIsInNlbGxlciIsIm93bmVyIiwiaW1hZ2UiLCJuYW1lIiwiZGVzY3JpcHRpb24iLCJjb25zb2xlIiwibG9nIiwiYnV5TmZ0IiwibmZ0Iiwic2lnbmVyIiwiZ2V0U2lnbmVyIiwiY29udHJhY3QiLCJ0cmFuc2FjdGlvbiIsImNyZWF0ZU1hcmtldFNhbGUiLCJ2YWx1ZSIsIndhaXQiLCJsZW5ndGgiLCJ3aWR0aCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBTyxNQUFNQSxnQkFBZ0IsR0FBRyw0Q0FBekIsQyxDQUF1RTs7QUFDdkUsTUFBTUMsVUFBVSxHQUFHLDRDQUFuQixDLENBQWlFOztBQUVqRSxNQUFNQyxZQUFZLEdBQUcsQ0FDM0I7QUFDQyxZQUFVLEVBRFg7QUFFQyxxQkFBbUIsWUFGcEI7QUFHQyxVQUFRO0FBSFQsQ0FEMkIsRUFNM0I7QUFDQyxlQUFhLEtBRGQ7QUFFQyxZQUFVLENBQ1Q7QUFDQyxlQUFXLElBRFo7QUFFQyxvQkFBZ0IsU0FGakI7QUFHQyxZQUFRLFFBSFQ7QUFJQyxZQUFRO0FBSlQsR0FEUyxFQU9UO0FBQ0MsZUFBVyxJQURaO0FBRUMsb0JBQWdCLFNBRmpCO0FBR0MsWUFBUSxhQUhUO0FBSUMsWUFBUTtBQUpULEdBUFMsRUFhVDtBQUNDLGVBQVcsSUFEWjtBQUVDLG9CQUFnQixTQUZqQjtBQUdDLFlBQVEsU0FIVDtBQUlDLFlBQVE7QUFKVCxHQWJTLEVBbUJUO0FBQ0MsZUFBVyxLQURaO0FBRUMsb0JBQWdCLFNBRmpCO0FBR0MsWUFBUSxRQUhUO0FBSUMsWUFBUTtBQUpULEdBbkJTLEVBeUJUO0FBQ0MsZUFBVyxLQURaO0FBRUMsb0JBQWdCLFNBRmpCO0FBR0MsWUFBUSxPQUhUO0FBSUMsWUFBUTtBQUpULEdBekJTLEVBK0JUO0FBQ0MsZUFBVyxLQURaO0FBRUMsb0JBQWdCLFNBRmpCO0FBR0MsWUFBUSxPQUhUO0FBSUMsWUFBUTtBQUpULEdBL0JTLENBRlg7QUF3Q0MsVUFBUSxtQkF4Q1Q7QUF5Q0MsVUFBUTtBQXpDVCxDQU4yQixFQWlEM0I7QUFDQyxZQUFVLENBQ1Q7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLGFBRlQ7QUFHQyxZQUFRO0FBSFQsR0FEUyxFQU1UO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxTQUZUO0FBR0MsWUFBUTtBQUhULEdBTlMsRUFXVDtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsT0FGVDtBQUdDLFlBQVE7QUFIVCxHQVhTLENBRFg7QUFrQkMsVUFBUSxrQkFsQlQ7QUFtQkMsYUFBVyxFQW5CWjtBQW9CQyxxQkFBbUIsU0FwQnBCO0FBcUJDLFVBQVE7QUFyQlQsQ0FqRDJCLEVBd0UzQjtBQUNDLFlBQVUsQ0FDVDtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsYUFGVDtBQUdDLFlBQVE7QUFIVCxHQURTLEVBTVQ7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLFFBRlQ7QUFHQyxZQUFRO0FBSFQsR0FOUyxDQURYO0FBYUMsVUFBUSxrQkFiVDtBQWNDLGFBQVcsRUFkWjtBQWVDLHFCQUFtQixTQWZwQjtBQWdCQyxVQUFRO0FBaEJULENBeEUyQixFQTBGM0I7QUFDQyxZQUFVLENBQ1Q7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLFFBRlQ7QUFHQyxZQUFRO0FBSFQsR0FEUyxDQURYO0FBUUMsVUFBUSxpQkFSVDtBQVNDLGFBQVcsQ0FDVjtBQUNDLGtCQUFjLENBQ2I7QUFDQyxzQkFBZ0IsU0FEakI7QUFFQyxjQUFRLFFBRlQ7QUFHQyxjQUFRO0FBSFQsS0FEYSxFQU1iO0FBQ0Msc0JBQWdCLFNBRGpCO0FBRUMsY0FBUSxhQUZUO0FBR0MsY0FBUTtBQUhULEtBTmEsRUFXYjtBQUNDLHNCQUFnQixTQURqQjtBQUVDLGNBQVEsU0FGVDtBQUdDLGNBQVE7QUFIVCxLQVhhLEVBZ0JiO0FBQ0Msc0JBQWdCLGlCQURqQjtBQUVDLGNBQVEsUUFGVDtBQUdDLGNBQVE7QUFIVCxLQWhCYSxFQXFCYjtBQUNDLHNCQUFnQixpQkFEakI7QUFFQyxjQUFRLE9BRlQ7QUFHQyxjQUFRO0FBSFQsS0FyQmEsRUEwQmI7QUFDQyxzQkFBZ0IsU0FEakI7QUFFQyxjQUFRLE9BRlQ7QUFHQyxjQUFRO0FBSFQsS0ExQmEsQ0FEZjtBQWlDQyxvQkFBZ0IsNkJBakNqQjtBQWtDQyxZQUFRLEVBbENUO0FBbUNDLFlBQVE7QUFuQ1QsR0FEVSxDQVRaO0FBZ0RDLHFCQUFtQixNQWhEcEI7QUFpREMsVUFBUTtBQWpEVCxDQTFGMkIsRUE2STNCO0FBQ0MsWUFBVSxFQURYO0FBRUMsVUFBUSxrQkFGVDtBQUdDLGFBQVcsQ0FDVjtBQUNDLGtCQUFjLENBQ2I7QUFDQyxzQkFBZ0IsU0FEakI7QUFFQyxjQUFRLFFBRlQ7QUFHQyxjQUFRO0FBSFQsS0FEYSxFQU1iO0FBQ0Msc0JBQWdCLFNBRGpCO0FBRUMsY0FBUSxhQUZUO0FBR0MsY0FBUTtBQUhULEtBTmEsRUFXYjtBQUNDLHNCQUFnQixTQURqQjtBQUVDLGNBQVEsU0FGVDtBQUdDLGNBQVE7QUFIVCxLQVhhLEVBZ0JiO0FBQ0Msc0JBQWdCLGlCQURqQjtBQUVDLGNBQVEsUUFGVDtBQUdDLGNBQVE7QUFIVCxLQWhCYSxFQXFCYjtBQUNDLHNCQUFnQixpQkFEakI7QUFFQyxjQUFRLE9BRlQ7QUFHQyxjQUFRO0FBSFQsS0FyQmEsRUEwQmI7QUFDQyxzQkFBZ0IsU0FEakI7QUFFQyxjQUFRLE9BRlQ7QUFHQyxjQUFRO0FBSFQsS0ExQmEsQ0FEZjtBQWlDQyxvQkFBZ0IsK0JBakNqQjtBQWtDQyxZQUFRLEVBbENUO0FBbUNDLFlBQVE7QUFuQ1QsR0FEVSxDQUhaO0FBMENDLHFCQUFtQixNQTFDcEI7QUEyQ0MsVUFBUTtBQTNDVCxDQTdJMkIsRUEwTDNCO0FBQ0MsWUFBVSxFQURYO0FBRUMsVUFBUSxhQUZUO0FBR0MsYUFBVyxDQUNWO0FBQ0Msa0JBQWMsQ0FDYjtBQUNDLHNCQUFnQixTQURqQjtBQUVDLGNBQVEsUUFGVDtBQUdDLGNBQVE7QUFIVCxLQURhLEVBTWI7QUFDQyxzQkFBZ0IsU0FEakI7QUFFQyxjQUFRLGFBRlQ7QUFHQyxjQUFRO0FBSFQsS0FOYSxFQVdiO0FBQ0Msc0JBQWdCLFNBRGpCO0FBRUMsY0FBUSxTQUZUO0FBR0MsY0FBUTtBQUhULEtBWGEsRUFnQmI7QUFDQyxzQkFBZ0IsaUJBRGpCO0FBRUMsY0FBUSxRQUZUO0FBR0MsY0FBUTtBQUhULEtBaEJhLEVBcUJiO0FBQ0Msc0JBQWdCLGlCQURqQjtBQUVDLGNBQVEsT0FGVDtBQUdDLGNBQVE7QUFIVCxLQXJCYSxFQTBCYjtBQUNDLHNCQUFnQixTQURqQjtBQUVDLGNBQVEsT0FGVDtBQUdDLGNBQVE7QUFIVCxLQTFCYSxDQURmO0FBaUNDLG9CQUFnQiwrQkFqQ2pCO0FBa0NDLFlBQVEsRUFsQ1Q7QUFtQ0MsWUFBUTtBQW5DVCxHQURVLENBSFo7QUEwQ0MscUJBQW1CLE1BMUNwQjtBQTJDQyxVQUFRO0FBM0NULENBMUwyQixFQXVPM0I7QUFDQyxZQUFVLENBQ1Q7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLGNBRlQ7QUFHQyxZQUFRO0FBSFQsR0FEUyxDQURYO0FBUUMsVUFBUSxlQVJUO0FBU0MsYUFBVyxDQUNWO0FBQ0Msa0JBQWMsQ0FDYjtBQUNDLHNCQUFnQixTQURqQjtBQUVDLGNBQVEsUUFGVDtBQUdDLGNBQVE7QUFIVCxLQURhLEVBTWI7QUFDQyxzQkFBZ0IsU0FEakI7QUFFQyxjQUFRLGFBRlQ7QUFHQyxjQUFRO0FBSFQsS0FOYSxFQVdiO0FBQ0Msc0JBQWdCLFNBRGpCO0FBRUMsY0FBUSxTQUZUO0FBR0MsY0FBUTtBQUhULEtBWGEsRUFnQmI7QUFDQyxzQkFBZ0IsaUJBRGpCO0FBRUMsY0FBUSxRQUZUO0FBR0MsY0FBUTtBQUhULEtBaEJhLEVBcUJiO0FBQ0Msc0JBQWdCLGlCQURqQjtBQUVDLGNBQVEsT0FGVDtBQUdDLGNBQVE7QUFIVCxLQXJCYSxFQTBCYjtBQUNDLHNCQUFnQixTQURqQjtBQUVDLGNBQVEsT0FGVDtBQUdDLGNBQVE7QUFIVCxLQTFCYSxDQURmO0FBaUNDLG9CQUFnQiw2QkFqQ2pCO0FBa0NDLFlBQVEsRUFsQ1Q7QUFtQ0MsWUFBUTtBQW5DVCxHQURVLENBVFo7QUFnREMscUJBQW1CLE1BaERwQjtBQWlEQyxVQUFRO0FBakRULENBdk8yQixDQUFyQjtBQTJSQSxNQUFNQyxNQUFNLEdBQUcsQ0FDckI7QUFDQyxZQUFVLENBQ1Q7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLG9CQUZUO0FBR0MsWUFBUTtBQUhULEdBRFMsQ0FEWDtBQVFDLHFCQUFtQixZQVJwQjtBQVNDLFVBQVE7QUFUVCxDQURxQixFQVlyQjtBQUNDLGVBQWEsS0FEZDtBQUVDLFlBQVUsQ0FDVDtBQUNDLGVBQVcsSUFEWjtBQUVDLG9CQUFnQixTQUZqQjtBQUdDLFlBQVEsT0FIVDtBQUlDLFlBQVE7QUFKVCxHQURTLEVBT1Q7QUFDQyxlQUFXLElBRFo7QUFFQyxvQkFBZ0IsU0FGakI7QUFHQyxZQUFRLFVBSFQ7QUFJQyxZQUFRO0FBSlQsR0FQUyxFQWFUO0FBQ0MsZUFBVyxJQURaO0FBRUMsb0JBQWdCLFNBRmpCO0FBR0MsWUFBUSxTQUhUO0FBSUMsWUFBUTtBQUpULEdBYlMsQ0FGWDtBQXNCQyxVQUFRLFVBdEJUO0FBdUJDLFVBQVE7QUF2QlQsQ0FacUIsRUFxQ3JCO0FBQ0MsZUFBYSxLQURkO0FBRUMsWUFBVSxDQUNUO0FBQ0MsZUFBVyxJQURaO0FBRUMsb0JBQWdCLFNBRmpCO0FBR0MsWUFBUSxPQUhUO0FBSUMsWUFBUTtBQUpULEdBRFMsRUFPVDtBQUNDLGVBQVcsSUFEWjtBQUVDLG9CQUFnQixTQUZqQjtBQUdDLFlBQVEsVUFIVDtBQUlDLFlBQVE7QUFKVCxHQVBTLEVBYVQ7QUFDQyxlQUFXLEtBRFo7QUFFQyxvQkFBZ0IsTUFGakI7QUFHQyxZQUFRLFVBSFQ7QUFJQyxZQUFRO0FBSlQsR0FiUyxDQUZYO0FBc0JDLFVBQVEsZ0JBdEJUO0FBdUJDLFVBQVE7QUF2QlQsQ0FyQ3FCLEVBOERyQjtBQUNDLFlBQVUsQ0FDVDtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsSUFGVDtBQUdDLFlBQVE7QUFIVCxHQURTLEVBTVQ7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLFNBRlQ7QUFHQyxZQUFRO0FBSFQsR0FOUyxDQURYO0FBYUMsVUFBUSxTQWJUO0FBY0MsYUFBVyxFQWRaO0FBZUMscUJBQW1CLFlBZnBCO0FBZ0JDLFVBQVE7QUFoQlQsQ0E5RHFCLEVBZ0ZyQjtBQUNDLFlBQVUsQ0FDVDtBQUNDLG9CQUFnQixRQURqQjtBQUVDLFlBQVEsVUFGVDtBQUdDLFlBQVE7QUFIVCxHQURTLENBRFg7QUFRQyxVQUFRLGFBUlQ7QUFTQyxhQUFXLENBQ1Y7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLEVBRlQ7QUFHQyxZQUFRO0FBSFQsR0FEVSxDQVRaO0FBZ0JDLHFCQUFtQixZQWhCcEI7QUFpQkMsVUFBUTtBQWpCVCxDQWhGcUIsRUFtR3JCO0FBQ0MsWUFBVSxDQUNUO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxNQUZUO0FBR0MsWUFBUTtBQUhULEdBRFMsRUFNVDtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsSUFGVDtBQUdDLFlBQVE7QUFIVCxHQU5TLEVBV1Q7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLFNBRlQ7QUFHQyxZQUFRO0FBSFQsR0FYUyxDQURYO0FBa0JDLFVBQVEsa0JBbEJUO0FBbUJDLGFBQVcsRUFuQlo7QUFvQkMscUJBQW1CLFlBcEJwQjtBQXFCQyxVQUFRO0FBckJULENBbkdxQixFQTBIckI7QUFDQyxZQUFVLENBQ1Q7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLE1BRlQ7QUFHQyxZQUFRO0FBSFQsR0FEUyxFQU1UO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxJQUZUO0FBR0MsWUFBUTtBQUhULEdBTlMsRUFXVDtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsU0FGVDtBQUdDLFlBQVE7QUFIVCxHQVhTLEVBZ0JUO0FBQ0Msb0JBQWdCLE9BRGpCO0FBRUMsWUFBUSxPQUZUO0FBR0MsWUFBUTtBQUhULEdBaEJTLENBRFg7QUF1QkMsVUFBUSxrQkF2QlQ7QUF3QkMsYUFBVyxFQXhCWjtBQXlCQyxxQkFBbUIsWUF6QnBCO0FBMEJDLFVBQVE7QUExQlQsQ0ExSHFCLEVBc0pyQjtBQUNDLFlBQVUsQ0FDVDtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsVUFGVDtBQUdDLFlBQVE7QUFIVCxHQURTLEVBTVQ7QUFDQyxvQkFBZ0IsTUFEakI7QUFFQyxZQUFRLFVBRlQ7QUFHQyxZQUFRO0FBSFQsR0FOUyxDQURYO0FBYUMsVUFBUSxtQkFiVDtBQWNDLGFBQVcsRUFkWjtBQWVDLHFCQUFtQixZQWZwQjtBQWdCQyxVQUFRO0FBaEJULENBdEpxQixFQXdLckI7QUFDQyxlQUFhLEtBRGQ7QUFFQyxZQUFVLENBQ1Q7QUFDQyxlQUFXLElBRFo7QUFFQyxvQkFBZ0IsU0FGakI7QUFHQyxZQUFRLE1BSFQ7QUFJQyxZQUFRO0FBSlQsR0FEUyxFQU9UO0FBQ0MsZUFBVyxJQURaO0FBRUMsb0JBQWdCLFNBRmpCO0FBR0MsWUFBUSxJQUhUO0FBSUMsWUFBUTtBQUpULEdBUFMsRUFhVDtBQUNDLGVBQVcsSUFEWjtBQUVDLG9CQUFnQixTQUZqQjtBQUdDLFlBQVEsU0FIVDtBQUlDLFlBQVE7QUFKVCxHQWJTLENBRlg7QUFzQkMsVUFBUSxVQXRCVDtBQXVCQyxVQUFRO0FBdkJULENBeEtxQixFQWlNckI7QUFDQyxZQUFVLENBQ1Q7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLE1BRlQ7QUFHQyxZQUFRO0FBSFQsR0FEUyxFQU1UO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxJQUZUO0FBR0MsWUFBUTtBQUhULEdBTlMsRUFXVDtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsU0FGVDtBQUdDLFlBQVE7QUFIVCxHQVhTLENBRFg7QUFrQkMsVUFBUSxjQWxCVDtBQW1CQyxhQUFXLEVBbkJaO0FBb0JDLHFCQUFtQixZQXBCcEI7QUFxQkMsVUFBUTtBQXJCVCxDQWpNcUIsRUF3TnJCO0FBQ0MsWUFBVSxDQUNUO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxPQUZUO0FBR0MsWUFBUTtBQUhULEdBRFMsQ0FEWDtBQVFDLFVBQVEsV0FSVDtBQVNDLGFBQVcsQ0FDVjtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsRUFGVDtBQUdDLFlBQVE7QUFIVCxHQURVLENBVFo7QUFnQkMscUJBQW1CLE1BaEJwQjtBQWlCQyxVQUFRO0FBakJULENBeE5xQixFQTJPckI7QUFDQyxZQUFVLENBQ1Q7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLFNBRlQ7QUFHQyxZQUFRO0FBSFQsR0FEUyxDQURYO0FBUUMsVUFBUSxhQVJUO0FBU0MsYUFBVyxDQUNWO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxFQUZUO0FBR0MsWUFBUTtBQUhULEdBRFUsQ0FUWjtBQWdCQyxxQkFBbUIsTUFoQnBCO0FBaUJDLFVBQVE7QUFqQlQsQ0EzT3FCLEVBOFByQjtBQUNDLFlBQVUsQ0FDVDtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsT0FGVDtBQUdDLFlBQVE7QUFIVCxHQURTLEVBTVQ7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLFVBRlQ7QUFHQyxZQUFRO0FBSFQsR0FOUyxDQURYO0FBYUMsVUFBUSxrQkFiVDtBQWNDLGFBQVcsQ0FDVjtBQUNDLG9CQUFnQixNQURqQjtBQUVDLFlBQVEsRUFGVDtBQUdDLFlBQVE7QUFIVCxHQURVLENBZFo7QUFxQkMscUJBQW1CLE1BckJwQjtBQXNCQyxVQUFRO0FBdEJULENBOVBxQixFQXNSckI7QUFDQyxZQUFVLEVBRFg7QUFFQyxVQUFRLE1BRlQ7QUFHQyxhQUFXLENBQ1Y7QUFDQyxvQkFBZ0IsUUFEakI7QUFFQyxZQUFRLEVBRlQ7QUFHQyxZQUFRO0FBSFQsR0FEVSxDQUhaO0FBVUMscUJBQW1CLE1BVnBCO0FBV0MsVUFBUTtBQVhULENBdFJxQixFQW1TckI7QUFDQyxZQUFVLENBQ1Q7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLFNBRlQ7QUFHQyxZQUFRO0FBSFQsR0FEUyxDQURYO0FBUUMsVUFBUSxTQVJUO0FBU0MsYUFBVyxDQUNWO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxFQUZUO0FBR0MsWUFBUTtBQUhULEdBRFUsQ0FUWjtBQWdCQyxxQkFBbUIsTUFoQnBCO0FBaUJDLFVBQVE7QUFqQlQsQ0FuU3FCLEVBc1RyQjtBQUNDLFlBQVUsQ0FDVDtBQUNDLG9CQUFnQixRQURqQjtBQUVDLFlBQVEsYUFGVDtBQUdDLFlBQVE7QUFIVCxHQURTLENBRFg7QUFRQyxVQUFRLG1CQVJUO0FBU0MsYUFBVyxDQUNWO0FBQ0Msb0JBQWdCLE1BRGpCO0FBRUMsWUFBUSxFQUZUO0FBR0MsWUFBUTtBQUhULEdBRFUsQ0FUWjtBQWdCQyxxQkFBbUIsTUFoQnBCO0FBaUJDLFVBQVE7QUFqQlQsQ0F0VHFCLEVBeVVyQjtBQUNDLFlBQVUsRUFEWDtBQUVDLFVBQVEsUUFGVDtBQUdDLGFBQVcsQ0FDVjtBQUNDLG9CQUFnQixRQURqQjtBQUVDLFlBQVEsRUFGVDtBQUdDLFlBQVE7QUFIVCxHQURVLENBSFo7QUFVQyxxQkFBbUIsTUFWcEI7QUFXQyxVQUFRO0FBWFQsQ0F6VXFCLEVBc1ZyQjtBQUNDLFlBQVUsQ0FDVDtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsU0FGVDtBQUdDLFlBQVE7QUFIVCxHQURTLENBRFg7QUFRQyxVQUFRLFVBUlQ7QUFTQyxhQUFXLENBQ1Y7QUFDQyxvQkFBZ0IsUUFEakI7QUFFQyxZQUFRLEVBRlQ7QUFHQyxZQUFRO0FBSFQsR0FEVSxDQVRaO0FBZ0JDLHFCQUFtQixNQWhCcEI7QUFpQkMsVUFBUTtBQWpCVCxDQXRWcUIsQ0FBZixDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDOVJQO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFNZSxTQUFTQyxJQUFULEdBQWdCO0FBQzdCLFFBQU07QUFBQSxPQUFDQyxJQUFEO0FBQUEsT0FBT0M7QUFBUCxNQUFrQkMsK0NBQVEsQ0FBQyxFQUFELENBQWhDO0FBQ0EsUUFBTTtBQUFBLE9BQUNDLE1BQUQ7QUFBQSxPQUFTQztBQUFULE1BQXNCRiwrQ0FBUSxDQUFDLFlBQUQsQ0FBcEM7QUFDQUcsa0RBQVMsQ0FBQyxNQUFNO0FBQ2RDLFlBQVE7QUFDVCxHQUZRLEVBRU4sRUFGTSxDQUFUOztBQUdBLGlCQUFlQSxRQUFmLEdBQTBCO0FBQ3hCLFVBQU1DLGVBQWUsR0FBRztBQUN0QkMsZUFBUyxFQUFFO0FBQ1RDLGVBQU8sRUFBRUMsa0RBREE7QUFFVEMsZUFBTyxFQUFFO0FBQ1A7QUFDQUMsYUFBRyxFQUFFO0FBRkU7QUFGQSxPQURXO0FBUXRCQyxtQkFBYSxFQUFFO0FBQ2JKLGVBQU8sRUFBRUsscUVBREk7QUFDbUI7QUFDaENILGVBQU8sRUFBRTtBQUNQSSxrQkFBUSxFQUFFLFdBREgsQ0FDZTs7QUFEZjtBQUZJO0FBUk8sS0FBeEI7QUFlQSxVQUFNQyxTQUFTLEdBQUcsSUFBSUMsa0RBQUosQ0FBYztBQUM5QkMsYUFBTyxFQUFFLFNBRHFCO0FBRTlCQyxtQkFBYSxFQUFFLElBRmU7QUFHOUJDLDZCQUF1QixFQUFFLEtBSEs7QUFJOUJiLHFCQUFlLEVBQUVBLGVBSmEsQ0FJSTs7QUFKSixLQUFkLENBQWxCO0FBTUEsVUFBTWMsVUFBVSxHQUFHLE1BQU1MLFNBQVMsQ0FBQ00sT0FBVixFQUF6QjtBQUNBLFVBQU1DLFFBQVEsR0FBRyxJQUFJQyxpRUFBSixDQUFrQ0gsVUFBbEMsQ0FBakI7QUFDQSxVQUFNSSxhQUFhLEdBQUcsSUFBSUQsbURBQUosQ0FBb0I1QiwrQ0FBcEIsRUFBZ0NFLDJDQUFoQyxFQUF3Q3lCLFFBQXhDLENBQXRCO0FBQ0EsVUFBTUcsY0FBYyxHQUFHLElBQUlGLG1EQUFKLENBQW9CN0IscURBQXBCLEVBQXNDRSxpREFBdEMsRUFBb0QwQixRQUFwRCxDQUF2QjtBQUNBLFVBQU1JLElBQUksR0FBRyxNQUFNRCxjQUFjLENBQUNFLGdCQUFmLEVBQW5CO0FBRUEsVUFBTUMsS0FBSyxHQUFHLE1BQU1DLE9BQU8sQ0FBQ0MsR0FBUixDQUFZSixJQUFJLENBQUNLLEdBQUwsQ0FBUyxNQUFNQyxDQUFOLElBQVc7QUFDbEQsWUFBTUMsUUFBUSxHQUFHLE1BQU1ULGFBQWEsQ0FBQ1UsUUFBZCxDQUF1QkYsQ0FBQyxDQUFDRyxPQUF6QixDQUF2QjtBQUNBLFlBQU1DLElBQUksR0FBRyxNQUFNQyxnREFBQSxDQUFVSixRQUFWLENBQW5CO0FBQ0EsVUFBSUssS0FBSyxHQUFHQyx5REFBQSxDQUFtQlAsQ0FBQyxDQUFDTSxLQUFGLENBQVFFLFFBQVIsRUFBbkIsRUFBdUMsT0FBdkMsQ0FBWjtBQUNBLFVBQUlDLElBQUksR0FBRztBQUNUSCxhQURTO0FBRVRILGVBQU8sRUFBRUgsQ0FBQyxDQUFDRyxPQUFGLENBQVVPLFFBQVYsRUFGQTtBQUdUQyxjQUFNLEVBQUVYLENBQUMsQ0FBQ1csTUFIRDtBQUlUQyxhQUFLLEVBQUVaLENBQUMsQ0FBQ1ksS0FKQTtBQUtUQyxhQUFLLEVBQUVULElBQUksQ0FBQ1YsSUFBTCxDQUFVbUIsS0FMUjtBQU1UQyxZQUFJLEVBQUVWLElBQUksQ0FBQ1YsSUFBTCxDQUFVb0IsSUFOUDtBQU9UQyxtQkFBVyxFQUFFWCxJQUFJLENBQUNWLElBQUwsQ0FBVXFCO0FBUGQsT0FBWDtBQVNBLGFBQU9OLElBQVA7QUFDRCxLQWQrQixDQUFaLENBQXBCO0FBZUFPLFdBQU8sQ0FBQ0MsR0FBUixDQUFZLFNBQVosRUFBdUJyQixLQUF2QjtBQUNBNUIsV0FBTyxDQUFDNEIsS0FBRCxDQUFQO0FBQ0F6QixhQUFTLENBQUMsUUFBRCxDQUFUO0FBQ0Q7O0FBQ0QsaUJBQWUrQyxNQUFmLENBQXNCQyxHQUF0QixFQUEyQjtBQUN6QixVQUFNcEMsU0FBUyxHQUFHLElBQUlDLGtEQUFKLENBQWM7QUFDOUJDLGFBQU8sRUFBRSxTQURxQjtBQUU5QkMsbUJBQWEsRUFBRTtBQUZlLEtBQWQsQ0FBbEI7QUFJQSxVQUFNRSxVQUFVLEdBQUcsTUFBTUwsU0FBUyxDQUFDTSxPQUFWLEVBQXpCO0FBQ0EsVUFBTUMsUUFBUSxHQUFHLElBQUlDLGlFQUFKLENBQWtDSCxVQUFsQyxDQUFqQjtBQUNBLFVBQU1nQyxNQUFNLEdBQUc5QixRQUFRLENBQUMrQixTQUFULEVBQWY7QUFDQSxVQUFNQyxRQUFRLEdBQUcsSUFBSS9CLG1EQUFKLENBQW9CN0IscURBQXBCLEVBQXNDRSxpREFBdEMsRUFBb0R3RCxNQUFwRCxDQUFqQjtBQUVBLFVBQU1kLEtBQUssR0FBR0MsdURBQUEsQ0FBaUJZLEdBQUcsQ0FBQ2IsS0FBSixDQUFVRSxRQUFWLEVBQWpCLEVBQXVDLE9BQXZDLENBQWQ7QUFFQVEsV0FBTyxDQUFDQyxHQUFSLENBQVksU0FBWixFQUF1QlgsS0FBdkI7QUFFQSxVQUFNaUIsV0FBVyxHQUFHLE1BQU1ELFFBQVEsQ0FBQ0UsZ0JBQVQsQ0FBMEI3RCwrQ0FBMUIsRUFBc0N3RCxHQUFHLENBQUNoQixPQUExQyxFQUFtRDtBQUMzRXNCLFdBQUssRUFBRW5CO0FBRG9FLEtBQW5ELENBQTFCO0FBR0EsVUFBTWlCLFdBQVcsQ0FBQ0csSUFBWixFQUFOO0FBQ0FyRCxZQUFRO0FBQ1Q7O0FBRUQsTUFBSUgsTUFBTSxLQUFLLFFBQVgsSUFBdUIsQ0FBQ0gsSUFBSSxDQUFDNEQsTUFBakMsRUFBeUMsb0JBQVE7QUFBSSxhQUFTLEVBQUMsZUFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQUFSO0FBQ3pDLHNCQUNFO0FBQUssYUFBUyxFQUFDLHFCQUFmO0FBQUEsMkJBQ0U7QUFBSyxXQUFLLEVBQUU7QUFBRUMsYUFBSyxFQUFFO0FBQVQsT0FBWjtBQUFBLDZCQUNFO0FBQUssaUJBQVMsRUFBQyw2QkFBZjtBQUFBLGtCQUVJN0QsSUFBSSxDQUFDZ0MsR0FBTCxDQUFTLENBQUNvQixHQUFELEVBQU1uQixDQUFOLGtCQUNQO0FBQWEsbUJBQVMsRUFBQyxtQkFBdkI7QUFBQSxrQ0FDRTtBQUFLLGVBQUcsRUFBRW1CLEdBQUcsQ0FBQ04sS0FBZDtBQUFxQixxQkFBUyxFQUFDO0FBQS9CO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBREYsZUFFRTtBQUFHLHFCQUFTLEVBQUMseUJBQWI7QUFBQSxrQ0FBK0NNLEdBQUcsQ0FBQ2IsS0FBbkQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUZGLGVBR0U7QUFBRyxxQkFBUyxFQUFDLHlCQUFiO0FBQUEsaUNBQThDYSxHQUFHLENBQUNMLElBQWxEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFIRixlQUlFO0FBQUcscUJBQVMsRUFBQyx5QkFBYjtBQUFBLHdDQUFxREssR0FBRyxDQUFDSixXQUF6RDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBSkYsZUFLRTtBQUFRLHFCQUFTLEVBQUMsNENBQWxCO0FBQStELG1CQUFPLEVBQUUsTUFBTUcsTUFBTSxDQUFDQyxHQUFELENBQXBGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUxGO0FBQUEsV0FBVW5CLENBQVY7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFERjtBQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURGO0FBbUJELEM7Ozs7Ozs7Ozs7O0FDN0dELDBEOzs7Ozs7Ozs7OztBQ0FBLG1DOzs7Ozs7Ozs7OztBQ0FBLG9DOzs7Ozs7Ozs7OztBQ0FBLHVDOzs7Ozs7Ozs7OztBQ0FBLG1DOzs7Ozs7Ozs7OztBQ0FBLG1EOzs7Ozs7Ozs7OztBQ0FBLGtDOzs7Ozs7Ozs7OztBQ0FBLHVDIiwiZmlsZSI6InBhZ2VzL2luZGV4LmpzIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNvbnN0IG5mdG1hcmtldGFkZHJlc3MgPSBcIjB4OTEwNmYwQUYwMDBBQjBmODM4YjAyNTE1OGU1ODMyZjU4RmUzYTM1RVwiICAvL3JpbmtlYnlcbmV4cG9ydCBjb25zdCBuZnRhZGRyZXNzID0gXCIweEQ4MWFiMDgzZTgzNUZCZTcyMURCNkI1Qzc3NDlkYzcyRTI3NDRlNTdcIiAgLy9yaW5rZWJ5XG5cbmV4cG9ydCBjb25zdCBuZnRtYXJrZXRhYmkgPSBbXG5cdHtcblx0XHRcImlucHV0c1wiOiBbXSxcblx0XHRcInN0YXRlTXV0YWJpbGl0eVwiOiBcIm5vbnBheWFibGVcIixcblx0XHRcInR5cGVcIjogXCJjb25zdHJ1Y3RvclwiXG5cdH0sXG5cdHtcblx0XHRcImFub255bW91c1wiOiBmYWxzZSxcblx0XHRcImlucHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW5kZXhlZFwiOiB0cnVlLFxuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwiaXRlbUlkXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0XCJpbmRleGVkXCI6IHRydWUsXG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJuZnRDb250cmFjdFwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW5kZXhlZFwiOiB0cnVlLFxuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwidG9rZW5JZFwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW5kZXhlZFwiOiBmYWxzZSxcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcInNlbGxlclwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW5kZXhlZFwiOiBmYWxzZSxcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIm93bmVyXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0XCJpbmRleGVkXCI6IGZhbHNlLFxuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwicHJpY2VcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcIm5hbWVcIjogXCJNYXJrZXRJdGVtQ3JlYXRlZFwiLFxuXHRcdFwidHlwZVwiOiBcImV2ZW50XCJcblx0fSxcblx0e1xuXHRcdFwiaW5wdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIm5mdENvbnRyYWN0XCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcInRva2VuSWRcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwicHJpY2VcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcIm5hbWVcIjogXCJjcmVhdGVNYXJrZXRJdGVtXCIsXG5cdFx0XCJvdXRwdXRzXCI6IFtdLFxuXHRcdFwic3RhdGVNdXRhYmlsaXR5XCI6IFwicGF5YWJsZVwiLFxuXHRcdFwidHlwZVwiOiBcImZ1bmN0aW9uXCJcblx0fSxcblx0e1xuXHRcdFwiaW5wdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIm5mdENvbnRyYWN0XCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIml0ZW1JZFwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwibmFtZVwiOiBcImNyZWF0ZU1hcmtldFNhbGVcIixcblx0XHRcIm91dHB1dHNcIjogW10sXG5cdFx0XCJzdGF0ZU11dGFiaWxpdHlcIjogXCJwYXlhYmxlXCIsXG5cdFx0XCJ0eXBlXCI6IFwiZnVuY3Rpb25cIlxuXHR9LFxuXHR7XG5cdFx0XCJpbnB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwiaXRlbUlkXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJuYW1lXCI6IFwiZmV0Y2hNYXJrZXRJdGVtXCIsXG5cdFx0XCJvdXRwdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJjb21wb25lbnRzXCI6IFtcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XHRcdFwibmFtZVwiOiBcIml0ZW1JZFwiLFxuXHRcdFx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XHRcdFwibmFtZVwiOiBcIm5mdENvbnRyYWN0XCIsXG5cdFx0XHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcdFx0XCJuYW1lXCI6IFwidG9rZW5JZFwiLFxuXHRcdFx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3MgcGF5YWJsZVwiLFxuXHRcdFx0XHRcdFx0XCJuYW1lXCI6IFwic2VsbGVyXCIsXG5cdFx0XHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzcyBwYXlhYmxlXCIsXG5cdFx0XHRcdFx0XHRcIm5hbWVcIjogXCJvd25lclwiLFxuXHRcdFx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XHRcdFwibmFtZVwiOiBcInByaWNlXCIsXG5cdFx0XHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdFx0XHR9XG5cdFx0XHRcdF0sXG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwic3RydWN0IE5GVE1hcmtldC5NYXJrZXRJdGVtXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIlwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJ0dXBsZVwiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcInN0YXRlTXV0YWJpbGl0eVwiOiBcInZpZXdcIixcblx0XHRcInR5cGVcIjogXCJmdW5jdGlvblwiXG5cdH0sXG5cdHtcblx0XHRcImlucHV0c1wiOiBbXSxcblx0XHRcIm5hbWVcIjogXCJmZXRjaE1hcmtldEl0ZW1zXCIsXG5cdFx0XCJvdXRwdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJjb21wb25lbnRzXCI6IFtcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XHRcdFwibmFtZVwiOiBcIml0ZW1JZFwiLFxuXHRcdFx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XHRcdFwibmFtZVwiOiBcIm5mdENvbnRyYWN0XCIsXG5cdFx0XHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcdFx0XCJuYW1lXCI6IFwidG9rZW5JZFwiLFxuXHRcdFx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3MgcGF5YWJsZVwiLFxuXHRcdFx0XHRcdFx0XCJuYW1lXCI6IFwic2VsbGVyXCIsXG5cdFx0XHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzcyBwYXlhYmxlXCIsXG5cdFx0XHRcdFx0XHRcIm5hbWVcIjogXCJvd25lclwiLFxuXHRcdFx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XHRcdFwibmFtZVwiOiBcInByaWNlXCIsXG5cdFx0XHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdFx0XHR9XG5cdFx0XHRcdF0sXG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwic3RydWN0IE5GVE1hcmtldC5NYXJrZXRJdGVtW11cIixcblx0XHRcdFx0XCJuYW1lXCI6IFwiXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInR1cGxlW11cIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJzdGF0ZU11dGFiaWxpdHlcIjogXCJ2aWV3XCIsXG5cdFx0XCJ0eXBlXCI6IFwiZnVuY3Rpb25cIlxuXHR9LFxuXHR7XG5cdFx0XCJpbnB1dHNcIjogW10sXG5cdFx0XCJuYW1lXCI6IFwiZmV0Y2hNeU5GVHNcIixcblx0XHRcIm91dHB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImNvbXBvbmVudHNcIjogW1xuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcdFx0XCJuYW1lXCI6IFwiaXRlbUlkXCIsXG5cdFx0XHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcdFx0XCJuYW1lXCI6IFwibmZ0Q29udHJhY3RcIixcblx0XHRcdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFx0XHRcIm5hbWVcIjogXCJ0b2tlbklkXCIsXG5cdFx0XHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzcyBwYXlhYmxlXCIsXG5cdFx0XHRcdFx0XHRcIm5hbWVcIjogXCJzZWxsZXJcIixcblx0XHRcdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzIHBheWFibGVcIixcblx0XHRcdFx0XHRcdFwibmFtZVwiOiBcIm93bmVyXCIsXG5cdFx0XHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcdFx0XCJuYW1lXCI6IFwicHJpY2VcIixcblx0XHRcdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0XHRcdH1cblx0XHRcdFx0XSxcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJzdHJ1Y3QgTkZUTWFya2V0Lk1hcmtldEl0ZW1bXVwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwidHVwbGVbXVwiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcInN0YXRlTXV0YWJpbGl0eVwiOiBcInZpZXdcIixcblx0XHRcInR5cGVcIjogXCJmdW5jdGlvblwiXG5cdH0sXG5cdHtcblx0XHRcImlucHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJtYXJrZXRJdGVtSWRcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcIm5hbWVcIjogXCJnZXRNYXJrZXRJdGVtXCIsXG5cdFx0XCJvdXRwdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJjb21wb25lbnRzXCI6IFtcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XHRcdFwibmFtZVwiOiBcIml0ZW1JZFwiLFxuXHRcdFx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XHRcdFwibmFtZVwiOiBcIm5mdENvbnRyYWN0XCIsXG5cdFx0XHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcdFx0XCJuYW1lXCI6IFwidG9rZW5JZFwiLFxuXHRcdFx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3MgcGF5YWJsZVwiLFxuXHRcdFx0XHRcdFx0XCJuYW1lXCI6IFwic2VsbGVyXCIsXG5cdFx0XHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzcyBwYXlhYmxlXCIsXG5cdFx0XHRcdFx0XHRcIm5hbWVcIjogXCJvd25lclwiLFxuXHRcdFx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XHRcdFwibmFtZVwiOiBcInByaWNlXCIsXG5cdFx0XHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdFx0XHR9XG5cdFx0XHRcdF0sXG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwic3RydWN0IE5GVE1hcmtldC5NYXJrZXRJdGVtXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIlwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJ0dXBsZVwiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcInN0YXRlTXV0YWJpbGl0eVwiOiBcInZpZXdcIixcblx0XHRcInR5cGVcIjogXCJmdW5jdGlvblwiXG5cdH1cbl1cbmV4cG9ydCBjb25zdCBuZnRhYmkgPSBbXG5cdHtcblx0XHRcImlucHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJtYXJrZXRwbGFjZUFkZHJlc3NcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcInN0YXRlTXV0YWJpbGl0eVwiOiBcIm5vbnBheWFibGVcIixcblx0XHRcInR5cGVcIjogXCJjb25zdHJ1Y3RvclwiXG5cdH0sXG5cdHtcblx0XHRcImFub255bW91c1wiOiBmYWxzZSxcblx0XHRcImlucHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW5kZXhlZFwiOiB0cnVlLFxuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwib3duZXJcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRcImluZGV4ZWRcIjogdHJ1ZSxcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcImFwcHJvdmVkXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0XCJpbmRleGVkXCI6IHRydWUsXG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJ0b2tlbklkXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJuYW1lXCI6IFwiQXBwcm92YWxcIixcblx0XHRcInR5cGVcIjogXCJldmVudFwiXG5cdH0sXG5cdHtcblx0XHRcImFub255bW91c1wiOiBmYWxzZSxcblx0XHRcImlucHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW5kZXhlZFwiOiB0cnVlLFxuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwib3duZXJcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRcImluZGV4ZWRcIjogdHJ1ZSxcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIm9wZXJhdG9yXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0XCJpbmRleGVkXCI6IGZhbHNlLFxuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImJvb2xcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwiYXBwcm92ZWRcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYm9vbFwiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcIm5hbWVcIjogXCJBcHByb3ZhbEZvckFsbFwiLFxuXHRcdFwidHlwZVwiOiBcImV2ZW50XCJcblx0fSxcblx0e1xuXHRcdFwiaW5wdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcInRvXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcInRva2VuSWRcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcIm5hbWVcIjogXCJhcHByb3ZlXCIsXG5cdFx0XCJvdXRwdXRzXCI6IFtdLFxuXHRcdFwic3RhdGVNdXRhYmlsaXR5XCI6IFwibm9ucGF5YWJsZVwiLFxuXHRcdFwidHlwZVwiOiBcImZ1bmN0aW9uXCJcblx0fSxcblx0e1xuXHRcdFwiaW5wdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJzdHJpbmdcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwidG9rZW5VUklcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwic3RyaW5nXCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwibmFtZVwiOiBcImNyZWF0ZVRva2VuXCIsXG5cdFx0XCJvdXRwdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIlwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwic3RhdGVNdXRhYmlsaXR5XCI6IFwibm9ucGF5YWJsZVwiLFxuXHRcdFwidHlwZVwiOiBcImZ1bmN0aW9uXCJcblx0fSxcblx0e1xuXHRcdFwiaW5wdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcImZyb21cIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwidG9cIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwidG9rZW5JZFwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwibmFtZVwiOiBcInNhZmVUcmFuc2ZlckZyb21cIixcblx0XHRcIm91dHB1dHNcIjogW10sXG5cdFx0XCJzdGF0ZU11dGFiaWxpdHlcIjogXCJub25wYXlhYmxlXCIsXG5cdFx0XCJ0eXBlXCI6IFwiZnVuY3Rpb25cIlxuXHR9LFxuXHR7XG5cdFx0XCJpbnB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwiZnJvbVwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJ0b1wiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJ0b2tlbklkXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJieXRlc1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJfZGF0YVwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJieXRlc1wiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcIm5hbWVcIjogXCJzYWZlVHJhbnNmZXJGcm9tXCIsXG5cdFx0XCJvdXRwdXRzXCI6IFtdLFxuXHRcdFwic3RhdGVNdXRhYmlsaXR5XCI6IFwibm9ucGF5YWJsZVwiLFxuXHRcdFwidHlwZVwiOiBcImZ1bmN0aW9uXCJcblx0fSxcblx0e1xuXHRcdFwiaW5wdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIm9wZXJhdG9yXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJib29sXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcImFwcHJvdmVkXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImJvb2xcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJuYW1lXCI6IFwic2V0QXBwcm92YWxGb3JBbGxcIixcblx0XHRcIm91dHB1dHNcIjogW10sXG5cdFx0XCJzdGF0ZU11dGFiaWxpdHlcIjogXCJub25wYXlhYmxlXCIsXG5cdFx0XCJ0eXBlXCI6IFwiZnVuY3Rpb25cIlxuXHR9LFxuXHR7XG5cdFx0XCJhbm9ueW1vdXNcIjogZmFsc2UsXG5cdFx0XCJpbnB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImluZGV4ZWRcIjogdHJ1ZSxcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcImZyb21cIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRcImluZGV4ZWRcIjogdHJ1ZSxcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcInRvXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0XCJpbmRleGVkXCI6IHRydWUsXG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJ0b2tlbklkXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJuYW1lXCI6IFwiVHJhbnNmZXJcIixcblx0XHRcInR5cGVcIjogXCJldmVudFwiXG5cdH0sXG5cdHtcblx0XHRcImlucHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJmcm9tXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcInRvXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcInRva2VuSWRcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcIm5hbWVcIjogXCJ0cmFuc2ZlckZyb21cIixcblx0XHRcIm91dHB1dHNcIjogW10sXG5cdFx0XCJzdGF0ZU11dGFiaWxpdHlcIjogXCJub25wYXlhYmxlXCIsXG5cdFx0XCJ0eXBlXCI6IFwiZnVuY3Rpb25cIlxuXHR9LFxuXHR7XG5cdFx0XCJpbnB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwib3duZXJcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcIm5hbWVcIjogXCJiYWxhbmNlT2ZcIixcblx0XHRcIm91dHB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwiXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJzdGF0ZU11dGFiaWxpdHlcIjogXCJ2aWV3XCIsXG5cdFx0XCJ0eXBlXCI6IFwiZnVuY3Rpb25cIlxuXHR9LFxuXHR7XG5cdFx0XCJpbnB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwidG9rZW5JZFwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwibmFtZVwiOiBcImdldEFwcHJvdmVkXCIsXG5cdFx0XCJvdXRwdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIlwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwic3RhdGVNdXRhYmlsaXR5XCI6IFwidmlld1wiLFxuXHRcdFwidHlwZVwiOiBcImZ1bmN0aW9uXCJcblx0fSxcblx0e1xuXHRcdFwiaW5wdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIm93bmVyXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIm9wZXJhdG9yXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJuYW1lXCI6IFwiaXNBcHByb3ZlZEZvckFsbFwiLFxuXHRcdFwib3V0cHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYm9vbFwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYm9vbFwiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcInN0YXRlTXV0YWJpbGl0eVwiOiBcInZpZXdcIixcblx0XHRcInR5cGVcIjogXCJmdW5jdGlvblwiXG5cdH0sXG5cdHtcblx0XHRcImlucHV0c1wiOiBbXSxcblx0XHRcIm5hbWVcIjogXCJuYW1lXCIsXG5cdFx0XCJvdXRwdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJzdHJpbmdcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwiXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInN0cmluZ1wiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcInN0YXRlTXV0YWJpbGl0eVwiOiBcInZpZXdcIixcblx0XHRcInR5cGVcIjogXCJmdW5jdGlvblwiXG5cdH0sXG5cdHtcblx0XHRcImlucHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJ0b2tlbklkXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJuYW1lXCI6IFwib3duZXJPZlwiLFxuXHRcdFwib3V0cHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcInN0YXRlTXV0YWJpbGl0eVwiOiBcInZpZXdcIixcblx0XHRcInR5cGVcIjogXCJmdW5jdGlvblwiXG5cdH0sXG5cdHtcblx0XHRcImlucHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYnl0ZXM0XCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcImludGVyZmFjZUlkXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImJ5dGVzNFwiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcIm5hbWVcIjogXCJzdXBwb3J0c0ludGVyZmFjZVwiLFxuXHRcdFwib3V0cHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYm9vbFwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYm9vbFwiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcInN0YXRlTXV0YWJpbGl0eVwiOiBcInZpZXdcIixcblx0XHRcInR5cGVcIjogXCJmdW5jdGlvblwiXG5cdH0sXG5cdHtcblx0XHRcImlucHV0c1wiOiBbXSxcblx0XHRcIm5hbWVcIjogXCJzeW1ib2xcIixcblx0XHRcIm91dHB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInN0cmluZ1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwic3RyaW5nXCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwic3RhdGVNdXRhYmlsaXR5XCI6IFwidmlld1wiLFxuXHRcdFwidHlwZVwiOiBcImZ1bmN0aW9uXCJcblx0fSxcblx0e1xuXHRcdFwiaW5wdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcInRva2VuSWRcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcIm5hbWVcIjogXCJ0b2tlblVSSVwiLFxuXHRcdFwib3V0cHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwic3RyaW5nXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIlwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJzdHJpbmdcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJzdGF0ZU11dGFiaWxpdHlcIjogXCJ2aWV3XCIsXG5cdFx0XCJ0eXBlXCI6IFwiZnVuY3Rpb25cIlxuXHR9XG5dIiwiaW1wb3J0IHsgZXRoZXJzIH0gZnJvbSAnZXRoZXJzJ1xuaW1wb3J0IHsgdXNlRWZmZWN0LCB1c2VTdGF0ZSB9IGZyb20gJ3JlYWN0J1xuaW1wb3J0IGF4aW9zIGZyb20gJ2F4aW9zJ1xuXG5pbXBvcnQgd2ViMyBmcm9tICd3ZWIzJ1xuaW1wb3J0IFdlYjNNb2RhbCBmcm9tIFwid2ViM21vZGFsXCJcbmltcG9ydCBGb3J0bWF0aWMgZnJvbSBcImZvcnRtYXRpY1wiO1xuaW1wb3J0IFdhbGxldENvbm5lY3RQcm92aWRlciBmcm9tIFwiQHdhbGxldGNvbm5lY3Qvd2ViMy1wcm92aWRlclwiO1xuXG5pbXBvcnQge1xuICBuZnRtYXJrZXRhZGRyZXNzLCBuZnRhZGRyZXNzLCBuZnRhYmksIG5mdG1hcmtldGFiaVxufSBmcm9tICcuLi9jb25maWcnXG5cblxuXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBIb21lKCkge1xuICBjb25zdCBbbmZ0cywgc2V0TmZ0c10gPSB1c2VTdGF0ZShbXSlcbiAgY29uc3QgW2xvYWRlZCwgc2V0TG9hZGVkXSA9IHVzZVN0YXRlKCdub3QtbG9hZGVkJylcbiAgdXNlRWZmZWN0KCgpID0+IHtcbiAgICBsb2FkTkZUcygpXG4gIH0sIFtdKVxuICBhc3luYyBmdW5jdGlvbiBsb2FkTkZUcygpIHtcbiAgICBjb25zdCBwcm92aWRlck9wdGlvbnMgPSB7XG4gICAgICBmb3J0bWF0aWM6IHtcbiAgICAgICAgcGFja2FnZTogRm9ydG1hdGljLFxuICAgICAgICBvcHRpb25zOiB7XG4gICAgICAgICAgLy8gTWlra28ncyBURVNUTkVUIGFwaSBrZXlcbiAgICAgICAgICBrZXk6IFwicGtfdGVzdF8zOTFFMjZBM0I0M0EzMzUwXCJcbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgIHdhbGxldGNvbm5lY3Q6IHtcbiAgICAgICAgcGFja2FnZTogV2FsbGV0Q29ubmVjdFByb3ZpZGVyLCAvLyByZXF1aXJlZFxuICAgICAgICBvcHRpb25zOiB7XG4gICAgICAgICAgaW5mdXJhSWQ6IFwiSU5GVVJBX0lEXCIgLy8gcmVxdWlyZWRcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH07XG4gICAgY29uc3Qgd2ViM01vZGFsID0gbmV3IFdlYjNNb2RhbCh7XG4gICAgICBuZXR3b3JrOiBcIm1haW5uZXRcIixcbiAgICAgIGNhY2hlUHJvdmlkZXI6IHRydWUsXG4gICAgICBkaXNhYmxlSW5qZWN0ZWRQcm92aWRlcjogZmFsc2UsXG4gICAgICBwcm92aWRlck9wdGlvbnM6IHByb3ZpZGVyT3B0aW9ucywgLy8gcmVxdWlyZWRcbiAgICB9KTtcbiAgICBjb25zdCBjb25uZWN0aW9uID0gYXdhaXQgd2ViM01vZGFsLmNvbm5lY3QoKVxuICAgIGNvbnN0IHByb3ZpZGVyID0gbmV3IGV0aGVycy5wcm92aWRlcnMuV2ViM1Byb3ZpZGVyKGNvbm5lY3Rpb24pXG4gICAgY29uc3QgdG9rZW5Db250cmFjdCA9IG5ldyBldGhlcnMuQ29udHJhY3QobmZ0YWRkcmVzcywgbmZ0YWJpLCBwcm92aWRlcilcbiAgICBjb25zdCBtYXJrZXRDb250cmFjdCA9IG5ldyBldGhlcnMuQ29udHJhY3QobmZ0bWFya2V0YWRkcmVzcywgbmZ0bWFya2V0YWJpLCBwcm92aWRlcilcbiAgICBjb25zdCBkYXRhID0gYXdhaXQgbWFya2V0Q29udHJhY3QuZmV0Y2hNYXJrZXRJdGVtcygpXG5cbiAgICBjb25zdCBpdGVtcyA9IGF3YWl0IFByb21pc2UuYWxsKGRhdGEubWFwKGFzeW5jIGkgPT4ge1xuICAgICAgY29uc3QgdG9rZW5VcmkgPSBhd2FpdCB0b2tlbkNvbnRyYWN0LnRva2VuVVJJKGkudG9rZW5JZClcbiAgICAgIGNvbnN0IG1ldGEgPSBhd2FpdCBheGlvcy5nZXQodG9rZW5VcmkpXG4gICAgICBsZXQgcHJpY2UgPSB3ZWIzLnV0aWxzLmZyb21XZWkoaS5wcmljZS50b1N0cmluZygpLCAnZXRoZXInKTtcbiAgICAgIGxldCBpdGVtID0ge1xuICAgICAgICBwcmljZSxcbiAgICAgICAgdG9rZW5JZDogaS50b2tlbklkLnRvTnVtYmVyKCksXG4gICAgICAgIHNlbGxlcjogaS5zZWxsZXIsXG4gICAgICAgIG93bmVyOiBpLm93bmVyLFxuICAgICAgICBpbWFnZTogbWV0YS5kYXRhLmltYWdlLFxuICAgICAgICBuYW1lOiBtZXRhLmRhdGEubmFtZSxcbiAgICAgICAgZGVzY3JpcHRpb246IG1ldGEuZGF0YS5kZXNjcmlwdGlvbixcbiAgICAgIH1cbiAgICAgIHJldHVybiBpdGVtXG4gICAgfSkpXG4gICAgY29uc29sZS5sb2coJ2l0ZW1zOiAnLCBpdGVtcylcbiAgICBzZXROZnRzKGl0ZW1zKVxuICAgIHNldExvYWRlZCgnbG9hZGVkJylcbiAgfVxuICBhc3luYyBmdW5jdGlvbiBidXlOZnQobmZ0KSB7XG4gICAgY29uc3Qgd2ViM01vZGFsID0gbmV3IFdlYjNNb2RhbCh7XG4gICAgICBuZXR3b3JrOiBcIm1haW5uZXRcIixcbiAgICAgIGNhY2hlUHJvdmlkZXI6IHRydWUsXG4gICAgfSk7XG4gICAgY29uc3QgY29ubmVjdGlvbiA9IGF3YWl0IHdlYjNNb2RhbC5jb25uZWN0KClcbiAgICBjb25zdCBwcm92aWRlciA9IG5ldyBldGhlcnMucHJvdmlkZXJzLldlYjNQcm92aWRlcihjb25uZWN0aW9uKVxuICAgIGNvbnN0IHNpZ25lciA9IHByb3ZpZGVyLmdldFNpZ25lcigpXG4gICAgY29uc3QgY29udHJhY3QgPSBuZXcgZXRoZXJzLkNvbnRyYWN0KG5mdG1hcmtldGFkZHJlc3MsIG5mdG1hcmtldGFiaSwgc2lnbmVyKVxuXG4gICAgY29uc3QgcHJpY2UgPSB3ZWIzLnV0aWxzLnRvV2VpKG5mdC5wcmljZS50b1N0cmluZygpLCAnZXRoZXInKTtcblxuICAgIGNvbnNvbGUubG9nKCdwcmljZTogJywgcHJpY2UpO1xuXG4gICAgY29uc3QgdHJhbnNhY3Rpb24gPSBhd2FpdCBjb250cmFjdC5jcmVhdGVNYXJrZXRTYWxlKG5mdGFkZHJlc3MsIG5mdC50b2tlbklkLCB7XG4gICAgICB2YWx1ZTogcHJpY2VcbiAgICB9KVxuICAgIGF3YWl0IHRyYW5zYWN0aW9uLndhaXQoKVxuICAgIGxvYWRORlRzKClcbiAgfVxuXG4gIGlmIChsb2FkZWQgPT09ICdsb2FkZWQnICYmICFuZnRzLmxlbmd0aCkgcmV0dXJuICg8aDEgY2xhc3NOYW1lPVwicC0yMCB0ZXh0LTR4bFwiPk5vIE5GVHMhPC9oMT4pXG4gIHJldHVybiAoXG4gICAgPGRpdiBjbGFzc05hbWU9XCJmbGV4IGp1c3RpZnktY2VudGVyXCI+XG4gICAgICA8ZGl2IHN0eWxlPXt7IHdpZHRoOiA5MDAgfX0+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZ3JpZCBncmlkLWNvbHMtMiBnYXAtNCBwdC04XCI+XG4gICAgICAgICAge1xuICAgICAgICAgICAgbmZ0cy5tYXAoKG5mdCwgaSkgPT4gKFxuICAgICAgICAgICAgICA8ZGl2IGtleT17aX0gY2xhc3NOYW1lPVwiYm9yZGVyIHAtNCBzaGFkb3dcIj5cbiAgICAgICAgICAgICAgICA8aW1nIHNyYz17bmZ0LmltYWdlfSBjbGFzc05hbWU9XCJyb3VuZGVkXCIgLz5cbiAgICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJ0ZXh0LTJ4bCBteS00IGZvbnQtYm9sZFwiPlByaWNlOiB7bmZ0LnByaWNlfTwvcD5cbiAgICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJ0ZXh0LTJ4bCBteS00IGZvbnQtYm9sZFwiPk5hbWU6IHtuZnQubmFtZX08L3A+XG4gICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwidGV4dC0yeGwgbXktNCBmb250LWJvbGRcIj5EZXNjcmlwdGlvbjoge25mdC5kZXNjcmlwdGlvbn08L3A+XG4gICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzc05hbWU9XCJiZy1ncmVlbi02MDAgdGV4dC13aGl0ZSBweS0yIHB4LTEyIHJvdW5kZWRcIiBvbkNsaWNrPXsoKSA9PiBidXlOZnQobmZ0KX0+QnV5IE5GVDwvYnV0dG9uPlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICkpXG4gICAgICAgICAgfVxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApXG59XG4iLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAd2FsbGV0Y29ubmVjdC93ZWIzLXByb3ZpZGVyXCIpOzsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJheGlvc1wiKTs7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiZXRoZXJzXCIpOzsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJmb3J0bWF0aWNcIik7OyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0XCIpOzsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC9qc3gtZGV2LXJ1bnRpbWVcIik7OyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIndlYjNcIik7OyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIndlYjNtb2RhbFwiKTs7Il0sInNvdXJjZVJvb3QiOiIifQ==