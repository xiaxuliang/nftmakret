(function() {
var exports = {};
exports.id = "pages/my-nfts";
exports.ids = ["pages/my-nfts"];
exports.modules = {

/***/ "./config.js":
/*!*******************!*\
  !*** ./config.js ***!
  \*******************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "nftmarketaddress": function() { return /* binding */ nftmarketaddress; },
/* harmony export */   "nftaddress": function() { return /* binding */ nftaddress; },
/* harmony export */   "nftmarketabi": function() { return /* binding */ nftmarketabi; },
/* harmony export */   "nftabi": function() { return /* binding */ nftabi; }
/* harmony export */ });
const nftmarketaddress = "0x9106f0AF000AB0f838b025158e5832f58Fe3a35E"; //rinkeby

const nftaddress = "0xD81ab083e835FBe721DB6B5C7749dc72E2744e57"; //rinkeby

const nftmarketabi = [{
  "inputs": [],
  "stateMutability": "nonpayable",
  "type": "constructor"
}, {
  "anonymous": false,
  "inputs": [{
    "indexed": true,
    "internalType": "uint256",
    "name": "itemId",
    "type": "uint256"
  }, {
    "indexed": true,
    "internalType": "address",
    "name": "nftContract",
    "type": "address"
  }, {
    "indexed": true,
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }, {
    "indexed": false,
    "internalType": "address",
    "name": "seller",
    "type": "address"
  }, {
    "indexed": false,
    "internalType": "address",
    "name": "owner",
    "type": "address"
  }, {
    "indexed": false,
    "internalType": "uint256",
    "name": "price",
    "type": "uint256"
  }],
  "name": "MarketItemCreated",
  "type": "event"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "nftContract",
    "type": "address"
  }, {
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }, {
    "internalType": "uint256",
    "name": "price",
    "type": "uint256"
  }],
  "name": "createMarketItem",
  "outputs": [],
  "stateMutability": "payable",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "nftContract",
    "type": "address"
  }, {
    "internalType": "uint256",
    "name": "itemId",
    "type": "uint256"
  }],
  "name": "createMarketSale",
  "outputs": [],
  "stateMutability": "payable",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "uint256",
    "name": "itemId",
    "type": "uint256"
  }],
  "name": "fetchMarketItem",
  "outputs": [{
    "components": [{
      "internalType": "uint256",
      "name": "itemId",
      "type": "uint256"
    }, {
      "internalType": "address",
      "name": "nftContract",
      "type": "address"
    }, {
      "internalType": "uint256",
      "name": "tokenId",
      "type": "uint256"
    }, {
      "internalType": "address payable",
      "name": "seller",
      "type": "address"
    }, {
      "internalType": "address payable",
      "name": "owner",
      "type": "address"
    }, {
      "internalType": "uint256",
      "name": "price",
      "type": "uint256"
    }],
    "internalType": "struct NFTMarket.MarketItem",
    "name": "",
    "type": "tuple"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "fetchMarketItems",
  "outputs": [{
    "components": [{
      "internalType": "uint256",
      "name": "itemId",
      "type": "uint256"
    }, {
      "internalType": "address",
      "name": "nftContract",
      "type": "address"
    }, {
      "internalType": "uint256",
      "name": "tokenId",
      "type": "uint256"
    }, {
      "internalType": "address payable",
      "name": "seller",
      "type": "address"
    }, {
      "internalType": "address payable",
      "name": "owner",
      "type": "address"
    }, {
      "internalType": "uint256",
      "name": "price",
      "type": "uint256"
    }],
    "internalType": "struct NFTMarket.MarketItem[]",
    "name": "",
    "type": "tuple[]"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "fetchMyNFTs",
  "outputs": [{
    "components": [{
      "internalType": "uint256",
      "name": "itemId",
      "type": "uint256"
    }, {
      "internalType": "address",
      "name": "nftContract",
      "type": "address"
    }, {
      "internalType": "uint256",
      "name": "tokenId",
      "type": "uint256"
    }, {
      "internalType": "address payable",
      "name": "seller",
      "type": "address"
    }, {
      "internalType": "address payable",
      "name": "owner",
      "type": "address"
    }, {
      "internalType": "uint256",
      "name": "price",
      "type": "uint256"
    }],
    "internalType": "struct NFTMarket.MarketItem[]",
    "name": "",
    "type": "tuple[]"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "uint256",
    "name": "marketItemId",
    "type": "uint256"
  }],
  "name": "getMarketItem",
  "outputs": [{
    "components": [{
      "internalType": "uint256",
      "name": "itemId",
      "type": "uint256"
    }, {
      "internalType": "address",
      "name": "nftContract",
      "type": "address"
    }, {
      "internalType": "uint256",
      "name": "tokenId",
      "type": "uint256"
    }, {
      "internalType": "address payable",
      "name": "seller",
      "type": "address"
    }, {
      "internalType": "address payable",
      "name": "owner",
      "type": "address"
    }, {
      "internalType": "uint256",
      "name": "price",
      "type": "uint256"
    }],
    "internalType": "struct NFTMarket.MarketItem",
    "name": "",
    "type": "tuple"
  }],
  "stateMutability": "view",
  "type": "function"
}];
const nftabi = [{
  "inputs": [{
    "internalType": "address",
    "name": "marketplaceAddress",
    "type": "address"
  }],
  "stateMutability": "nonpayable",
  "type": "constructor"
}, {
  "anonymous": false,
  "inputs": [{
    "indexed": true,
    "internalType": "address",
    "name": "owner",
    "type": "address"
  }, {
    "indexed": true,
    "internalType": "address",
    "name": "approved",
    "type": "address"
  }, {
    "indexed": true,
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }],
  "name": "Approval",
  "type": "event"
}, {
  "anonymous": false,
  "inputs": [{
    "indexed": true,
    "internalType": "address",
    "name": "owner",
    "type": "address"
  }, {
    "indexed": true,
    "internalType": "address",
    "name": "operator",
    "type": "address"
  }, {
    "indexed": false,
    "internalType": "bool",
    "name": "approved",
    "type": "bool"
  }],
  "name": "ApprovalForAll",
  "type": "event"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "to",
    "type": "address"
  }, {
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }],
  "name": "approve",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "string",
    "name": "tokenURI",
    "type": "string"
  }],
  "name": "createToken",
  "outputs": [{
    "internalType": "uint256",
    "name": "",
    "type": "uint256"
  }],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "from",
    "type": "address"
  }, {
    "internalType": "address",
    "name": "to",
    "type": "address"
  }, {
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }],
  "name": "safeTransferFrom",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "from",
    "type": "address"
  }, {
    "internalType": "address",
    "name": "to",
    "type": "address"
  }, {
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }, {
    "internalType": "bytes",
    "name": "_data",
    "type": "bytes"
  }],
  "name": "safeTransferFrom",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "operator",
    "type": "address"
  }, {
    "internalType": "bool",
    "name": "approved",
    "type": "bool"
  }],
  "name": "setApprovalForAll",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "anonymous": false,
  "inputs": [{
    "indexed": true,
    "internalType": "address",
    "name": "from",
    "type": "address"
  }, {
    "indexed": true,
    "internalType": "address",
    "name": "to",
    "type": "address"
  }, {
    "indexed": true,
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }],
  "name": "Transfer",
  "type": "event"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "from",
    "type": "address"
  }, {
    "internalType": "address",
    "name": "to",
    "type": "address"
  }, {
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }],
  "name": "transferFrom",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "owner",
    "type": "address"
  }],
  "name": "balanceOf",
  "outputs": [{
    "internalType": "uint256",
    "name": "",
    "type": "uint256"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }],
  "name": "getApproved",
  "outputs": [{
    "internalType": "address",
    "name": "",
    "type": "address"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "address",
    "name": "owner",
    "type": "address"
  }, {
    "internalType": "address",
    "name": "operator",
    "type": "address"
  }],
  "name": "isApprovedForAll",
  "outputs": [{
    "internalType": "bool",
    "name": "",
    "type": "bool"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "name",
  "outputs": [{
    "internalType": "string",
    "name": "",
    "type": "string"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }],
  "name": "ownerOf",
  "outputs": [{
    "internalType": "address",
    "name": "",
    "type": "address"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "bytes4",
    "name": "interfaceId",
    "type": "bytes4"
  }],
  "name": "supportsInterface",
  "outputs": [{
    "internalType": "bool",
    "name": "",
    "type": "bool"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "symbol",
  "outputs": [{
    "internalType": "string",
    "name": "",
    "type": "string"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }],
  "name": "tokenURI",
  "outputs": [{
    "internalType": "string",
    "name": "",
    "type": "string"
  }],
  "stateMutability": "view",
  "type": "function"
}];

/***/ }),

/***/ "./pages/my-nfts.js":
/*!**************************!*\
  !*** ./pages/my-nfts.js ***!
  \**************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ Home; }
/* harmony export */ });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var ethers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ethers */ "ethers");
/* harmony import */ var ethers__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(ethers__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var web3__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! web3 */ "web3");
/* harmony import */ var web3__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(web3__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var web3modal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! web3modal */ "web3modal");
/* harmony import */ var web3modal__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(web3modal__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../config */ "./config.js");

var _jsxFileName = "E:\\marketplace-complete\\pages\\my-nfts.js";






function Home() {
  const {
    0: nfts,
    1: setNfts
  } = (0,react__WEBPACK_IMPORTED_MODULE_2__.useState)([]);
  const {
    0: loaded,
    1: setLoaded
  } = (0,react__WEBPACK_IMPORTED_MODULE_2__.useState)('not-loaded');
  (0,react__WEBPACK_IMPORTED_MODULE_2__.useEffect)(() => {
    loadNFTs();
  }, []);

  async function loadNFTs() {
    const web3Modal = new (web3modal__WEBPACK_IMPORTED_MODULE_5___default())({
      network: "mainnet",
      cacheProvider: true
    });
    const connection = await web3Modal.connect();
    const provider = new ethers__WEBPACK_IMPORTED_MODULE_1__.ethers.providers.Web3Provider(connection);
    const signer = provider.getSigner();
    const marketContract = new ethers__WEBPACK_IMPORTED_MODULE_1__.ethers.Contract(_config__WEBPACK_IMPORTED_MODULE_6__.nftmarketaddress, _config__WEBPACK_IMPORTED_MODULE_6__.nftmarketabi, signer);
    const tokenContract = new ethers__WEBPACK_IMPORTED_MODULE_1__.ethers.Contract(_config__WEBPACK_IMPORTED_MODULE_6__.nftaddress, _config__WEBPACK_IMPORTED_MODULE_6__.nftabi, provider);
    const data = await marketContract.fetchMyNFTs();
    const items = await Promise.all(data.map(async i => {
      const tokenUri = await tokenContract.tokenURI(i.tokenId);
      const meta = await axios__WEBPACK_IMPORTED_MODULE_4___default().get(tokenUri);
      let price = web3__WEBPACK_IMPORTED_MODULE_3___default().utils.fromWei(i.price.toString(), 'ether');
      let item = {
        price,
        tokenId: i.tokenId.toNumber(),
        seller: i.seller,
        owner: i.owner,
        image: meta.data.image,
        name: meta.data.name,
        description: meta.data.description
      };
      return item;
    }));
    console.log('items: ', items);
    setNfts(items);
    setLoaded('loaded');
  }

  if (loaded === 'loaded' && !nfts.length) return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h1", {
    className: "p-20 text-4xl",
    children: "No NFTs!"
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 50,
    columnNumber: 52
  }, this);
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
    className: "flex justify-center",
    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      style: {
        width: 900
      },
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "grid grid-cols-2 gap-4 pt-8",
        children: nfts.map((nft, i) => /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "border p-4 shadow",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("img", {
            src: nft.image,
            className: "rounded"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 58,
            columnNumber: 17
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
            className: "text-2xl my-4 font-bold",
            children: ["Price paid: ", nft.price, " Ether"]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 59,
            columnNumber: 17
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
            className: "text-2xl my-4 font-bold",
            children: ["Name: ", nft.name]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 60,
            columnNumber: 17
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
            className: "text-2xl my-4 font-bold",
            children: ["Description: ", nft.description]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 61,
            columnNumber: 17
          }, this)]
        }, i, true, {
          fileName: _jsxFileName,
          lineNumber: 57,
          columnNumber: 15
        }, this))
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 54,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 53,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 52,
    columnNumber: 5
  }, this);
}

/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/***/ (function(module) {

"use strict";
module.exports = require("axios");;

/***/ }),

/***/ "ethers":
/*!*************************!*\
  !*** external "ethers" ***!
  \*************************/
/***/ (function(module) {

"use strict";
module.exports = require("ethers");;

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ (function(module) {

"use strict";
module.exports = require("react");;

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ (function(module) {

"use strict";
module.exports = require("react/jsx-dev-runtime");;

/***/ }),

/***/ "web3":
/*!***********************!*\
  !*** external "web3" ***!
  \***********************/
/***/ (function(module) {

"use strict";
module.exports = require("web3");;

/***/ }),

/***/ "web3modal":
/*!****************************!*\
  !*** external "web3modal" ***!
  \****************************/
/***/ (function(module) {

"use strict";
module.exports = require("web3modal");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
var __webpack_exports__ = (__webpack_exec__("./pages/my-nfts.js"));
module.exports = __webpack_exports__;

})();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9tYXJrZXRwbGFjZS1jb21wbGV0ZS8uL2NvbmZpZy5qcyIsIndlYnBhY2s6Ly9tYXJrZXRwbGFjZS1jb21wbGV0ZS8uL3BhZ2VzL215LW5mdHMuanMiLCJ3ZWJwYWNrOi8vbWFya2V0cGxhY2UtY29tcGxldGUvZXh0ZXJuYWwgXCJheGlvc1wiIiwid2VicGFjazovL21hcmtldHBsYWNlLWNvbXBsZXRlL2V4dGVybmFsIFwiZXRoZXJzXCIiLCJ3ZWJwYWNrOi8vbWFya2V0cGxhY2UtY29tcGxldGUvZXh0ZXJuYWwgXCJyZWFjdFwiIiwid2VicGFjazovL21hcmtldHBsYWNlLWNvbXBsZXRlL2V4dGVybmFsIFwicmVhY3QvanN4LWRldi1ydW50aW1lXCIiLCJ3ZWJwYWNrOi8vbWFya2V0cGxhY2UtY29tcGxldGUvZXh0ZXJuYWwgXCJ3ZWIzXCIiLCJ3ZWJwYWNrOi8vbWFya2V0cGxhY2UtY29tcGxldGUvZXh0ZXJuYWwgXCJ3ZWIzbW9kYWxcIiJdLCJuYW1lcyI6WyJuZnRtYXJrZXRhZGRyZXNzIiwibmZ0YWRkcmVzcyIsIm5mdG1hcmtldGFiaSIsIm5mdGFiaSIsIkhvbWUiLCJuZnRzIiwic2V0TmZ0cyIsInVzZVN0YXRlIiwibG9hZGVkIiwic2V0TG9hZGVkIiwidXNlRWZmZWN0IiwibG9hZE5GVHMiLCJ3ZWIzTW9kYWwiLCJXZWIzTW9kYWwiLCJuZXR3b3JrIiwiY2FjaGVQcm92aWRlciIsImNvbm5lY3Rpb24iLCJjb25uZWN0IiwicHJvdmlkZXIiLCJldGhlcnMiLCJzaWduZXIiLCJnZXRTaWduZXIiLCJtYXJrZXRDb250cmFjdCIsInRva2VuQ29udHJhY3QiLCJkYXRhIiwiZmV0Y2hNeU5GVHMiLCJpdGVtcyIsIlByb21pc2UiLCJhbGwiLCJtYXAiLCJpIiwidG9rZW5VcmkiLCJ0b2tlblVSSSIsInRva2VuSWQiLCJtZXRhIiwiYXhpb3MiLCJwcmljZSIsIndlYjMiLCJ0b1N0cmluZyIsIml0ZW0iLCJ0b051bWJlciIsInNlbGxlciIsIm93bmVyIiwiaW1hZ2UiLCJuYW1lIiwiZGVzY3JpcHRpb24iLCJjb25zb2xlIiwibG9nIiwibGVuZ3RoIiwid2lkdGgiLCJuZnQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQU8sTUFBTUEsZ0JBQWdCLEdBQUcsNENBQXpCLEMsQ0FBdUU7O0FBQ3ZFLE1BQU1DLFVBQVUsR0FBRyw0Q0FBbkIsQyxDQUFpRTs7QUFFakUsTUFBTUMsWUFBWSxHQUFHLENBQzNCO0FBQ0MsWUFBVSxFQURYO0FBRUMscUJBQW1CLFlBRnBCO0FBR0MsVUFBUTtBQUhULENBRDJCLEVBTTNCO0FBQ0MsZUFBYSxLQURkO0FBRUMsWUFBVSxDQUNUO0FBQ0MsZUFBVyxJQURaO0FBRUMsb0JBQWdCLFNBRmpCO0FBR0MsWUFBUSxRQUhUO0FBSUMsWUFBUTtBQUpULEdBRFMsRUFPVDtBQUNDLGVBQVcsSUFEWjtBQUVDLG9CQUFnQixTQUZqQjtBQUdDLFlBQVEsYUFIVDtBQUlDLFlBQVE7QUFKVCxHQVBTLEVBYVQ7QUFDQyxlQUFXLElBRFo7QUFFQyxvQkFBZ0IsU0FGakI7QUFHQyxZQUFRLFNBSFQ7QUFJQyxZQUFRO0FBSlQsR0FiUyxFQW1CVDtBQUNDLGVBQVcsS0FEWjtBQUVDLG9CQUFnQixTQUZqQjtBQUdDLFlBQVEsUUFIVDtBQUlDLFlBQVE7QUFKVCxHQW5CUyxFQXlCVDtBQUNDLGVBQVcsS0FEWjtBQUVDLG9CQUFnQixTQUZqQjtBQUdDLFlBQVEsT0FIVDtBQUlDLFlBQVE7QUFKVCxHQXpCUyxFQStCVDtBQUNDLGVBQVcsS0FEWjtBQUVDLG9CQUFnQixTQUZqQjtBQUdDLFlBQVEsT0FIVDtBQUlDLFlBQVE7QUFKVCxHQS9CUyxDQUZYO0FBd0NDLFVBQVEsbUJBeENUO0FBeUNDLFVBQVE7QUF6Q1QsQ0FOMkIsRUFpRDNCO0FBQ0MsWUFBVSxDQUNUO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxhQUZUO0FBR0MsWUFBUTtBQUhULEdBRFMsRUFNVDtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsU0FGVDtBQUdDLFlBQVE7QUFIVCxHQU5TLEVBV1Q7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLE9BRlQ7QUFHQyxZQUFRO0FBSFQsR0FYUyxDQURYO0FBa0JDLFVBQVEsa0JBbEJUO0FBbUJDLGFBQVcsRUFuQlo7QUFvQkMscUJBQW1CLFNBcEJwQjtBQXFCQyxVQUFRO0FBckJULENBakQyQixFQXdFM0I7QUFDQyxZQUFVLENBQ1Q7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLGFBRlQ7QUFHQyxZQUFRO0FBSFQsR0FEUyxFQU1UO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxRQUZUO0FBR0MsWUFBUTtBQUhULEdBTlMsQ0FEWDtBQWFDLFVBQVEsa0JBYlQ7QUFjQyxhQUFXLEVBZFo7QUFlQyxxQkFBbUIsU0FmcEI7QUFnQkMsVUFBUTtBQWhCVCxDQXhFMkIsRUEwRjNCO0FBQ0MsWUFBVSxDQUNUO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxRQUZUO0FBR0MsWUFBUTtBQUhULEdBRFMsQ0FEWDtBQVFDLFVBQVEsaUJBUlQ7QUFTQyxhQUFXLENBQ1Y7QUFDQyxrQkFBYyxDQUNiO0FBQ0Msc0JBQWdCLFNBRGpCO0FBRUMsY0FBUSxRQUZUO0FBR0MsY0FBUTtBQUhULEtBRGEsRUFNYjtBQUNDLHNCQUFnQixTQURqQjtBQUVDLGNBQVEsYUFGVDtBQUdDLGNBQVE7QUFIVCxLQU5hLEVBV2I7QUFDQyxzQkFBZ0IsU0FEakI7QUFFQyxjQUFRLFNBRlQ7QUFHQyxjQUFRO0FBSFQsS0FYYSxFQWdCYjtBQUNDLHNCQUFnQixpQkFEakI7QUFFQyxjQUFRLFFBRlQ7QUFHQyxjQUFRO0FBSFQsS0FoQmEsRUFxQmI7QUFDQyxzQkFBZ0IsaUJBRGpCO0FBRUMsY0FBUSxPQUZUO0FBR0MsY0FBUTtBQUhULEtBckJhLEVBMEJiO0FBQ0Msc0JBQWdCLFNBRGpCO0FBRUMsY0FBUSxPQUZUO0FBR0MsY0FBUTtBQUhULEtBMUJhLENBRGY7QUFpQ0Msb0JBQWdCLDZCQWpDakI7QUFrQ0MsWUFBUSxFQWxDVDtBQW1DQyxZQUFRO0FBbkNULEdBRFUsQ0FUWjtBQWdEQyxxQkFBbUIsTUFoRHBCO0FBaURDLFVBQVE7QUFqRFQsQ0ExRjJCLEVBNkkzQjtBQUNDLFlBQVUsRUFEWDtBQUVDLFVBQVEsa0JBRlQ7QUFHQyxhQUFXLENBQ1Y7QUFDQyxrQkFBYyxDQUNiO0FBQ0Msc0JBQWdCLFNBRGpCO0FBRUMsY0FBUSxRQUZUO0FBR0MsY0FBUTtBQUhULEtBRGEsRUFNYjtBQUNDLHNCQUFnQixTQURqQjtBQUVDLGNBQVEsYUFGVDtBQUdDLGNBQVE7QUFIVCxLQU5hLEVBV2I7QUFDQyxzQkFBZ0IsU0FEakI7QUFFQyxjQUFRLFNBRlQ7QUFHQyxjQUFRO0FBSFQsS0FYYSxFQWdCYjtBQUNDLHNCQUFnQixpQkFEakI7QUFFQyxjQUFRLFFBRlQ7QUFHQyxjQUFRO0FBSFQsS0FoQmEsRUFxQmI7QUFDQyxzQkFBZ0IsaUJBRGpCO0FBRUMsY0FBUSxPQUZUO0FBR0MsY0FBUTtBQUhULEtBckJhLEVBMEJiO0FBQ0Msc0JBQWdCLFNBRGpCO0FBRUMsY0FBUSxPQUZUO0FBR0MsY0FBUTtBQUhULEtBMUJhLENBRGY7QUFpQ0Msb0JBQWdCLCtCQWpDakI7QUFrQ0MsWUFBUSxFQWxDVDtBQW1DQyxZQUFRO0FBbkNULEdBRFUsQ0FIWjtBQTBDQyxxQkFBbUIsTUExQ3BCO0FBMkNDLFVBQVE7QUEzQ1QsQ0E3STJCLEVBMEwzQjtBQUNDLFlBQVUsRUFEWDtBQUVDLFVBQVEsYUFGVDtBQUdDLGFBQVcsQ0FDVjtBQUNDLGtCQUFjLENBQ2I7QUFDQyxzQkFBZ0IsU0FEakI7QUFFQyxjQUFRLFFBRlQ7QUFHQyxjQUFRO0FBSFQsS0FEYSxFQU1iO0FBQ0Msc0JBQWdCLFNBRGpCO0FBRUMsY0FBUSxhQUZUO0FBR0MsY0FBUTtBQUhULEtBTmEsRUFXYjtBQUNDLHNCQUFnQixTQURqQjtBQUVDLGNBQVEsU0FGVDtBQUdDLGNBQVE7QUFIVCxLQVhhLEVBZ0JiO0FBQ0Msc0JBQWdCLGlCQURqQjtBQUVDLGNBQVEsUUFGVDtBQUdDLGNBQVE7QUFIVCxLQWhCYSxFQXFCYjtBQUNDLHNCQUFnQixpQkFEakI7QUFFQyxjQUFRLE9BRlQ7QUFHQyxjQUFRO0FBSFQsS0FyQmEsRUEwQmI7QUFDQyxzQkFBZ0IsU0FEakI7QUFFQyxjQUFRLE9BRlQ7QUFHQyxjQUFRO0FBSFQsS0ExQmEsQ0FEZjtBQWlDQyxvQkFBZ0IsK0JBakNqQjtBQWtDQyxZQUFRLEVBbENUO0FBbUNDLFlBQVE7QUFuQ1QsR0FEVSxDQUhaO0FBMENDLHFCQUFtQixNQTFDcEI7QUEyQ0MsVUFBUTtBQTNDVCxDQTFMMkIsRUF1TzNCO0FBQ0MsWUFBVSxDQUNUO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxjQUZUO0FBR0MsWUFBUTtBQUhULEdBRFMsQ0FEWDtBQVFDLFVBQVEsZUFSVDtBQVNDLGFBQVcsQ0FDVjtBQUNDLGtCQUFjLENBQ2I7QUFDQyxzQkFBZ0IsU0FEakI7QUFFQyxjQUFRLFFBRlQ7QUFHQyxjQUFRO0FBSFQsS0FEYSxFQU1iO0FBQ0Msc0JBQWdCLFNBRGpCO0FBRUMsY0FBUSxhQUZUO0FBR0MsY0FBUTtBQUhULEtBTmEsRUFXYjtBQUNDLHNCQUFnQixTQURqQjtBQUVDLGNBQVEsU0FGVDtBQUdDLGNBQVE7QUFIVCxLQVhhLEVBZ0JiO0FBQ0Msc0JBQWdCLGlCQURqQjtBQUVDLGNBQVEsUUFGVDtBQUdDLGNBQVE7QUFIVCxLQWhCYSxFQXFCYjtBQUNDLHNCQUFnQixpQkFEakI7QUFFQyxjQUFRLE9BRlQ7QUFHQyxjQUFRO0FBSFQsS0FyQmEsRUEwQmI7QUFDQyxzQkFBZ0IsU0FEakI7QUFFQyxjQUFRLE9BRlQ7QUFHQyxjQUFRO0FBSFQsS0ExQmEsQ0FEZjtBQWlDQyxvQkFBZ0IsNkJBakNqQjtBQWtDQyxZQUFRLEVBbENUO0FBbUNDLFlBQVE7QUFuQ1QsR0FEVSxDQVRaO0FBZ0RDLHFCQUFtQixNQWhEcEI7QUFpREMsVUFBUTtBQWpEVCxDQXZPMkIsQ0FBckI7QUEyUkEsTUFBTUMsTUFBTSxHQUFHLENBQ3JCO0FBQ0MsWUFBVSxDQUNUO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxvQkFGVDtBQUdDLFlBQVE7QUFIVCxHQURTLENBRFg7QUFRQyxxQkFBbUIsWUFScEI7QUFTQyxVQUFRO0FBVFQsQ0FEcUIsRUFZckI7QUFDQyxlQUFhLEtBRGQ7QUFFQyxZQUFVLENBQ1Q7QUFDQyxlQUFXLElBRFo7QUFFQyxvQkFBZ0IsU0FGakI7QUFHQyxZQUFRLE9BSFQ7QUFJQyxZQUFRO0FBSlQsR0FEUyxFQU9UO0FBQ0MsZUFBVyxJQURaO0FBRUMsb0JBQWdCLFNBRmpCO0FBR0MsWUFBUSxVQUhUO0FBSUMsWUFBUTtBQUpULEdBUFMsRUFhVDtBQUNDLGVBQVcsSUFEWjtBQUVDLG9CQUFnQixTQUZqQjtBQUdDLFlBQVEsU0FIVDtBQUlDLFlBQVE7QUFKVCxHQWJTLENBRlg7QUFzQkMsVUFBUSxVQXRCVDtBQXVCQyxVQUFRO0FBdkJULENBWnFCLEVBcUNyQjtBQUNDLGVBQWEsS0FEZDtBQUVDLFlBQVUsQ0FDVDtBQUNDLGVBQVcsSUFEWjtBQUVDLG9CQUFnQixTQUZqQjtBQUdDLFlBQVEsT0FIVDtBQUlDLFlBQVE7QUFKVCxHQURTLEVBT1Q7QUFDQyxlQUFXLElBRFo7QUFFQyxvQkFBZ0IsU0FGakI7QUFHQyxZQUFRLFVBSFQ7QUFJQyxZQUFRO0FBSlQsR0FQUyxFQWFUO0FBQ0MsZUFBVyxLQURaO0FBRUMsb0JBQWdCLE1BRmpCO0FBR0MsWUFBUSxVQUhUO0FBSUMsWUFBUTtBQUpULEdBYlMsQ0FGWDtBQXNCQyxVQUFRLGdCQXRCVDtBQXVCQyxVQUFRO0FBdkJULENBckNxQixFQThEckI7QUFDQyxZQUFVLENBQ1Q7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLElBRlQ7QUFHQyxZQUFRO0FBSFQsR0FEUyxFQU1UO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxTQUZUO0FBR0MsWUFBUTtBQUhULEdBTlMsQ0FEWDtBQWFDLFVBQVEsU0FiVDtBQWNDLGFBQVcsRUFkWjtBQWVDLHFCQUFtQixZQWZwQjtBQWdCQyxVQUFRO0FBaEJULENBOURxQixFQWdGckI7QUFDQyxZQUFVLENBQ1Q7QUFDQyxvQkFBZ0IsUUFEakI7QUFFQyxZQUFRLFVBRlQ7QUFHQyxZQUFRO0FBSFQsR0FEUyxDQURYO0FBUUMsVUFBUSxhQVJUO0FBU0MsYUFBVyxDQUNWO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxFQUZUO0FBR0MsWUFBUTtBQUhULEdBRFUsQ0FUWjtBQWdCQyxxQkFBbUIsWUFoQnBCO0FBaUJDLFVBQVE7QUFqQlQsQ0FoRnFCLEVBbUdyQjtBQUNDLFlBQVUsQ0FDVDtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsTUFGVDtBQUdDLFlBQVE7QUFIVCxHQURTLEVBTVQ7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLElBRlQ7QUFHQyxZQUFRO0FBSFQsR0FOUyxFQVdUO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxTQUZUO0FBR0MsWUFBUTtBQUhULEdBWFMsQ0FEWDtBQWtCQyxVQUFRLGtCQWxCVDtBQW1CQyxhQUFXLEVBbkJaO0FBb0JDLHFCQUFtQixZQXBCcEI7QUFxQkMsVUFBUTtBQXJCVCxDQW5HcUIsRUEwSHJCO0FBQ0MsWUFBVSxDQUNUO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxNQUZUO0FBR0MsWUFBUTtBQUhULEdBRFMsRUFNVDtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsSUFGVDtBQUdDLFlBQVE7QUFIVCxHQU5TLEVBV1Q7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLFNBRlQ7QUFHQyxZQUFRO0FBSFQsR0FYUyxFQWdCVDtBQUNDLG9CQUFnQixPQURqQjtBQUVDLFlBQVEsT0FGVDtBQUdDLFlBQVE7QUFIVCxHQWhCUyxDQURYO0FBdUJDLFVBQVEsa0JBdkJUO0FBd0JDLGFBQVcsRUF4Qlo7QUF5QkMscUJBQW1CLFlBekJwQjtBQTBCQyxVQUFRO0FBMUJULENBMUhxQixFQXNKckI7QUFDQyxZQUFVLENBQ1Q7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLFVBRlQ7QUFHQyxZQUFRO0FBSFQsR0FEUyxFQU1UO0FBQ0Msb0JBQWdCLE1BRGpCO0FBRUMsWUFBUSxVQUZUO0FBR0MsWUFBUTtBQUhULEdBTlMsQ0FEWDtBQWFDLFVBQVEsbUJBYlQ7QUFjQyxhQUFXLEVBZFo7QUFlQyxxQkFBbUIsWUFmcEI7QUFnQkMsVUFBUTtBQWhCVCxDQXRKcUIsRUF3S3JCO0FBQ0MsZUFBYSxLQURkO0FBRUMsWUFBVSxDQUNUO0FBQ0MsZUFBVyxJQURaO0FBRUMsb0JBQWdCLFNBRmpCO0FBR0MsWUFBUSxNQUhUO0FBSUMsWUFBUTtBQUpULEdBRFMsRUFPVDtBQUNDLGVBQVcsSUFEWjtBQUVDLG9CQUFnQixTQUZqQjtBQUdDLFlBQVEsSUFIVDtBQUlDLFlBQVE7QUFKVCxHQVBTLEVBYVQ7QUFDQyxlQUFXLElBRFo7QUFFQyxvQkFBZ0IsU0FGakI7QUFHQyxZQUFRLFNBSFQ7QUFJQyxZQUFRO0FBSlQsR0FiUyxDQUZYO0FBc0JDLFVBQVEsVUF0QlQ7QUF1QkMsVUFBUTtBQXZCVCxDQXhLcUIsRUFpTXJCO0FBQ0MsWUFBVSxDQUNUO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxNQUZUO0FBR0MsWUFBUTtBQUhULEdBRFMsRUFNVDtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsSUFGVDtBQUdDLFlBQVE7QUFIVCxHQU5TLEVBV1Q7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLFNBRlQ7QUFHQyxZQUFRO0FBSFQsR0FYUyxDQURYO0FBa0JDLFVBQVEsY0FsQlQ7QUFtQkMsYUFBVyxFQW5CWjtBQW9CQyxxQkFBbUIsWUFwQnBCO0FBcUJDLFVBQVE7QUFyQlQsQ0FqTXFCLEVBd05yQjtBQUNDLFlBQVUsQ0FDVDtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsT0FGVDtBQUdDLFlBQVE7QUFIVCxHQURTLENBRFg7QUFRQyxVQUFRLFdBUlQ7QUFTQyxhQUFXLENBQ1Y7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLEVBRlQ7QUFHQyxZQUFRO0FBSFQsR0FEVSxDQVRaO0FBZ0JDLHFCQUFtQixNQWhCcEI7QUFpQkMsVUFBUTtBQWpCVCxDQXhOcUIsRUEyT3JCO0FBQ0MsWUFBVSxDQUNUO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxTQUZUO0FBR0MsWUFBUTtBQUhULEdBRFMsQ0FEWDtBQVFDLFVBQVEsYUFSVDtBQVNDLGFBQVcsQ0FDVjtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsRUFGVDtBQUdDLFlBQVE7QUFIVCxHQURVLENBVFo7QUFnQkMscUJBQW1CLE1BaEJwQjtBQWlCQyxVQUFRO0FBakJULENBM09xQixFQThQckI7QUFDQyxZQUFVLENBQ1Q7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLE9BRlQ7QUFHQyxZQUFRO0FBSFQsR0FEUyxFQU1UO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxVQUZUO0FBR0MsWUFBUTtBQUhULEdBTlMsQ0FEWDtBQWFDLFVBQVEsa0JBYlQ7QUFjQyxhQUFXLENBQ1Y7QUFDQyxvQkFBZ0IsTUFEakI7QUFFQyxZQUFRLEVBRlQ7QUFHQyxZQUFRO0FBSFQsR0FEVSxDQWRaO0FBcUJDLHFCQUFtQixNQXJCcEI7QUFzQkMsVUFBUTtBQXRCVCxDQTlQcUIsRUFzUnJCO0FBQ0MsWUFBVSxFQURYO0FBRUMsVUFBUSxNQUZUO0FBR0MsYUFBVyxDQUNWO0FBQ0Msb0JBQWdCLFFBRGpCO0FBRUMsWUFBUSxFQUZUO0FBR0MsWUFBUTtBQUhULEdBRFUsQ0FIWjtBQVVDLHFCQUFtQixNQVZwQjtBQVdDLFVBQVE7QUFYVCxDQXRScUIsRUFtU3JCO0FBQ0MsWUFBVSxDQUNUO0FBQ0Msb0JBQWdCLFNBRGpCO0FBRUMsWUFBUSxTQUZUO0FBR0MsWUFBUTtBQUhULEdBRFMsQ0FEWDtBQVFDLFVBQVEsU0FSVDtBQVNDLGFBQVcsQ0FDVjtBQUNDLG9CQUFnQixTQURqQjtBQUVDLFlBQVEsRUFGVDtBQUdDLFlBQVE7QUFIVCxHQURVLENBVFo7QUFnQkMscUJBQW1CLE1BaEJwQjtBQWlCQyxVQUFRO0FBakJULENBblNxQixFQXNUckI7QUFDQyxZQUFVLENBQ1Q7QUFDQyxvQkFBZ0IsUUFEakI7QUFFQyxZQUFRLGFBRlQ7QUFHQyxZQUFRO0FBSFQsR0FEUyxDQURYO0FBUUMsVUFBUSxtQkFSVDtBQVNDLGFBQVcsQ0FDVjtBQUNDLG9CQUFnQixNQURqQjtBQUVDLFlBQVEsRUFGVDtBQUdDLFlBQVE7QUFIVCxHQURVLENBVFo7QUFnQkMscUJBQW1CLE1BaEJwQjtBQWlCQyxVQUFRO0FBakJULENBdFRxQixFQXlVckI7QUFDQyxZQUFVLEVBRFg7QUFFQyxVQUFRLFFBRlQ7QUFHQyxhQUFXLENBQ1Y7QUFDQyxvQkFBZ0IsUUFEakI7QUFFQyxZQUFRLEVBRlQ7QUFHQyxZQUFRO0FBSFQsR0FEVSxDQUhaO0FBVUMscUJBQW1CLE1BVnBCO0FBV0MsVUFBUTtBQVhULENBelVxQixFQXNWckI7QUFDQyxZQUFVLENBQ1Q7QUFDQyxvQkFBZ0IsU0FEakI7QUFFQyxZQUFRLFNBRlQ7QUFHQyxZQUFRO0FBSFQsR0FEUyxDQURYO0FBUUMsVUFBUSxVQVJUO0FBU0MsYUFBVyxDQUNWO0FBQ0Msb0JBQWdCLFFBRGpCO0FBRUMsWUFBUSxFQUZUO0FBR0MsWUFBUTtBQUhULEdBRFUsQ0FUWjtBQWdCQyxxQkFBbUIsTUFoQnBCO0FBaUJDLFVBQVE7QUFqQlQsQ0F0VnFCLENBQWYsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDOVJQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUdlLFNBQVNDLElBQVQsR0FBZ0I7QUFDN0IsUUFBTTtBQUFBLE9BQUNDLElBQUQ7QUFBQSxPQUFPQztBQUFQLE1BQWtCQywrQ0FBUSxDQUFDLEVBQUQsQ0FBaEM7QUFDQSxRQUFNO0FBQUEsT0FBQ0MsTUFBRDtBQUFBLE9BQVNDO0FBQVQsTUFBc0JGLCtDQUFRLENBQUMsWUFBRCxDQUFwQztBQUVBRyxrREFBUyxDQUFDLE1BQU07QUFDZEMsWUFBUTtBQUNULEdBRlEsRUFFTixFQUZNLENBQVQ7O0FBSUEsaUJBQWVBLFFBQWYsR0FBMEI7QUFDeEIsVUFBTUMsU0FBUyxHQUFHLElBQUlDLGtEQUFKLENBQWM7QUFDOUJDLGFBQU8sRUFBRSxTQURxQjtBQUU5QkMsbUJBQWEsRUFBRTtBQUZlLEtBQWQsQ0FBbEI7QUFJQSxVQUFNQyxVQUFVLEdBQUcsTUFBTUosU0FBUyxDQUFDSyxPQUFWLEVBQXpCO0FBQ0EsVUFBTUMsUUFBUSxHQUFHLElBQUlDLGlFQUFKLENBQWtDSCxVQUFsQyxDQUFqQjtBQUNBLFVBQU1JLE1BQU0sR0FBR0YsUUFBUSxDQUFDRyxTQUFULEVBQWY7QUFFQSxVQUFNQyxjQUFjLEdBQUcsSUFBSUgsbURBQUosQ0FBb0JuQixxREFBcEIsRUFBc0NFLGlEQUF0QyxFQUFvRGtCLE1BQXBELENBQXZCO0FBQ0EsVUFBTUcsYUFBYSxHQUFHLElBQUlKLG1EQUFKLENBQW9CbEIsK0NBQXBCLEVBQWdDRSwyQ0FBaEMsRUFBd0NlLFFBQXhDLENBQXRCO0FBQ0EsVUFBTU0sSUFBSSxHQUFHLE1BQU1GLGNBQWMsQ0FBQ0csV0FBZixFQUFuQjtBQUVBLFVBQU1DLEtBQUssR0FBRyxNQUFNQyxPQUFPLENBQUNDLEdBQVIsQ0FBWUosSUFBSSxDQUFDSyxHQUFMLENBQVMsTUFBTUMsQ0FBTixJQUFXO0FBQ2xELFlBQU1DLFFBQVEsR0FBRyxNQUFNUixhQUFhLENBQUNTLFFBQWQsQ0FBdUJGLENBQUMsQ0FBQ0csT0FBekIsQ0FBdkI7QUFDQSxZQUFNQyxJQUFJLEdBQUcsTUFBTUMsZ0RBQUEsQ0FBVUosUUFBVixDQUFuQjtBQUNBLFVBQUlLLEtBQUssR0FBR0MseURBQUEsQ0FBbUJQLENBQUMsQ0FBQ00sS0FBRixDQUFRRSxRQUFSLEVBQW5CLEVBQXVDLE9BQXZDLENBQVo7QUFDQSxVQUFJQyxJQUFJLEdBQUc7QUFDVEgsYUFEUztBQUVUSCxlQUFPLEVBQUVILENBQUMsQ0FBQ0csT0FBRixDQUFVTyxRQUFWLEVBRkE7QUFHVEMsY0FBTSxFQUFFWCxDQUFDLENBQUNXLE1BSEQ7QUFJVEMsYUFBSyxFQUFFWixDQUFDLENBQUNZLEtBSkE7QUFLVEMsYUFBSyxFQUFFVCxJQUFJLENBQUNWLElBQUwsQ0FBVW1CLEtBTFI7QUFNVEMsWUFBSSxFQUFFVixJQUFJLENBQUNWLElBQUwsQ0FBVW9CLElBTlA7QUFPVEMsbUJBQVcsRUFBRVgsSUFBSSxDQUFDVixJQUFMLENBQVVxQjtBQVBkLE9BQVg7QUFTQSxhQUFPTixJQUFQO0FBQ0QsS0FkK0IsQ0FBWixDQUFwQjtBQWVBTyxXQUFPLENBQUNDLEdBQVIsQ0FBWSxTQUFaLEVBQXVCckIsS0FBdkI7QUFDQXBCLFdBQU8sQ0FBQ29CLEtBQUQsQ0FBUDtBQUNBakIsYUFBUyxDQUFDLFFBQUQsQ0FBVDtBQUNEOztBQUNELE1BQUlELE1BQU0sS0FBSyxRQUFYLElBQXVCLENBQUNILElBQUksQ0FBQzJDLE1BQWpDLEVBQXlDLG9CQUFRO0FBQUksYUFBUyxFQUFDLGVBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFBUjtBQUN6QyxzQkFDRTtBQUFLLGFBQVMsRUFBQyxxQkFBZjtBQUFBLDJCQUNFO0FBQUssV0FBSyxFQUFFO0FBQUVDLGFBQUssRUFBRTtBQUFULE9BQVo7QUFBQSw2QkFDRTtBQUFLLGlCQUFTLEVBQUMsNkJBQWY7QUFBQSxrQkFFSTVDLElBQUksQ0FBQ3dCLEdBQUwsQ0FBUyxDQUFDcUIsR0FBRCxFQUFNcEIsQ0FBTixrQkFDUDtBQUFhLG1CQUFTLEVBQUMsbUJBQXZCO0FBQUEsa0NBQ0U7QUFBSyxlQUFHLEVBQUVvQixHQUFHLENBQUNQLEtBQWQ7QUFBcUIscUJBQVMsRUFBQztBQUEvQjtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQURGLGVBRUU7QUFBRyxxQkFBUyxFQUFDLHlCQUFiO0FBQUEsdUNBQW9ETyxHQUFHLENBQUNkLEtBQXhEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFGRixlQUdFO0FBQUcscUJBQVMsRUFBQyx5QkFBYjtBQUFBLGlDQUE4Q2MsR0FBRyxDQUFDTixJQUFsRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBSEYsZUFJRTtBQUFHLHFCQUFTLEVBQUMseUJBQWI7QUFBQSx3Q0FBcURNLEdBQUcsQ0FBQ0wsV0FBekQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUpGO0FBQUEsV0FBVWYsQ0FBVjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURGO0FBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREY7QUFrQkQsQzs7Ozs7Ozs7Ozs7QUNwRUQsbUM7Ozs7Ozs7Ozs7O0FDQUEsb0M7Ozs7Ozs7Ozs7O0FDQUEsbUM7Ozs7Ozs7Ozs7O0FDQUEsbUQ7Ozs7Ozs7Ozs7O0FDQUEsa0M7Ozs7Ozs7Ozs7O0FDQUEsdUMiLCJmaWxlIjoicGFnZXMvbXktbmZ0cy5qcyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjb25zdCBuZnRtYXJrZXRhZGRyZXNzID0gXCIweDkxMDZmMEFGMDAwQUIwZjgzOGIwMjUxNThlNTgzMmY1OEZlM2EzNUVcIiAgLy9yaW5rZWJ5XG5leHBvcnQgY29uc3QgbmZ0YWRkcmVzcyA9IFwiMHhEODFhYjA4M2U4MzVGQmU3MjFEQjZCNUM3NzQ5ZGM3MkUyNzQ0ZTU3XCIgIC8vcmlua2VieVxuXG5leHBvcnQgY29uc3QgbmZ0bWFya2V0YWJpID0gW1xuXHR7XG5cdFx0XCJpbnB1dHNcIjogW10sXG5cdFx0XCJzdGF0ZU11dGFiaWxpdHlcIjogXCJub25wYXlhYmxlXCIsXG5cdFx0XCJ0eXBlXCI6IFwiY29uc3RydWN0b3JcIlxuXHR9LFxuXHR7XG5cdFx0XCJhbm9ueW1vdXNcIjogZmFsc2UsXG5cdFx0XCJpbnB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImluZGV4ZWRcIjogdHJ1ZSxcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIml0ZW1JZFwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW5kZXhlZFwiOiB0cnVlLFxuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwibmZ0Q29udHJhY3RcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRcImluZGV4ZWRcIjogdHJ1ZSxcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcInRva2VuSWRcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRcImluZGV4ZWRcIjogZmFsc2UsXG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJzZWxsZXJcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRcImluZGV4ZWRcIjogZmFsc2UsXG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJvd25lclwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW5kZXhlZFwiOiBmYWxzZSxcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcInByaWNlXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJuYW1lXCI6IFwiTWFya2V0SXRlbUNyZWF0ZWRcIixcblx0XHRcInR5cGVcIjogXCJldmVudFwiXG5cdH0sXG5cdHtcblx0XHRcImlucHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJuZnRDb250cmFjdFwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJ0b2tlbklkXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcInByaWNlXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJuYW1lXCI6IFwiY3JlYXRlTWFya2V0SXRlbVwiLFxuXHRcdFwib3V0cHV0c1wiOiBbXSxcblx0XHRcInN0YXRlTXV0YWJpbGl0eVwiOiBcInBheWFibGVcIixcblx0XHRcInR5cGVcIjogXCJmdW5jdGlvblwiXG5cdH0sXG5cdHtcblx0XHRcImlucHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJuZnRDb250cmFjdFwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJpdGVtSWRcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcIm5hbWVcIjogXCJjcmVhdGVNYXJrZXRTYWxlXCIsXG5cdFx0XCJvdXRwdXRzXCI6IFtdLFxuXHRcdFwic3RhdGVNdXRhYmlsaXR5XCI6IFwicGF5YWJsZVwiLFxuXHRcdFwidHlwZVwiOiBcImZ1bmN0aW9uXCJcblx0fSxcblx0e1xuXHRcdFwiaW5wdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIml0ZW1JZFwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwibmFtZVwiOiBcImZldGNoTWFya2V0SXRlbVwiLFxuXHRcdFwib3V0cHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiY29tcG9uZW50c1wiOiBbXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFx0XHRcIm5hbWVcIjogXCJpdGVtSWRcIixcblx0XHRcdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFx0XHRcIm5hbWVcIjogXCJuZnRDb250cmFjdFwiLFxuXHRcdFx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XHRcdFwibmFtZVwiOiBcInRva2VuSWRcIixcblx0XHRcdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzIHBheWFibGVcIixcblx0XHRcdFx0XHRcdFwibmFtZVwiOiBcInNlbGxlclwiLFxuXHRcdFx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3MgcGF5YWJsZVwiLFxuXHRcdFx0XHRcdFx0XCJuYW1lXCI6IFwib3duZXJcIixcblx0XHRcdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFx0XHRcIm5hbWVcIjogXCJwcmljZVwiLFxuXHRcdFx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRdLFxuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInN0cnVjdCBORlRNYXJrZXQuTWFya2V0SXRlbVwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwidHVwbGVcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJzdGF0ZU11dGFiaWxpdHlcIjogXCJ2aWV3XCIsXG5cdFx0XCJ0eXBlXCI6IFwiZnVuY3Rpb25cIlxuXHR9LFxuXHR7XG5cdFx0XCJpbnB1dHNcIjogW10sXG5cdFx0XCJuYW1lXCI6IFwiZmV0Y2hNYXJrZXRJdGVtc1wiLFxuXHRcdFwib3V0cHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiY29tcG9uZW50c1wiOiBbXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFx0XHRcIm5hbWVcIjogXCJpdGVtSWRcIixcblx0XHRcdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFx0XHRcIm5hbWVcIjogXCJuZnRDb250cmFjdFwiLFxuXHRcdFx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XHRcdFwibmFtZVwiOiBcInRva2VuSWRcIixcblx0XHRcdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzIHBheWFibGVcIixcblx0XHRcdFx0XHRcdFwibmFtZVwiOiBcInNlbGxlclwiLFxuXHRcdFx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3MgcGF5YWJsZVwiLFxuXHRcdFx0XHRcdFx0XCJuYW1lXCI6IFwib3duZXJcIixcblx0XHRcdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFx0XHRcIm5hbWVcIjogXCJwcmljZVwiLFxuXHRcdFx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRdLFxuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInN0cnVjdCBORlRNYXJrZXQuTWFya2V0SXRlbVtdXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIlwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJ0dXBsZVtdXCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwic3RhdGVNdXRhYmlsaXR5XCI6IFwidmlld1wiLFxuXHRcdFwidHlwZVwiOiBcImZ1bmN0aW9uXCJcblx0fSxcblx0e1xuXHRcdFwiaW5wdXRzXCI6IFtdLFxuXHRcdFwibmFtZVwiOiBcImZldGNoTXlORlRzXCIsXG5cdFx0XCJvdXRwdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJjb21wb25lbnRzXCI6IFtcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XHRcdFwibmFtZVwiOiBcIml0ZW1JZFwiLFxuXHRcdFx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XHRcdFwibmFtZVwiOiBcIm5mdENvbnRyYWN0XCIsXG5cdFx0XHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcdFx0XCJuYW1lXCI6IFwidG9rZW5JZFwiLFxuXHRcdFx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3MgcGF5YWJsZVwiLFxuXHRcdFx0XHRcdFx0XCJuYW1lXCI6IFwic2VsbGVyXCIsXG5cdFx0XHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzcyBwYXlhYmxlXCIsXG5cdFx0XHRcdFx0XHRcIm5hbWVcIjogXCJvd25lclwiLFxuXHRcdFx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XHRcdFwibmFtZVwiOiBcInByaWNlXCIsXG5cdFx0XHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdFx0XHR9XG5cdFx0XHRcdF0sXG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwic3RydWN0IE5GVE1hcmtldC5NYXJrZXRJdGVtW11cIixcblx0XHRcdFx0XCJuYW1lXCI6IFwiXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInR1cGxlW11cIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJzdGF0ZU11dGFiaWxpdHlcIjogXCJ2aWV3XCIsXG5cdFx0XCJ0eXBlXCI6IFwiZnVuY3Rpb25cIlxuXHR9LFxuXHR7XG5cdFx0XCJpbnB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwibWFya2V0SXRlbUlkXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJuYW1lXCI6IFwiZ2V0TWFya2V0SXRlbVwiLFxuXHRcdFwib3V0cHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiY29tcG9uZW50c1wiOiBbXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFx0XHRcIm5hbWVcIjogXCJpdGVtSWRcIixcblx0XHRcdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFx0XHRcIm5hbWVcIjogXCJuZnRDb250cmFjdFwiLFxuXHRcdFx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XHRcdFwibmFtZVwiOiBcInRva2VuSWRcIixcblx0XHRcdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzIHBheWFibGVcIixcblx0XHRcdFx0XHRcdFwibmFtZVwiOiBcInNlbGxlclwiLFxuXHRcdFx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3MgcGF5YWJsZVwiLFxuXHRcdFx0XHRcdFx0XCJuYW1lXCI6IFwib3duZXJcIixcblx0XHRcdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFx0XHRcIm5hbWVcIjogXCJwcmljZVwiLFxuXHRcdFx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRdLFxuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInN0cnVjdCBORlRNYXJrZXQuTWFya2V0SXRlbVwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwidHVwbGVcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJzdGF0ZU11dGFiaWxpdHlcIjogXCJ2aWV3XCIsXG5cdFx0XCJ0eXBlXCI6IFwiZnVuY3Rpb25cIlxuXHR9XG5dXG5leHBvcnQgY29uc3QgbmZ0YWJpID0gW1xuXHR7XG5cdFx0XCJpbnB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwibWFya2V0cGxhY2VBZGRyZXNzXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJzdGF0ZU11dGFiaWxpdHlcIjogXCJub25wYXlhYmxlXCIsXG5cdFx0XCJ0eXBlXCI6IFwiY29uc3RydWN0b3JcIlxuXHR9LFxuXHR7XG5cdFx0XCJhbm9ueW1vdXNcIjogZmFsc2UsXG5cdFx0XCJpbnB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImluZGV4ZWRcIjogdHJ1ZSxcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIm93bmVyXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0XCJpbmRleGVkXCI6IHRydWUsXG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJhcHByb3ZlZFwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW5kZXhlZFwiOiB0cnVlLFxuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwidG9rZW5JZFwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwibmFtZVwiOiBcIkFwcHJvdmFsXCIsXG5cdFx0XCJ0eXBlXCI6IFwiZXZlbnRcIlxuXHR9LFxuXHR7XG5cdFx0XCJhbm9ueW1vdXNcIjogZmFsc2UsXG5cdFx0XCJpbnB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImluZGV4ZWRcIjogdHJ1ZSxcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIm93bmVyXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0XCJpbmRleGVkXCI6IHRydWUsXG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJvcGVyYXRvclwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW5kZXhlZFwiOiBmYWxzZSxcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJib29sXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcImFwcHJvdmVkXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImJvb2xcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJuYW1lXCI6IFwiQXBwcm92YWxGb3JBbGxcIixcblx0XHRcInR5cGVcIjogXCJldmVudFwiXG5cdH0sXG5cdHtcblx0XHRcImlucHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJ0b1wiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJ0b2tlbklkXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJuYW1lXCI6IFwiYXBwcm92ZVwiLFxuXHRcdFwib3V0cHV0c1wiOiBbXSxcblx0XHRcInN0YXRlTXV0YWJpbGl0eVwiOiBcIm5vbnBheWFibGVcIixcblx0XHRcInR5cGVcIjogXCJmdW5jdGlvblwiXG5cdH0sXG5cdHtcblx0XHRcImlucHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwic3RyaW5nXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcInRva2VuVVJJXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInN0cmluZ1wiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcIm5hbWVcIjogXCJjcmVhdGVUb2tlblwiLFxuXHRcdFwib3V0cHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcInN0YXRlTXV0YWJpbGl0eVwiOiBcIm5vbnBheWFibGVcIixcblx0XHRcInR5cGVcIjogXCJmdW5jdGlvblwiXG5cdH0sXG5cdHtcblx0XHRcImlucHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJmcm9tXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcInRvXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcInRva2VuSWRcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcIm5hbWVcIjogXCJzYWZlVHJhbnNmZXJGcm9tXCIsXG5cdFx0XCJvdXRwdXRzXCI6IFtdLFxuXHRcdFwic3RhdGVNdXRhYmlsaXR5XCI6IFwibm9ucGF5YWJsZVwiLFxuXHRcdFwidHlwZVwiOiBcImZ1bmN0aW9uXCJcblx0fSxcblx0e1xuXHRcdFwiaW5wdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcImZyb21cIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwidG9cIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwidG9rZW5JZFwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYnl0ZXNcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwiX2RhdGFcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYnl0ZXNcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJuYW1lXCI6IFwic2FmZVRyYW5zZmVyRnJvbVwiLFxuXHRcdFwib3V0cHV0c1wiOiBbXSxcblx0XHRcInN0YXRlTXV0YWJpbGl0eVwiOiBcIm5vbnBheWFibGVcIixcblx0XHRcInR5cGVcIjogXCJmdW5jdGlvblwiXG5cdH0sXG5cdHtcblx0XHRcImlucHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJvcGVyYXRvclwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYm9vbFwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJhcHByb3ZlZFwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJib29sXCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwibmFtZVwiOiBcInNldEFwcHJvdmFsRm9yQWxsXCIsXG5cdFx0XCJvdXRwdXRzXCI6IFtdLFxuXHRcdFwic3RhdGVNdXRhYmlsaXR5XCI6IFwibm9ucGF5YWJsZVwiLFxuXHRcdFwidHlwZVwiOiBcImZ1bmN0aW9uXCJcblx0fSxcblx0e1xuXHRcdFwiYW5vbnltb3VzXCI6IGZhbHNlLFxuXHRcdFwiaW5wdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbmRleGVkXCI6IHRydWUsXG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJmcm9tXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0XCJpbmRleGVkXCI6IHRydWUsXG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJ0b1wiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW5kZXhlZFwiOiB0cnVlLFxuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwidG9rZW5JZFwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwibmFtZVwiOiBcIlRyYW5zZmVyXCIsXG5cdFx0XCJ0eXBlXCI6IFwiZXZlbnRcIlxuXHR9LFxuXHR7XG5cdFx0XCJpbnB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwiZnJvbVwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJ0b1wiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJ0b2tlbklkXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJuYW1lXCI6IFwidHJhbnNmZXJGcm9tXCIsXG5cdFx0XCJvdXRwdXRzXCI6IFtdLFxuXHRcdFwic3RhdGVNdXRhYmlsaXR5XCI6IFwibm9ucGF5YWJsZVwiLFxuXHRcdFwidHlwZVwiOiBcImZ1bmN0aW9uXCJcblx0fSxcblx0e1xuXHRcdFwiaW5wdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJhZGRyZXNzXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIm93bmVyXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJuYW1lXCI6IFwiYmFsYW5jZU9mXCIsXG5cdFx0XCJvdXRwdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIlwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwic3RhdGVNdXRhYmlsaXR5XCI6IFwidmlld1wiLFxuXHRcdFwidHlwZVwiOiBcImZ1bmN0aW9uXCJcblx0fSxcblx0e1xuXHRcdFwiaW5wdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJ1aW50MjU2XCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcInRva2VuSWRcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwidWludDI1NlwiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcIm5hbWVcIjogXCJnZXRBcHByb3ZlZFwiLFxuXHRcdFwib3V0cHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwiYWRkcmVzc1wiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcInN0YXRlTXV0YWJpbGl0eVwiOiBcInZpZXdcIixcblx0XHRcInR5cGVcIjogXCJmdW5jdGlvblwiXG5cdH0sXG5cdHtcblx0XHRcImlucHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJvd25lclwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwiYWRkcmVzc1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJvcGVyYXRvclwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJhZGRyZXNzXCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwibmFtZVwiOiBcImlzQXBwcm92ZWRGb3JBbGxcIixcblx0XHRcIm91dHB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImJvb2xcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwiXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImJvb2xcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJzdGF0ZU11dGFiaWxpdHlcIjogXCJ2aWV3XCIsXG5cdFx0XCJ0eXBlXCI6IFwiZnVuY3Rpb25cIlxuXHR9LFxuXHR7XG5cdFx0XCJpbnB1dHNcIjogW10sXG5cdFx0XCJuYW1lXCI6IFwibmFtZVwiLFxuXHRcdFwib3V0cHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwic3RyaW5nXCIsXG5cdFx0XHRcdFwibmFtZVwiOiBcIlwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJzdHJpbmdcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJzdGF0ZU11dGFiaWxpdHlcIjogXCJ2aWV3XCIsXG5cdFx0XCJ0eXBlXCI6IFwiZnVuY3Rpb25cIlxuXHR9LFxuXHR7XG5cdFx0XCJpbnB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInVpbnQyNTZcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwidG9rZW5JZFwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJ1aW50MjU2XCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwibmFtZVwiOiBcIm93bmVyT2ZcIixcblx0XHRcIm91dHB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImFkZHJlc3NcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwiXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImFkZHJlc3NcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJzdGF0ZU11dGFiaWxpdHlcIjogXCJ2aWV3XCIsXG5cdFx0XCJ0eXBlXCI6IFwiZnVuY3Rpb25cIlxuXHR9LFxuXHR7XG5cdFx0XCJpbnB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImJ5dGVzNFwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJpbnRlcmZhY2VJZFwiLFxuXHRcdFx0XHRcInR5cGVcIjogXCJieXRlczRcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJuYW1lXCI6IFwic3VwcG9ydHNJbnRlcmZhY2VcIixcblx0XHRcIm91dHB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcImJvb2xcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwiXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcImJvb2xcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJzdGF0ZU11dGFiaWxpdHlcIjogXCJ2aWV3XCIsXG5cdFx0XCJ0eXBlXCI6IFwiZnVuY3Rpb25cIlxuXHR9LFxuXHR7XG5cdFx0XCJpbnB1dHNcIjogW10sXG5cdFx0XCJuYW1lXCI6IFwic3ltYm9sXCIsXG5cdFx0XCJvdXRwdXRzXCI6IFtcblx0XHRcdHtcblx0XHRcdFx0XCJpbnRlcm5hbFR5cGVcIjogXCJzdHJpbmdcIixcblx0XHRcdFx0XCJuYW1lXCI6IFwiXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInN0cmluZ1wiXG5cdFx0XHR9XG5cdFx0XSxcblx0XHRcInN0YXRlTXV0YWJpbGl0eVwiOiBcInZpZXdcIixcblx0XHRcInR5cGVcIjogXCJmdW5jdGlvblwiXG5cdH0sXG5cdHtcblx0XHRcImlucHV0c1wiOiBbXG5cdFx0XHR7XG5cdFx0XHRcdFwiaW50ZXJuYWxUeXBlXCI6IFwidWludDI1NlwiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJ0b2tlbklkXCIsXG5cdFx0XHRcdFwidHlwZVwiOiBcInVpbnQyNTZcIlxuXHRcdFx0fVxuXHRcdF0sXG5cdFx0XCJuYW1lXCI6IFwidG9rZW5VUklcIixcblx0XHRcIm91dHB1dHNcIjogW1xuXHRcdFx0e1xuXHRcdFx0XHRcImludGVybmFsVHlwZVwiOiBcInN0cmluZ1wiLFxuXHRcdFx0XHRcIm5hbWVcIjogXCJcIixcblx0XHRcdFx0XCJ0eXBlXCI6IFwic3RyaW5nXCJcblx0XHRcdH1cblx0XHRdLFxuXHRcdFwic3RhdGVNdXRhYmlsaXR5XCI6IFwidmlld1wiLFxuXHRcdFwidHlwZVwiOiBcImZ1bmN0aW9uXCJcblx0fVxuXSIsImltcG9ydCB7IGV0aGVycyB9IGZyb20gJ2V0aGVycydcbmltcG9ydCB7IHVzZUVmZmVjdCwgdXNlU3RhdGUgfSBmcm9tICdyZWFjdCdcbmltcG9ydCB3ZWIzIGZyb20gJ3dlYjMnXG5pbXBvcnQgYXhpb3MgZnJvbSAnYXhpb3MnXG5pbXBvcnQgV2ViM01vZGFsIGZyb20gXCJ3ZWIzbW9kYWxcIlxuXG5pbXBvcnQge1xuICBuZnRtYXJrZXRhZGRyZXNzLCBuZnRhZGRyZXNzLCBuZnRhYmksIG5mdG1hcmtldGFiaVxufSBmcm9tICcuLi9jb25maWcnXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBIb21lKCkge1xuICBjb25zdCBbbmZ0cywgc2V0TmZ0c10gPSB1c2VTdGF0ZShbXSlcbiAgY29uc3QgW2xvYWRlZCwgc2V0TG9hZGVkXSA9IHVzZVN0YXRlKCdub3QtbG9hZGVkJylcblxuICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgIGxvYWRORlRzKClcbiAgfSwgW10pXG5cbiAgYXN5bmMgZnVuY3Rpb24gbG9hZE5GVHMoKSB7XG4gICAgY29uc3Qgd2ViM01vZGFsID0gbmV3IFdlYjNNb2RhbCh7XG4gICAgICBuZXR3b3JrOiBcIm1haW5uZXRcIixcbiAgICAgIGNhY2hlUHJvdmlkZXI6IHRydWUsXG4gICAgfSk7XG4gICAgY29uc3QgY29ubmVjdGlvbiA9IGF3YWl0IHdlYjNNb2RhbC5jb25uZWN0KClcbiAgICBjb25zdCBwcm92aWRlciA9IG5ldyBldGhlcnMucHJvdmlkZXJzLldlYjNQcm92aWRlcihjb25uZWN0aW9uKVxuICAgIGNvbnN0IHNpZ25lciA9IHByb3ZpZGVyLmdldFNpZ25lcigpXG5cbiAgICBjb25zdCBtYXJrZXRDb250cmFjdCA9IG5ldyBldGhlcnMuQ29udHJhY3QobmZ0bWFya2V0YWRkcmVzcywgbmZ0bWFya2V0YWJpLCBzaWduZXIpXG4gICAgY29uc3QgdG9rZW5Db250cmFjdCA9IG5ldyBldGhlcnMuQ29udHJhY3QobmZ0YWRkcmVzcywgbmZ0YWJpLCBwcm92aWRlcilcbiAgICBjb25zdCBkYXRhID0gYXdhaXQgbWFya2V0Q29udHJhY3QuZmV0Y2hNeU5GVHMoKVxuXG4gICAgY29uc3QgaXRlbXMgPSBhd2FpdCBQcm9taXNlLmFsbChkYXRhLm1hcChhc3luYyBpID0+IHtcbiAgICAgIGNvbnN0IHRva2VuVXJpID0gYXdhaXQgdG9rZW5Db250cmFjdC50b2tlblVSSShpLnRva2VuSWQpXG4gICAgICBjb25zdCBtZXRhID0gYXdhaXQgYXhpb3MuZ2V0KHRva2VuVXJpKVxuICAgICAgbGV0IHByaWNlID0gd2ViMy51dGlscy5mcm9tV2VpKGkucHJpY2UudG9TdHJpbmcoKSwgJ2V0aGVyJyk7XG4gICAgICBsZXQgaXRlbSA9IHtcbiAgICAgICAgcHJpY2UsXG4gICAgICAgIHRva2VuSWQ6IGkudG9rZW5JZC50b051bWJlcigpLFxuICAgICAgICBzZWxsZXI6IGkuc2VsbGVyLFxuICAgICAgICBvd25lcjogaS5vd25lcixcbiAgICAgICAgaW1hZ2U6IG1ldGEuZGF0YS5pbWFnZSxcbiAgICAgICAgbmFtZTogbWV0YS5kYXRhLm5hbWUsXG4gICAgICAgIGRlc2NyaXB0aW9uOiBtZXRhLmRhdGEuZGVzY3JpcHRpb24sXG4gICAgICB9XG4gICAgICByZXR1cm4gaXRlbVxuICAgIH0pKVxuICAgIGNvbnNvbGUubG9nKCdpdGVtczogJywgaXRlbXMpXG4gICAgc2V0TmZ0cyhpdGVtcylcbiAgICBzZXRMb2FkZWQoJ2xvYWRlZCcpXG4gIH1cbiAgaWYgKGxvYWRlZCA9PT0gJ2xvYWRlZCcgJiYgIW5mdHMubGVuZ3RoKSByZXR1cm4gKDxoMSBjbGFzc05hbWU9XCJwLTIwIHRleHQtNHhsXCI+Tm8gTkZUcyE8L2gxPilcbiAgcmV0dXJuIChcbiAgICA8ZGl2IGNsYXNzTmFtZT1cImZsZXgganVzdGlmeS1jZW50ZXJcIj5cbiAgICAgIDxkaXYgc3R5bGU9e3sgd2lkdGg6IDkwMCB9fT5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJncmlkIGdyaWQtY29scy0yIGdhcC00IHB0LThcIj5cbiAgICAgICAgICB7XG4gICAgICAgICAgICBuZnRzLm1hcCgobmZ0LCBpKSA9PiAoXG4gICAgICAgICAgICAgIDxkaXYga2V5PXtpfSBjbGFzc05hbWU9XCJib3JkZXIgcC00IHNoYWRvd1wiPlxuICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtuZnQuaW1hZ2V9IGNsYXNzTmFtZT1cInJvdW5kZWRcIiAvPlxuICAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cInRleHQtMnhsIG15LTQgZm9udC1ib2xkXCI+UHJpY2UgcGFpZDoge25mdC5wcmljZX0gRXRoZXI8L3A+XG4gICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwidGV4dC0yeGwgbXktNCBmb250LWJvbGRcIj5OYW1lOiB7bmZ0Lm5hbWV9PC9wPlxuICAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cInRleHQtMnhsIG15LTQgZm9udC1ib2xkXCI+RGVzY3JpcHRpb246IHtuZnQuZGVzY3JpcHRpb259PC9wPlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICkpXG4gICAgICAgICAgfVxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApXG59XG4iLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJheGlvc1wiKTs7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiZXRoZXJzXCIpOzsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdFwiKTs7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3QvanN4LWRldi1ydW50aW1lXCIpOzsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJ3ZWIzXCIpOzsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJ3ZWIzbW9kYWxcIik7OyJdLCJzb3VyY2VSb290IjoiIn0=