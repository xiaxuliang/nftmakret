self["webpackHotUpdate_N_E"]("pages/my-nfts",{

/***/ "./pages/my-nfts.js":
/*!**************************!*\
  !*** ./pages/my-nfts.js ***!
  \**************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ Home; }
/* harmony export */ });
/* harmony import */ var E_marketplace_complete_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/regenerator */ "./node_modules/next/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var E_marketplace_complete_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(E_marketplace_complete_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var E_marketplace_complete_node_modules_next_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var ethers__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ethers */ "./node_modules/ethers/lib.esm/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var web3__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! web3 */ "./node_modules/web3/lib/index.js");
/* harmony import */ var web3__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(web3__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var web3modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! web3modal */ "./node_modules/web3modal/dist/index.js");
/* harmony import */ var web3modal__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(web3modal__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../config */ "./config.js");
/* module decorator */ module = __webpack_require__.hmd(module);




var _jsxFileName = "E:\\marketplace-complete\\pages\\my-nfts.js",
    _s = $RefreshSig$();







function Home() {
  _s();

  var _this = this;

  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)([]),
      nfts = _useState[0],
      setNfts = _useState[1];

  var _useState2 = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)('not-loaded'),
      loaded = _useState2[0],
      setLoaded = _useState2[1];

  (0,react__WEBPACK_IMPORTED_MODULE_3__.useEffect)(function () {
    loadNFTs();
  }, []);

  function loadNFTs() {
    return _loadNFTs.apply(this, arguments);
  }

  function _loadNFTs() {
    _loadNFTs = (0,E_marketplace_complete_node_modules_next_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__.default)( /*#__PURE__*/E_marketplace_complete_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee2() {
      var web3Modal, connection, provider, signer, marketContract, tokenContract, data, items;
      return E_marketplace_complete_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              web3Modal = new (web3modal__WEBPACK_IMPORTED_MODULE_6___default())({
                network: "mainnet",
                cacheProvider: true
              });
              _context2.next = 3;
              return web3Modal.connect();

            case 3:
              connection = _context2.sent;
              provider = new ethers__WEBPACK_IMPORTED_MODULE_8__.ethers.providers.Web3Provider(connection);
              signer = provider.getSigner();
              marketContract = new ethers__WEBPACK_IMPORTED_MODULE_8__.ethers.Contract(_config__WEBPACK_IMPORTED_MODULE_7__.nftmarketaddress, _config__WEBPACK_IMPORTED_MODULE_7__.nftmarketabi, signer);
              tokenContract = new ethers__WEBPACK_IMPORTED_MODULE_8__.ethers.Contract(_config__WEBPACK_IMPORTED_MODULE_7__.nftaddress, _config__WEBPACK_IMPORTED_MODULE_7__.nftabi, provider);
              _context2.next = 10;
              return marketContract.fetchMyNFTs();

            case 10:
              data = _context2.sent;
              _context2.next = 13;
              return Promise.all(data.map( /*#__PURE__*/function () {
                var _ref = (0,E_marketplace_complete_node_modules_next_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__.default)( /*#__PURE__*/E_marketplace_complete_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee(i) {
                  var tokenUri, meta, price, item;
                  return E_marketplace_complete_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee$(_context) {
                    while (1) {
                      switch (_context.prev = _context.next) {
                        case 0:
                          _context.next = 2;
                          return tokenContract.tokenURI(i.tokenId);

                        case 2:
                          tokenUri = _context.sent;
                          _context.next = 5;
                          return axios__WEBPACK_IMPORTED_MODULE_5___default().get(tokenUri);

                        case 5:
                          meta = _context.sent;
                          price = web3__WEBPACK_IMPORTED_MODULE_4___default().utils.fromWei(i.price.toString(), 'ether');
                          item = {
                            price: price,
                            tokenId: i.tokenId.toNumber(),
                            seller: i.seller,
                            owner: i.owner,
                            image: meta.data.image,
                            name: meta.data.name,
                            description: meta.data.description
                          };
                          return _context.abrupt("return", item);

                        case 9:
                        case "end":
                          return _context.stop();
                      }
                    }
                  }, _callee);
                }));

                return function (_x) {
                  return _ref.apply(this, arguments);
                };
              }()));

            case 13:
              items = _context2.sent;
              console.log('items: ', items);
              setNfts(items);
              setLoaded('loaded');

            case 17:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));
    return _loadNFTs.apply(this, arguments);
  }

  if (loaded === 'loaded' && !nfts.length) return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("h1", {
    className: "p-20 text-4xl",
    children: "No NFTs!"
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 50,
    columnNumber: 52
  }, this);
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("div", {
    className: "flex justify-center",
    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("div", {
      style: {
        width: 900
      },
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("div", {
        className: "grid grid-cols-2 gap-4 pt-8",
        children: nfts.map(function (nft, i) {
          return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("div", {
            className: "border p-4 shadow",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("img", {
              src: nft.image,
              className: "rounded"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 58,
              columnNumber: 17
            }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("p", {
              className: "text-2xl my-4 font-bold",
              children: ["Price paid: ", nft.price, " Ether"]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 59,
              columnNumber: 17
            }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("p", {
              className: "text-2xl my-4 font-bold",
              children: ["Name: ", nft.name]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 60,
              columnNumber: 17
            }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("p", {
              className: "text-2xl my-4 font-bold",
              children: ["Description: ", nft.description]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 61,
              columnNumber: 17
            }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("button", {
              className: "rounded bg-blue-600 py-2 px-12 text-white m-16",
              children: "Sell"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 62,
              columnNumber: 17
            }, _this)]
          }, i, true, {
            fileName: _jsxFileName,
            lineNumber: 57,
            columnNumber: 15
          }, _this);
        })
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 54,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 53,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 52,
    columnNumber: 5
  }, this);
}

_s(Home, "l4Oq+AVPuMX79hZM8JqKmB+X57I=");

_c = Home;

var _c;

$RefreshReg$(_c, "Home");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.id);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }


/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvbXktbmZ0cy5qcyJdLCJuYW1lcyI6WyJIb21lIiwidXNlU3RhdGUiLCJuZnRzIiwic2V0TmZ0cyIsImxvYWRlZCIsInNldExvYWRlZCIsInVzZUVmZmVjdCIsImxvYWRORlRzIiwid2ViM01vZGFsIiwiV2ViM01vZGFsIiwibmV0d29yayIsImNhY2hlUHJvdmlkZXIiLCJjb25uZWN0IiwiY29ubmVjdGlvbiIsInByb3ZpZGVyIiwiZXRoZXJzIiwic2lnbmVyIiwiZ2V0U2lnbmVyIiwibWFya2V0Q29udHJhY3QiLCJuZnRtYXJrZXRhZGRyZXNzIiwibmZ0bWFya2V0YWJpIiwidG9rZW5Db250cmFjdCIsIm5mdGFkZHJlc3MiLCJuZnRhYmkiLCJmZXRjaE15TkZUcyIsImRhdGEiLCJQcm9taXNlIiwiYWxsIiwibWFwIiwiaSIsInRva2VuVVJJIiwidG9rZW5JZCIsInRva2VuVXJpIiwiYXhpb3MiLCJtZXRhIiwicHJpY2UiLCJ3ZWIzIiwidG9TdHJpbmciLCJpdGVtIiwidG9OdW1iZXIiLCJzZWxsZXIiLCJvd25lciIsImltYWdlIiwibmFtZSIsImRlc2NyaXB0aW9uIiwiaXRlbXMiLCJjb25zb2xlIiwibG9nIiwibGVuZ3RoIiwid2lkdGgiLCJuZnQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUdlLFNBQVNBLElBQVQsR0FBZ0I7QUFBQTs7QUFBQTs7QUFBQSxrQkFDTEMsK0NBQVEsQ0FBQyxFQUFELENBREg7QUFBQSxNQUN0QkMsSUFEc0I7QUFBQSxNQUNoQkMsT0FEZ0I7O0FBQUEsbUJBRURGLCtDQUFRLENBQUMsWUFBRCxDQUZQO0FBQUEsTUFFdEJHLE1BRnNCO0FBQUEsTUFFZEMsU0FGYzs7QUFJN0JDLGtEQUFTLENBQUMsWUFBTTtBQUNkQyxZQUFRO0FBQ1QsR0FGUSxFQUVOLEVBRk0sQ0FBVDs7QUFKNkIsV0FRZEEsUUFSYztBQUFBO0FBQUE7O0FBQUE7QUFBQSxvU0FRN0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ1FDLHVCQURSLEdBQ29CLElBQUlDLGtEQUFKLENBQWM7QUFDOUJDLHVCQUFPLEVBQUUsU0FEcUI7QUFFOUJDLDZCQUFhLEVBQUU7QUFGZSxlQUFkLENBRHBCO0FBQUE7QUFBQSxxQkFLMkJILFNBQVMsQ0FBQ0ksT0FBVixFQUwzQjs7QUFBQTtBQUtRQyx3QkFMUjtBQU1RQyxzQkFOUixHQU1tQixJQUFJQyxpRUFBSixDQUFrQ0YsVUFBbEMsQ0FObkI7QUFPUUcsb0JBUFIsR0FPaUJGLFFBQVEsQ0FBQ0csU0FBVCxFQVBqQjtBQVNRQyw0QkFUUixHQVN5QixJQUFJSCxtREFBSixDQUFvQkkscURBQXBCLEVBQXNDQyxpREFBdEMsRUFBb0RKLE1BQXBELENBVHpCO0FBVVFLLDJCQVZSLEdBVXdCLElBQUlOLG1EQUFKLENBQW9CTywrQ0FBcEIsRUFBZ0NDLDJDQUFoQyxFQUF3Q1QsUUFBeEMsQ0FWeEI7QUFBQTtBQUFBLHFCQVdxQkksY0FBYyxDQUFDTSxXQUFmLEVBWHJCOztBQUFBO0FBV1FDLGtCQVhSO0FBQUE7QUFBQSxxQkFhc0JDLE9BQU8sQ0FBQ0MsR0FBUixDQUFZRixJQUFJLENBQUNHLEdBQUw7QUFBQSwrU0FBUyxpQkFBTUMsQ0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlDQUNoQlIsYUFBYSxDQUFDUyxRQUFkLENBQXVCRCxDQUFDLENBQUNFLE9BQXpCLENBRGdCOztBQUFBO0FBQ2pDQyxrQ0FEaUM7QUFBQTtBQUFBLGlDQUVwQkMsZ0RBQUEsQ0FBVUQsUUFBVixDQUZvQjs7QUFBQTtBQUVqQ0UsOEJBRmlDO0FBR25DQywrQkFIbUMsR0FHM0JDLHlEQUFBLENBQW1CUCxDQUFDLENBQUNNLEtBQUYsQ0FBUUUsUUFBUixFQUFuQixFQUF1QyxPQUF2QyxDQUgyQjtBQUluQ0MsOEJBSm1DLEdBSTVCO0FBQ1RILGlDQUFLLEVBQUxBLEtBRFM7QUFFVEosbUNBQU8sRUFBRUYsQ0FBQyxDQUFDRSxPQUFGLENBQVVRLFFBQVYsRUFGQTtBQUdUQyxrQ0FBTSxFQUFFWCxDQUFDLENBQUNXLE1BSEQ7QUFJVEMsaUNBQUssRUFBRVosQ0FBQyxDQUFDWSxLQUpBO0FBS1RDLGlDQUFLLEVBQUVSLElBQUksQ0FBQ1QsSUFBTCxDQUFVaUIsS0FMUjtBQU1UQyxnQ0FBSSxFQUFFVCxJQUFJLENBQUNULElBQUwsQ0FBVWtCLElBTlA7QUFPVEMsdUNBQVcsRUFBRVYsSUFBSSxDQUFDVCxJQUFMLENBQVVtQjtBQVBkLDJCQUo0QjtBQUFBLDJEQWFoQ04sSUFiZ0M7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBQVQ7O0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBQVosQ0FidEI7O0FBQUE7QUFhUU8sbUJBYlI7QUE0QkVDLHFCQUFPLENBQUNDLEdBQVIsQ0FBWSxTQUFaLEVBQXVCRixLQUF2QjtBQUNBMUMscUJBQU8sQ0FBQzBDLEtBQUQsQ0FBUDtBQUNBeEMsdUJBQVMsQ0FBQyxRQUFELENBQVQ7O0FBOUJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBUjZCO0FBQUE7QUFBQTs7QUF3QzdCLE1BQUlELE1BQU0sS0FBSyxRQUFYLElBQXVCLENBQUNGLElBQUksQ0FBQzhDLE1BQWpDLEVBQXlDLG9CQUFRO0FBQUksYUFBUyxFQUFDLGVBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFBUjtBQUN6QyxzQkFDRTtBQUFLLGFBQVMsRUFBQyxxQkFBZjtBQUFBLDJCQUNFO0FBQUssV0FBSyxFQUFFO0FBQUVDLGFBQUssRUFBRTtBQUFULE9BQVo7QUFBQSw2QkFDRTtBQUFLLGlCQUFTLEVBQUMsNkJBQWY7QUFBQSxrQkFFSS9DLElBQUksQ0FBQzBCLEdBQUwsQ0FBUyxVQUFDc0IsR0FBRCxFQUFNckIsQ0FBTjtBQUFBLDhCQUNQO0FBQWEscUJBQVMsRUFBQyxtQkFBdkI7QUFBQSxvQ0FDRTtBQUFLLGlCQUFHLEVBQUVxQixHQUFHLENBQUNSLEtBQWQ7QUFBcUIsdUJBQVMsRUFBQztBQUEvQjtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQURGLGVBRUU7QUFBRyx1QkFBUyxFQUFDLHlCQUFiO0FBQUEseUNBQW9EUSxHQUFHLENBQUNmLEtBQXhEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFGRixlQUdFO0FBQUcsdUJBQVMsRUFBQyx5QkFBYjtBQUFBLG1DQUE4Q2UsR0FBRyxDQUFDUCxJQUFsRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBSEYsZUFJRTtBQUFHLHVCQUFTLEVBQUMseUJBQWI7QUFBQSwwQ0FBcURPLEdBQUcsQ0FBQ04sV0FBekQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQUpGLGVBS0U7QUFBUSx1QkFBUyxFQUFDLGdEQUFsQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFMRjtBQUFBLGFBQVVmLENBQVY7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFETztBQUFBLFNBQVQ7QUFGSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFERjtBQW1CRDs7R0E1RHVCN0IsSTs7S0FBQUEsSSIsImZpbGUiOiJzdGF0aWMvd2VicGFjay9wYWdlcy9teS1uZnRzLjlkMGYyMzJlYWMyOTQ1M2I4ZjNlLmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBldGhlcnMgfSBmcm9tICdldGhlcnMnXG5pbXBvcnQgeyB1c2VFZmZlY3QsIHVzZVN0YXRlIH0gZnJvbSAncmVhY3QnXG5pbXBvcnQgd2ViMyBmcm9tICd3ZWIzJ1xuaW1wb3J0IGF4aW9zIGZyb20gJ2F4aW9zJ1xuaW1wb3J0IFdlYjNNb2RhbCBmcm9tIFwid2ViM21vZGFsXCJcblxuaW1wb3J0IHtcbiAgbmZ0bWFya2V0YWRkcmVzcywgbmZ0YWRkcmVzcywgbmZ0YWJpLCBuZnRtYXJrZXRhYmlcbn0gZnJvbSAnLi4vY29uZmlnJ1xuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gSG9tZSgpIHtcbiAgY29uc3QgW25mdHMsIHNldE5mdHNdID0gdXNlU3RhdGUoW10pXG4gIGNvbnN0IFtsb2FkZWQsIHNldExvYWRlZF0gPSB1c2VTdGF0ZSgnbm90LWxvYWRlZCcpXG5cbiAgdXNlRWZmZWN0KCgpID0+IHtcbiAgICBsb2FkTkZUcygpXG4gIH0sIFtdKVxuXG4gIGFzeW5jIGZ1bmN0aW9uIGxvYWRORlRzKCkge1xuICAgIGNvbnN0IHdlYjNNb2RhbCA9IG5ldyBXZWIzTW9kYWwoe1xuICAgICAgbmV0d29yazogXCJtYWlubmV0XCIsXG4gICAgICBjYWNoZVByb3ZpZGVyOiB0cnVlLFxuICAgIH0pO1xuICAgIGNvbnN0IGNvbm5lY3Rpb24gPSBhd2FpdCB3ZWIzTW9kYWwuY29ubmVjdCgpXG4gICAgY29uc3QgcHJvdmlkZXIgPSBuZXcgZXRoZXJzLnByb3ZpZGVycy5XZWIzUHJvdmlkZXIoY29ubmVjdGlvbilcbiAgICBjb25zdCBzaWduZXIgPSBwcm92aWRlci5nZXRTaWduZXIoKVxuXG4gICAgY29uc3QgbWFya2V0Q29udHJhY3QgPSBuZXcgZXRoZXJzLkNvbnRyYWN0KG5mdG1hcmtldGFkZHJlc3MsIG5mdG1hcmtldGFiaSwgc2lnbmVyKVxuICAgIGNvbnN0IHRva2VuQ29udHJhY3QgPSBuZXcgZXRoZXJzLkNvbnRyYWN0KG5mdGFkZHJlc3MsIG5mdGFiaSwgcHJvdmlkZXIpXG4gICAgY29uc3QgZGF0YSA9IGF3YWl0IG1hcmtldENvbnRyYWN0LmZldGNoTXlORlRzKClcblxuICAgIGNvbnN0IGl0ZW1zID0gYXdhaXQgUHJvbWlzZS5hbGwoZGF0YS5tYXAoYXN5bmMgaSA9PiB7XG4gICAgICBjb25zdCB0b2tlblVyaSA9IGF3YWl0IHRva2VuQ29udHJhY3QudG9rZW5VUkkoaS50b2tlbklkKVxuICAgICAgY29uc3QgbWV0YSA9IGF3YWl0IGF4aW9zLmdldCh0b2tlblVyaSlcbiAgICAgIGxldCBwcmljZSA9IHdlYjMudXRpbHMuZnJvbVdlaShpLnByaWNlLnRvU3RyaW5nKCksICdldGhlcicpO1xuICAgICAgbGV0IGl0ZW0gPSB7XG4gICAgICAgIHByaWNlLFxuICAgICAgICB0b2tlbklkOiBpLnRva2VuSWQudG9OdW1iZXIoKSxcbiAgICAgICAgc2VsbGVyOiBpLnNlbGxlcixcbiAgICAgICAgb3duZXI6IGkub3duZXIsXG4gICAgICAgIGltYWdlOiBtZXRhLmRhdGEuaW1hZ2UsXG4gICAgICAgIG5hbWU6IG1ldGEuZGF0YS5uYW1lLFxuICAgICAgICBkZXNjcmlwdGlvbjogbWV0YS5kYXRhLmRlc2NyaXB0aW9uLFxuICAgICAgfVxuICAgICAgcmV0dXJuIGl0ZW1cbiAgICB9KSlcbiAgICBjb25zb2xlLmxvZygnaXRlbXM6ICcsIGl0ZW1zKVxuICAgIHNldE5mdHMoaXRlbXMpXG4gICAgc2V0TG9hZGVkKCdsb2FkZWQnKVxuICB9XG4gIGlmIChsb2FkZWQgPT09ICdsb2FkZWQnICYmICFuZnRzLmxlbmd0aCkgcmV0dXJuICg8aDEgY2xhc3NOYW1lPVwicC0yMCB0ZXh0LTR4bFwiPk5vIE5GVHMhPC9oMT4pXG4gIHJldHVybiAoXG4gICAgPGRpdiBjbGFzc05hbWU9XCJmbGV4IGp1c3RpZnktY2VudGVyXCI+XG4gICAgICA8ZGl2IHN0eWxlPXt7IHdpZHRoOiA5MDAgfX0+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZ3JpZCBncmlkLWNvbHMtMiBnYXAtNCBwdC04XCI+XG4gICAgICAgICAge1xuICAgICAgICAgICAgbmZ0cy5tYXAoKG5mdCwgaSkgPT4gKFxuICAgICAgICAgICAgICA8ZGl2IGtleT17aX0gY2xhc3NOYW1lPVwiYm9yZGVyIHAtNCBzaGFkb3dcIj5cbiAgICAgICAgICAgICAgICA8aW1nIHNyYz17bmZ0LmltYWdlfSBjbGFzc05hbWU9XCJyb3VuZGVkXCIgLz5cbiAgICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJ0ZXh0LTJ4bCBteS00IGZvbnQtYm9sZFwiPlByaWNlIHBhaWQ6IHtuZnQucHJpY2V9IEV0aGVyPC9wPlxuICAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cInRleHQtMnhsIG15LTQgZm9udC1ib2xkXCI+TmFtZToge25mdC5uYW1lfTwvcD5cbiAgICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJ0ZXh0LTJ4bCBteS00IGZvbnQtYm9sZFwiPkRlc2NyaXB0aW9uOiB7bmZ0LmRlc2NyaXB0aW9ufTwvcD5cbiAgICAgICAgICAgICAgICA8YnV0dG9uIGNsYXNzTmFtZT1cInJvdW5kZWQgYmctYmx1ZS02MDAgcHktMiBweC0xMiB0ZXh0LXdoaXRlIG0tMTZcIj5TZWxsPC9idXR0b24+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgKSlcbiAgICAgICAgICB9XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gIClcbn1cbiJdLCJzb3VyY2VSb290IjoiIn0=