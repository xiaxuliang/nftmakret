self["webpackHotUpdate_N_E"]("pages/my-nfts",{

/***/ "./pages/my-nfts.js":
/*!**************************!*\
  !*** ./pages/my-nfts.js ***!
  \**************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ Home; }
/* harmony export */ });
/* harmony import */ var E_marketplace_complete_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/regenerator */ "./node_modules/next/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var E_marketplace_complete_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(E_marketplace_complete_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var E_marketplace_complete_node_modules_next_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var ethers__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ethers */ "./node_modules/ethers/lib.esm/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var web3__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! web3 */ "./node_modules/web3/lib/index.js");
/* harmony import */ var web3__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(web3__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var web3modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! web3modal */ "./node_modules/web3modal/dist/index.js");
/* harmony import */ var web3modal__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(web3modal__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../config */ "./config.js");
/* module decorator */ module = __webpack_require__.hmd(module);




var _jsxFileName = "E:\\marketplace-complete\\pages\\my-nfts.js",
    _s = $RefreshSig$();







function Home() {
  _s();

  var _this = this;

  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)([]),
      nfts = _useState[0],
      setNfts = _useState[1];

  var _useState2 = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)('not-loaded'),
      loaded = _useState2[0],
      setLoaded = _useState2[1];

  (0,react__WEBPACK_IMPORTED_MODULE_3__.useEffect)(function () {
    loadNFTs();
  }, []);

  function loadNFTs() {
    return _loadNFTs.apply(this, arguments);
  }

  function _loadNFTs() {
    _loadNFTs = (0,E_marketplace_complete_node_modules_next_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__.default)( /*#__PURE__*/E_marketplace_complete_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee2() {
      var web3Modal, connection, provider, signer, marketContract, tokenContract, data, items;
      return E_marketplace_complete_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              web3Modal = new (web3modal__WEBPACK_IMPORTED_MODULE_6___default())({
                network: "mainnet",
                cacheProvider: true
              });
              _context2.next = 3;
              return web3Modal.connect();

            case 3:
              connection = _context2.sent;
              provider = new ethers__WEBPACK_IMPORTED_MODULE_8__.ethers.providers.Web3Provider(connection);
              signer = provider.getSigner();
              marketContract = new ethers__WEBPACK_IMPORTED_MODULE_8__.ethers.Contract(_config__WEBPACK_IMPORTED_MODULE_7__.nftmarketaddress, _config__WEBPACK_IMPORTED_MODULE_7__.nftmarketabi, signer);
              tokenContract = new ethers__WEBPACK_IMPORTED_MODULE_8__.ethers.Contract(_config__WEBPACK_IMPORTED_MODULE_7__.nftaddress, _config__WEBPACK_IMPORTED_MODULE_7__.nftabi, provider);
              _context2.next = 10;
              return marketContract.fetchMyNFTs();

            case 10:
              data = _context2.sent;
              _context2.next = 13;
              return Promise.all(data.map( /*#__PURE__*/function () {
                var _ref = (0,E_marketplace_complete_node_modules_next_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__.default)( /*#__PURE__*/E_marketplace_complete_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee(i) {
                  var tokenUri, meta, price, item;
                  return E_marketplace_complete_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee$(_context) {
                    while (1) {
                      switch (_context.prev = _context.next) {
                        case 0:
                          _context.next = 2;
                          return tokenContract.tokenURI(i.tokenId);

                        case 2:
                          tokenUri = _context.sent;
                          _context.next = 5;
                          return axios__WEBPACK_IMPORTED_MODULE_5___default().get(tokenUri);

                        case 5:
                          meta = _context.sent;
                          price = web3__WEBPACK_IMPORTED_MODULE_4___default().utils.fromWei(i.price.toString(), 'ether');
                          item = {
                            price: price,
                            tokenId: i.tokenId.toNumber(),
                            seller: i.seller,
                            owner: i.owner,
                            image: meta.data.image,
                            name: meta.data.name,
                            description: meta.data.description
                          };
                          return _context.abrupt("return", item);

                        case 9:
                        case "end":
                          return _context.stop();
                      }
                    }
                  }, _callee);
                }));

                return function (_x) {
                  return _ref.apply(this, arguments);
                };
              }()));

            case 13:
              items = _context2.sent;
              console.log('items: ', items);
              setNfts(items);
              setLoaded('loaded');

            case 17:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));
    return _loadNFTs.apply(this, arguments);
  }

  if (loaded === 'loaded' && !nfts.length) return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("h1", {
    className: "p-20 text-4xl",
    children: "No NFTs!"
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 50,
    columnNumber: 52
  }, this);
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("div", {
    className: "flex justify-center",
    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("div", {
      style: {
        width: 900
      },
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("div", {
        className: "grid grid-cols-2 gap-4 pt-8",
        children: nfts.map(function (nft, i) {
          return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("div", {
            className: "border p-4 shadow",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("img", {
              src: nft.image,
              className: "rounded"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 58,
              columnNumber: 17
            }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("p", {
              className: "text-2xl my-4 font-bold",
              children: ["Price paid: ", nft.price, " Ether"]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 59,
              columnNumber: 17
            }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("p", {
              className: "text-2xl my-4 font-bold",
              children: ["Name: ", nft.name]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 60,
              columnNumber: 17
            }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("p", {
              className: "text-2xl my-4 font-bold",
              children: ["Description: ", nft.description]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 61,
              columnNumber: 17
            }, _this)]
          }, i, true, {
            fileName: _jsxFileName,
            lineNumber: 57,
            columnNumber: 15
          }, _this);
        })
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 54,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 53,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 52,
    columnNumber: 5
  }, this);
}

_s(Home, "l4Oq+AVPuMX79hZM8JqKmB+X57I=");

_c = Home;

var _c;

$RefreshReg$(_c, "Home");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.id);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }


/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvbXktbmZ0cy5qcyJdLCJuYW1lcyI6WyJIb21lIiwidXNlU3RhdGUiLCJuZnRzIiwic2V0TmZ0cyIsImxvYWRlZCIsInNldExvYWRlZCIsInVzZUVmZmVjdCIsImxvYWRORlRzIiwid2ViM01vZGFsIiwiV2ViM01vZGFsIiwibmV0d29yayIsImNhY2hlUHJvdmlkZXIiLCJjb25uZWN0IiwiY29ubmVjdGlvbiIsInByb3ZpZGVyIiwiZXRoZXJzIiwic2lnbmVyIiwiZ2V0U2lnbmVyIiwibWFya2V0Q29udHJhY3QiLCJuZnRtYXJrZXRhZGRyZXNzIiwibmZ0bWFya2V0YWJpIiwidG9rZW5Db250cmFjdCIsIm5mdGFkZHJlc3MiLCJuZnRhYmkiLCJmZXRjaE15TkZUcyIsImRhdGEiLCJQcm9taXNlIiwiYWxsIiwibWFwIiwiaSIsInRva2VuVVJJIiwidG9rZW5JZCIsInRva2VuVXJpIiwiYXhpb3MiLCJtZXRhIiwicHJpY2UiLCJ3ZWIzIiwidG9TdHJpbmciLCJpdGVtIiwidG9OdW1iZXIiLCJzZWxsZXIiLCJvd25lciIsImltYWdlIiwibmFtZSIsImRlc2NyaXB0aW9uIiwiaXRlbXMiLCJjb25zb2xlIiwibG9nIiwibGVuZ3RoIiwid2lkdGgiLCJuZnQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUdlLFNBQVNBLElBQVQsR0FBZ0I7QUFBQTs7QUFBQTs7QUFBQSxrQkFDTEMsK0NBQVEsQ0FBQyxFQUFELENBREg7QUFBQSxNQUN0QkMsSUFEc0I7QUFBQSxNQUNoQkMsT0FEZ0I7O0FBQUEsbUJBRURGLCtDQUFRLENBQUMsWUFBRCxDQUZQO0FBQUEsTUFFdEJHLE1BRnNCO0FBQUEsTUFFZEMsU0FGYzs7QUFJN0JDLGtEQUFTLENBQUMsWUFBTTtBQUNkQyxZQUFRO0FBQ1QsR0FGUSxFQUVOLEVBRk0sQ0FBVDs7QUFKNkIsV0FRZEEsUUFSYztBQUFBO0FBQUE7O0FBQUE7QUFBQSxvU0FRN0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ1FDLHVCQURSLEdBQ29CLElBQUlDLGtEQUFKLENBQWM7QUFDOUJDLHVCQUFPLEVBQUUsU0FEcUI7QUFFOUJDLDZCQUFhLEVBQUU7QUFGZSxlQUFkLENBRHBCO0FBQUE7QUFBQSxxQkFLMkJILFNBQVMsQ0FBQ0ksT0FBVixFQUwzQjs7QUFBQTtBQUtRQyx3QkFMUjtBQU1RQyxzQkFOUixHQU1tQixJQUFJQyxpRUFBSixDQUFrQ0YsVUFBbEMsQ0FObkI7QUFPUUcsb0JBUFIsR0FPaUJGLFFBQVEsQ0FBQ0csU0FBVCxFQVBqQjtBQVNRQyw0QkFUUixHQVN5QixJQUFJSCxtREFBSixDQUFvQkkscURBQXBCLEVBQXNDQyxpREFBdEMsRUFBb0RKLE1BQXBELENBVHpCO0FBVVFLLDJCQVZSLEdBVXdCLElBQUlOLG1EQUFKLENBQW9CTywrQ0FBcEIsRUFBZ0NDLDJDQUFoQyxFQUF3Q1QsUUFBeEMsQ0FWeEI7QUFBQTtBQUFBLHFCQVdxQkksY0FBYyxDQUFDTSxXQUFmLEVBWHJCOztBQUFBO0FBV1FDLGtCQVhSO0FBQUE7QUFBQSxxQkFhc0JDLE9BQU8sQ0FBQ0MsR0FBUixDQUFZRixJQUFJLENBQUNHLEdBQUw7QUFBQSwrU0FBUyxpQkFBTUMsQ0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlDQUNoQlIsYUFBYSxDQUFDUyxRQUFkLENBQXVCRCxDQUFDLENBQUNFLE9BQXpCLENBRGdCOztBQUFBO0FBQ2pDQyxrQ0FEaUM7QUFBQTtBQUFBLGlDQUVwQkMsZ0RBQUEsQ0FBVUQsUUFBVixDQUZvQjs7QUFBQTtBQUVqQ0UsOEJBRmlDO0FBR25DQywrQkFIbUMsR0FHM0JDLHlEQUFBLENBQW1CUCxDQUFDLENBQUNNLEtBQUYsQ0FBUUUsUUFBUixFQUFuQixFQUF1QyxPQUF2QyxDQUgyQjtBQUluQ0MsOEJBSm1DLEdBSTVCO0FBQ1RILGlDQUFLLEVBQUxBLEtBRFM7QUFFVEosbUNBQU8sRUFBRUYsQ0FBQyxDQUFDRSxPQUFGLENBQVVRLFFBQVYsRUFGQTtBQUdUQyxrQ0FBTSxFQUFFWCxDQUFDLENBQUNXLE1BSEQ7QUFJVEMsaUNBQUssRUFBRVosQ0FBQyxDQUFDWSxLQUpBO0FBS1RDLGlDQUFLLEVBQUVSLElBQUksQ0FBQ1QsSUFBTCxDQUFVaUIsS0FMUjtBQU1UQyxnQ0FBSSxFQUFFVCxJQUFJLENBQUNULElBQUwsQ0FBVWtCLElBTlA7QUFPVEMsdUNBQVcsRUFBRVYsSUFBSSxDQUFDVCxJQUFMLENBQVVtQjtBQVBkLDJCQUo0QjtBQUFBLDJEQWFoQ04sSUFiZ0M7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBQVQ7O0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBQVosQ0FidEI7O0FBQUE7QUFhUU8sbUJBYlI7QUE0QkVDLHFCQUFPLENBQUNDLEdBQVIsQ0FBWSxTQUFaLEVBQXVCRixLQUF2QjtBQUNBMUMscUJBQU8sQ0FBQzBDLEtBQUQsQ0FBUDtBQUNBeEMsdUJBQVMsQ0FBQyxRQUFELENBQVQ7O0FBOUJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBUjZCO0FBQUE7QUFBQTs7QUF3QzdCLE1BQUlELE1BQU0sS0FBSyxRQUFYLElBQXVCLENBQUNGLElBQUksQ0FBQzhDLE1BQWpDLEVBQXlDLG9CQUFRO0FBQUksYUFBUyxFQUFDLGVBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFBUjtBQUN6QyxzQkFDRTtBQUFLLGFBQVMsRUFBQyxxQkFBZjtBQUFBLDJCQUNFO0FBQUssV0FBSyxFQUFFO0FBQUVDLGFBQUssRUFBRTtBQUFULE9BQVo7QUFBQSw2QkFDRTtBQUFLLGlCQUFTLEVBQUMsNkJBQWY7QUFBQSxrQkFFSS9DLElBQUksQ0FBQzBCLEdBQUwsQ0FBUyxVQUFDc0IsR0FBRCxFQUFNckIsQ0FBTjtBQUFBLDhCQUNQO0FBQWEscUJBQVMsRUFBQyxtQkFBdkI7QUFBQSxvQ0FDRTtBQUFLLGlCQUFHLEVBQUVxQixHQUFHLENBQUNSLEtBQWQ7QUFBcUIsdUJBQVMsRUFBQztBQUEvQjtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQURGLGVBRUU7QUFBRyx1QkFBUyxFQUFDLHlCQUFiO0FBQUEseUNBQW9EUSxHQUFHLENBQUNmLEtBQXhEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFGRixlQUdFO0FBQUcsdUJBQVMsRUFBQyx5QkFBYjtBQUFBLG1DQUE4Q2UsR0FBRyxDQUFDUCxJQUFsRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBSEYsZUFJRTtBQUFHLHVCQUFTLEVBQUMseUJBQWI7QUFBQSwwQ0FBcURPLEdBQUcsQ0FBQ04sV0FBekQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQUpGO0FBQUEsYUFBVWYsQ0FBVjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQURPO0FBQUEsU0FBVDtBQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURGO0FBa0JEOztHQTNEdUI3QixJOztLQUFBQSxJIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL215LW5mdHMuMTJiYjJkN2E5ZTUzZmNlNWJlNWQuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGV0aGVycyB9IGZyb20gJ2V0aGVycydcbmltcG9ydCB7IHVzZUVmZmVjdCwgdXNlU3RhdGUgfSBmcm9tICdyZWFjdCdcbmltcG9ydCB3ZWIzIGZyb20gJ3dlYjMnXG5pbXBvcnQgYXhpb3MgZnJvbSAnYXhpb3MnXG5pbXBvcnQgV2ViM01vZGFsIGZyb20gXCJ3ZWIzbW9kYWxcIlxuXG5pbXBvcnQge1xuICBuZnRtYXJrZXRhZGRyZXNzLCBuZnRhZGRyZXNzLCBuZnRhYmksIG5mdG1hcmtldGFiaVxufSBmcm9tICcuLi9jb25maWcnXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBIb21lKCkge1xuICBjb25zdCBbbmZ0cywgc2V0TmZ0c10gPSB1c2VTdGF0ZShbXSlcbiAgY29uc3QgW2xvYWRlZCwgc2V0TG9hZGVkXSA9IHVzZVN0YXRlKCdub3QtbG9hZGVkJylcblxuICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgIGxvYWRORlRzKClcbiAgfSwgW10pXG5cbiAgYXN5bmMgZnVuY3Rpb24gbG9hZE5GVHMoKSB7XG4gICAgY29uc3Qgd2ViM01vZGFsID0gbmV3IFdlYjNNb2RhbCh7XG4gICAgICBuZXR3b3JrOiBcIm1haW5uZXRcIixcbiAgICAgIGNhY2hlUHJvdmlkZXI6IHRydWUsXG4gICAgfSk7XG4gICAgY29uc3QgY29ubmVjdGlvbiA9IGF3YWl0IHdlYjNNb2RhbC5jb25uZWN0KClcbiAgICBjb25zdCBwcm92aWRlciA9IG5ldyBldGhlcnMucHJvdmlkZXJzLldlYjNQcm92aWRlcihjb25uZWN0aW9uKVxuICAgIGNvbnN0IHNpZ25lciA9IHByb3ZpZGVyLmdldFNpZ25lcigpXG5cbiAgICBjb25zdCBtYXJrZXRDb250cmFjdCA9IG5ldyBldGhlcnMuQ29udHJhY3QobmZ0bWFya2V0YWRkcmVzcywgbmZ0bWFya2V0YWJpLCBzaWduZXIpXG4gICAgY29uc3QgdG9rZW5Db250cmFjdCA9IG5ldyBldGhlcnMuQ29udHJhY3QobmZ0YWRkcmVzcywgbmZ0YWJpLCBwcm92aWRlcilcbiAgICBjb25zdCBkYXRhID0gYXdhaXQgbWFya2V0Q29udHJhY3QuZmV0Y2hNeU5GVHMoKVxuXG4gICAgY29uc3QgaXRlbXMgPSBhd2FpdCBQcm9taXNlLmFsbChkYXRhLm1hcChhc3luYyBpID0+IHtcbiAgICAgIGNvbnN0IHRva2VuVXJpID0gYXdhaXQgdG9rZW5Db250cmFjdC50b2tlblVSSShpLnRva2VuSWQpXG4gICAgICBjb25zdCBtZXRhID0gYXdhaXQgYXhpb3MuZ2V0KHRva2VuVXJpKVxuICAgICAgbGV0IHByaWNlID0gd2ViMy51dGlscy5mcm9tV2VpKGkucHJpY2UudG9TdHJpbmcoKSwgJ2V0aGVyJyk7XG4gICAgICBsZXQgaXRlbSA9IHtcbiAgICAgICAgcHJpY2UsXG4gICAgICAgIHRva2VuSWQ6IGkudG9rZW5JZC50b051bWJlcigpLFxuICAgICAgICBzZWxsZXI6IGkuc2VsbGVyLFxuICAgICAgICBvd25lcjogaS5vd25lcixcbiAgICAgICAgaW1hZ2U6IG1ldGEuZGF0YS5pbWFnZSxcbiAgICAgICAgbmFtZTogbWV0YS5kYXRhLm5hbWUsXG4gICAgICAgIGRlc2NyaXB0aW9uOiBtZXRhLmRhdGEuZGVzY3JpcHRpb24sXG4gICAgICB9XG4gICAgICByZXR1cm4gaXRlbVxuICAgIH0pKVxuICAgIGNvbnNvbGUubG9nKCdpdGVtczogJywgaXRlbXMpXG4gICAgc2V0TmZ0cyhpdGVtcylcbiAgICBzZXRMb2FkZWQoJ2xvYWRlZCcpXG4gIH1cbiAgaWYgKGxvYWRlZCA9PT0gJ2xvYWRlZCcgJiYgIW5mdHMubGVuZ3RoKSByZXR1cm4gKDxoMSBjbGFzc05hbWU9XCJwLTIwIHRleHQtNHhsXCI+Tm8gTkZUcyE8L2gxPilcbiAgcmV0dXJuIChcbiAgICA8ZGl2IGNsYXNzTmFtZT1cImZsZXgganVzdGlmeS1jZW50ZXJcIj5cbiAgICAgIDxkaXYgc3R5bGU9e3sgd2lkdGg6IDkwMCB9fT5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJncmlkIGdyaWQtY29scy0yIGdhcC00IHB0LThcIj5cbiAgICAgICAgICB7XG4gICAgICAgICAgICBuZnRzLm1hcCgobmZ0LCBpKSA9PiAoXG4gICAgICAgICAgICAgIDxkaXYga2V5PXtpfSBjbGFzc05hbWU9XCJib3JkZXIgcC00IHNoYWRvd1wiPlxuICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtuZnQuaW1hZ2V9IGNsYXNzTmFtZT1cInJvdW5kZWRcIiAvPlxuICAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cInRleHQtMnhsIG15LTQgZm9udC1ib2xkXCI+UHJpY2UgcGFpZDoge25mdC5wcmljZX0gRXRoZXI8L3A+XG4gICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwidGV4dC0yeGwgbXktNCBmb250LWJvbGRcIj5OYW1lOiB7bmZ0Lm5hbWV9PC9wPlxuICAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cInRleHQtMnhsIG15LTQgZm9udC1ib2xkXCI+RGVzY3JpcHRpb246IHtuZnQuZGVzY3JpcHRpb259PC9wPlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICkpXG4gICAgICAgICAgfVxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApXG59XG4iXSwic291cmNlUm9vdCI6IiJ9