self["webpackHotUpdate_N_E"]("pages/index",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ Home; }
/* harmony export */ });
/* harmony import */ var E_marketplace_complete_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/regenerator */ "./node_modules/next/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var E_marketplace_complete_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(E_marketplace_complete_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var E_marketplace_complete_node_modules_next_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var ethers__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ethers */ "./node_modules/ethers/lib.esm/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var web3__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! web3 */ "./node_modules/web3/lib/index.js");
/* harmony import */ var web3__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(web3__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var web3modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! web3modal */ "./node_modules/web3modal/dist/index.js");
/* harmony import */ var web3modal__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(web3modal__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var fortmatic__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! fortmatic */ "./node_modules/fortmatic/dist/cjs/fortmatic.js");
/* harmony import */ var fortmatic__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(fortmatic__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _walletconnect_web3_provider__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @walletconnect/web3-provider */ "./node_modules/@walletconnect/web3-provider/dist/cjs/index.js");
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../config */ "./config.js");
/* module decorator */ module = __webpack_require__.hmd(module);




var _jsxFileName = "E:\\marketplace-complete\\pages\\index.js",
    _s = $RefreshSig$();









function Home() {
  _s();

  var _this = this;

  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)([]),
      nfts = _useState[0],
      setNfts = _useState[1];

  var _useState2 = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)('not-loaded'),
      loaded = _useState2[0],
      setLoaded = _useState2[1];

  (0,react__WEBPACK_IMPORTED_MODULE_3__.useEffect)(function () {
    loadNFTs();
  }, []);

  function loadNFTs() {
    return _loadNFTs.apply(this, arguments);
  }

  function _loadNFTs() {
    _loadNFTs = (0,E_marketplace_complete_node_modules_next_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__.default)( /*#__PURE__*/E_marketplace_complete_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee2() {
      var providerOptions, web3Modal, connection, provider, tokenContract, marketContract, data, items;
      return E_marketplace_complete_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              providerOptions = {
                fortmatic: {
                  "package": (fortmatic__WEBPACK_IMPORTED_MODULE_7___default()),
                  options: {
                    // Mikko's TESTNET api key
                    key: "pk_test_391E26A3B43A3350"
                  }
                },
                walletconnect: {
                  "package": _walletconnect_web3_provider__WEBPACK_IMPORTED_MODULE_8__.default,
                  // required
                  options: {
                    infuraId: "INFURA_ID" // required

                  }
                }
              };
              web3Modal = new (web3modal__WEBPACK_IMPORTED_MODULE_6___default())({
                network: "mainnet",
                cacheProvider: true,
                disableInjectedProvider: false,
                providerOptions: providerOptions // required

              });
              _context2.next = 4;
              return web3Modal.connect();

            case 4:
              connection = _context2.sent;
              provider = new ethers__WEBPACK_IMPORTED_MODULE_10__.ethers.providers.Web3Provider(connection);
              tokenContract = new ethers__WEBPACK_IMPORTED_MODULE_10__.ethers.Contract(_config__WEBPACK_IMPORTED_MODULE_9__.nftaddress, _config__WEBPACK_IMPORTED_MODULE_9__.nftabi, provider);
              marketContract = new ethers__WEBPACK_IMPORTED_MODULE_10__.ethers.Contract(_config__WEBPACK_IMPORTED_MODULE_9__.nftmarketaddress, _config__WEBPACK_IMPORTED_MODULE_9__.nftmarketabi, provider);
              _context2.next = 10;
              return marketContract.fetchMarketItems();

            case 10:
              data = _context2.sent;
              _context2.next = 13;
              return Promise.all(data.map( /*#__PURE__*/function () {
                var _ref = (0,E_marketplace_complete_node_modules_next_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__.default)( /*#__PURE__*/E_marketplace_complete_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee(i) {
                  var tokenUri, meta, price, item;
                  return E_marketplace_complete_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee$(_context) {
                    while (1) {
                      switch (_context.prev = _context.next) {
                        case 0:
                          _context.next = 2;
                          return tokenContract.tokenURI(i.tokenId);

                        case 2:
                          tokenUri = _context.sent;
                          _context.next = 5;
                          return axios__WEBPACK_IMPORTED_MODULE_4___default().get(tokenUri);

                        case 5:
                          meta = _context.sent;
                          price = web3__WEBPACK_IMPORTED_MODULE_5___default().utils.fromWei(i.price.toString(), 'ether');
                          item = {
                            price: price,
                            tokenId: i.tokenId.toNumber(),
                            seller: i.seller,
                            owner: i.owner,
                            image: meta.data.image,
                            name: meta.data.name,
                            description: meta.data.description
                          };
                          return _context.abrupt("return", item);

                        case 9:
                        case "end":
                          return _context.stop();
                      }
                    }
                  }, _callee);
                }));

                return function (_x2) {
                  return _ref.apply(this, arguments);
                };
              }()));

            case 13:
              items = _context2.sent;
              console.log('items: ', items);
              setNfts(items);
              setLoaded('loaded');

            case 17:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));
    return _loadNFTs.apply(this, arguments);
  }

  function buyNft(_x) {
    return _buyNft.apply(this, arguments);
  }

  function _buyNft() {
    _buyNft = (0,E_marketplace_complete_node_modules_next_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__.default)( /*#__PURE__*/E_marketplace_complete_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee3(nft) {
      var web3Modal, connection, provider, signer, contract, price, transaction;
      return E_marketplace_complete_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              web3Modal = new (web3modal__WEBPACK_IMPORTED_MODULE_6___default())({
                network: "mainnet",
                cacheProvider: true
              });
              _context3.next = 3;
              return web3Modal.connect();

            case 3:
              connection = _context3.sent;
              provider = new ethers__WEBPACK_IMPORTED_MODULE_10__.ethers.providers.Web3Provider(connection);
              signer = provider.getSigner();
              contract = new ethers__WEBPACK_IMPORTED_MODULE_10__.ethers.Contract(_config__WEBPACK_IMPORTED_MODULE_9__.nftmarketaddress, _config__WEBPACK_IMPORTED_MODULE_9__.nftmarketabi, signer);
              price = web3__WEBPACK_IMPORTED_MODULE_5___default().utils.toWei(nft.price.toString(), 'ether');
              console.log('price: ', price);
              _context3.next = 11;
              return contract.createMarketSale(_config__WEBPACK_IMPORTED_MODULE_9__.nftaddress, nft.tokenId, {
                value: price
              });

            case 11:
              transaction = _context3.sent;
              _context3.next = 14;
              return transaction.wait();

            case 14:
              loadNFTs();

            case 15:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3);
    }));
    return _buyNft.apply(this, arguments);
  }

  if (loaded === 'loaded' && !nfts.length) return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("h1", {
    className: "p-20 text-4xl",
    children: "No NFTs!"
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 90,
    columnNumber: 52
  }, this);
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("div", {
    className: "flex justify-center",
    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("div", {
      style: {
        width: 900
      },
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("div", {
        className: "grid grid-cols-2 gap-4 pt-8",
        children: nfts.map(function (nft, i) {
          return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("div", {
            className: "border p-4 shadow",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("img", {
              src: nft.image,
              className: "rounded"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 98,
              columnNumber: 17
            }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("p", {
              className: "text-2xl my-4 font-bold",
              children: ["Price: ", nft.price]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 99,
              columnNumber: 17
            }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("p", {
              className: "text-2xl my-4 font-bold",
              children: ["Name: ", nft.name]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 100,
              columnNumber: 17
            }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("p", {
              className: "text-2xl my-4 font-bold",
              children: ["Description: ", nft.description]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 101,
              columnNumber: 17
            }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxDEV)("button", {
              className: "bg-green-600 text-white py-2 px-12 rounded",
              onClick: function onClick() {
                return buyNft(nft);
              },
              children: "Buy NFT"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 102,
              columnNumber: 17
            }, _this)]
          }, i, true, {
            fileName: _jsxFileName,
            lineNumber: 97,
            columnNumber: 15
          }, _this);
        })
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 94,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 93,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 92,
    columnNumber: 5
  }, this);
}

_s(Home, "l4Oq+AVPuMX79hZM8JqKmB+X57I=");

_c = Home;

var _c;

$RefreshReg$(_c, "Home");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.id);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }


/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvaW5kZXguanMiXSwibmFtZXMiOlsiSG9tZSIsInVzZVN0YXRlIiwibmZ0cyIsInNldE5mdHMiLCJsb2FkZWQiLCJzZXRMb2FkZWQiLCJ1c2VFZmZlY3QiLCJsb2FkTkZUcyIsInByb3ZpZGVyT3B0aW9ucyIsImZvcnRtYXRpYyIsIkZvcnRtYXRpYyIsIm9wdGlvbnMiLCJrZXkiLCJ3YWxsZXRjb25uZWN0IiwiV2FsbGV0Q29ubmVjdFByb3ZpZGVyIiwiaW5mdXJhSWQiLCJ3ZWIzTW9kYWwiLCJXZWIzTW9kYWwiLCJuZXR3b3JrIiwiY2FjaGVQcm92aWRlciIsImRpc2FibGVJbmplY3RlZFByb3ZpZGVyIiwiY29ubmVjdCIsImNvbm5lY3Rpb24iLCJwcm92aWRlciIsImV0aGVycyIsInRva2VuQ29udHJhY3QiLCJuZnRhZGRyZXNzIiwibmZ0YWJpIiwibWFya2V0Q29udHJhY3QiLCJuZnRtYXJrZXRhZGRyZXNzIiwibmZ0bWFya2V0YWJpIiwiZmV0Y2hNYXJrZXRJdGVtcyIsImRhdGEiLCJQcm9taXNlIiwiYWxsIiwibWFwIiwiaSIsInRva2VuVVJJIiwidG9rZW5JZCIsInRva2VuVXJpIiwiYXhpb3MiLCJtZXRhIiwicHJpY2UiLCJ3ZWIzIiwidG9TdHJpbmciLCJpdGVtIiwidG9OdW1iZXIiLCJzZWxsZXIiLCJvd25lciIsImltYWdlIiwibmFtZSIsImRlc2NyaXB0aW9uIiwiaXRlbXMiLCJjb25zb2xlIiwibG9nIiwiYnV5TmZ0IiwibmZ0Iiwic2lnbmVyIiwiZ2V0U2lnbmVyIiwiY29udHJhY3QiLCJjcmVhdGVNYXJrZXRTYWxlIiwidmFsdWUiLCJ0cmFuc2FjdGlvbiIsIndhaXQiLCJsZW5ndGgiLCJ3aWR0aCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQU1lLFNBQVNBLElBQVQsR0FBZ0I7QUFBQTs7QUFBQTs7QUFBQSxrQkFDTEMsK0NBQVEsQ0FBQyxFQUFELENBREg7QUFBQSxNQUN0QkMsSUFEc0I7QUFBQSxNQUNoQkMsT0FEZ0I7O0FBQUEsbUJBRURGLCtDQUFRLENBQUMsWUFBRCxDQUZQO0FBQUEsTUFFdEJHLE1BRnNCO0FBQUEsTUFFZEMsU0FGYzs7QUFHN0JDLGtEQUFTLENBQUMsWUFBTTtBQUNkQyxZQUFRO0FBQ1QsR0FGUSxFQUVOLEVBRk0sQ0FBVDs7QUFINkIsV0FNZEEsUUFOYztBQUFBO0FBQUE7O0FBQUE7QUFBQSxvU0FNN0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ1FDLDZCQURSLEdBQzBCO0FBQ3RCQyx5QkFBUyxFQUFFO0FBQ1QsNkJBQVNDLGtEQURBO0FBRVRDLHlCQUFPLEVBQUU7QUFDUDtBQUNBQyx1QkFBRyxFQUFFO0FBRkU7QUFGQSxpQkFEVztBQVF0QkMsNkJBQWEsRUFBRTtBQUNiLDZCQUFTQyxpRUFESTtBQUNtQjtBQUNoQ0gseUJBQU8sRUFBRTtBQUNQSSw0QkFBUSxFQUFFLFdBREgsQ0FDZTs7QUFEZjtBQUZJO0FBUk8sZUFEMUI7QUFnQlFDLHVCQWhCUixHQWdCb0IsSUFBSUMsa0RBQUosQ0FBYztBQUM5QkMsdUJBQU8sRUFBRSxTQURxQjtBQUU5QkMsNkJBQWEsRUFBRSxJQUZlO0FBRzlCQyx1Q0FBdUIsRUFBRSxLQUhLO0FBSTlCWiwrQkFBZSxFQUFFQSxlQUphLENBSUk7O0FBSkosZUFBZCxDQWhCcEI7QUFBQTtBQUFBLHFCQXNCMkJRLFNBQVMsQ0FBQ0ssT0FBVixFQXRCM0I7O0FBQUE7QUFzQlFDLHdCQXRCUjtBQXVCUUMsc0JBdkJSLEdBdUJtQixJQUFJQyxrRUFBSixDQUFrQ0YsVUFBbEMsQ0F2Qm5CO0FBd0JRRywyQkF4QlIsR0F3QndCLElBQUlELG9EQUFKLENBQW9CRSwrQ0FBcEIsRUFBZ0NDLDJDQUFoQyxFQUF3Q0osUUFBeEMsQ0F4QnhCO0FBeUJRSyw0QkF6QlIsR0F5QnlCLElBQUlKLG9EQUFKLENBQW9CSyxxREFBcEIsRUFBc0NDLGlEQUF0QyxFQUFvRFAsUUFBcEQsQ0F6QnpCO0FBQUE7QUFBQSxxQkEwQnFCSyxjQUFjLENBQUNHLGdCQUFmLEVBMUJyQjs7QUFBQTtBQTBCUUMsa0JBMUJSO0FBQUE7QUFBQSxxQkE0QnNCQyxPQUFPLENBQUNDLEdBQVIsQ0FBWUYsSUFBSSxDQUFDRyxHQUFMO0FBQUEsK1NBQVMsaUJBQU1DLENBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQ0FDaEJYLGFBQWEsQ0FBQ1ksUUFBZCxDQUF1QkQsQ0FBQyxDQUFDRSxPQUF6QixDQURnQjs7QUFBQTtBQUNqQ0Msa0NBRGlDO0FBQUE7QUFBQSxpQ0FFcEJDLGdEQUFBLENBQVVELFFBQVYsQ0FGb0I7O0FBQUE7QUFFakNFLDhCQUZpQztBQUduQ0MsK0JBSG1DLEdBRzNCQyx5REFBQSxDQUFtQlAsQ0FBQyxDQUFDTSxLQUFGLENBQVFFLFFBQVIsRUFBbkIsRUFBdUMsT0FBdkMsQ0FIMkI7QUFJbkNDLDhCQUptQyxHQUk1QjtBQUNUSCxpQ0FBSyxFQUFMQSxLQURTO0FBRVRKLG1DQUFPLEVBQUVGLENBQUMsQ0FBQ0UsT0FBRixDQUFVUSxRQUFWLEVBRkE7QUFHVEMsa0NBQU0sRUFBRVgsQ0FBQyxDQUFDVyxNQUhEO0FBSVRDLGlDQUFLLEVBQUVaLENBQUMsQ0FBQ1ksS0FKQTtBQUtUQyxpQ0FBSyxFQUFFUixJQUFJLENBQUNULElBQUwsQ0FBVWlCLEtBTFI7QUFNVEMsZ0NBQUksRUFBRVQsSUFBSSxDQUFDVCxJQUFMLENBQVVrQixJQU5QO0FBT1RDLHVDQUFXLEVBQUVWLElBQUksQ0FBQ1QsSUFBTCxDQUFVbUI7QUFQZCwyQkFKNEI7QUFBQSwyREFhaENOLElBYmdDOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUFUOztBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUFaLENBNUJ0Qjs7QUFBQTtBQTRCUU8sbUJBNUJSO0FBMkNFQyxxQkFBTyxDQUFDQyxHQUFSLENBQVksU0FBWixFQUF1QkYsS0FBdkI7QUFDQWpELHFCQUFPLENBQUNpRCxLQUFELENBQVA7QUFDQS9DLHVCQUFTLENBQUMsUUFBRCxDQUFUOztBQTdDRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQU42QjtBQUFBO0FBQUE7O0FBQUEsV0FxRGRrRCxNQXJEYztBQUFBO0FBQUE7O0FBQUE7QUFBQSxrU0FxRDdCLGtCQUFzQkMsR0FBdEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ1F4Qyx1QkFEUixHQUNvQixJQUFJQyxrREFBSixDQUFjO0FBQzlCQyx1QkFBTyxFQUFFLFNBRHFCO0FBRTlCQyw2QkFBYSxFQUFFO0FBRmUsZUFBZCxDQURwQjtBQUFBO0FBQUEscUJBSzJCSCxTQUFTLENBQUNLLE9BQVYsRUFMM0I7O0FBQUE7QUFLUUMsd0JBTFI7QUFNUUMsc0JBTlIsR0FNbUIsSUFBSUMsa0VBQUosQ0FBa0NGLFVBQWxDLENBTm5CO0FBT1FtQyxvQkFQUixHQU9pQmxDLFFBQVEsQ0FBQ21DLFNBQVQsRUFQakI7QUFRUUMsc0JBUlIsR0FRbUIsSUFBSW5DLG9EQUFKLENBQW9CSyxxREFBcEIsRUFBc0NDLGlEQUF0QyxFQUFvRDJCLE1BQXBELENBUm5CO0FBVVFmLG1CQVZSLEdBVWdCQyx1REFBQSxDQUFpQmEsR0FBRyxDQUFDZCxLQUFKLENBQVVFLFFBQVYsRUFBakIsRUFBdUMsT0FBdkMsQ0FWaEI7QUFZRVMscUJBQU8sQ0FBQ0MsR0FBUixDQUFZLFNBQVosRUFBdUJaLEtBQXZCO0FBWkY7QUFBQSxxQkFjNEJpQixRQUFRLENBQUNDLGdCQUFULENBQTBCbEMsK0NBQTFCLEVBQXNDOEIsR0FBRyxDQUFDbEIsT0FBMUMsRUFBbUQ7QUFDM0V1QixxQkFBSyxFQUFFbkI7QUFEb0UsZUFBbkQsQ0FkNUI7O0FBQUE7QUFjUW9CLHlCQWRSO0FBQUE7QUFBQSxxQkFpQlFBLFdBQVcsQ0FBQ0MsSUFBWixFQWpCUjs7QUFBQTtBQWtCRXhELHNCQUFROztBQWxCVjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQXJENkI7QUFBQTtBQUFBOztBQTBFN0IsTUFBSUgsTUFBTSxLQUFLLFFBQVgsSUFBdUIsQ0FBQ0YsSUFBSSxDQUFDOEQsTUFBakMsRUFBeUMsb0JBQVE7QUFBSSxhQUFTLEVBQUMsZUFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQUFSO0FBQ3pDLHNCQUNFO0FBQUssYUFBUyxFQUFDLHFCQUFmO0FBQUEsMkJBQ0U7QUFBSyxXQUFLLEVBQUU7QUFBRUMsYUFBSyxFQUFFO0FBQVQsT0FBWjtBQUFBLDZCQUNFO0FBQUssaUJBQVMsRUFBQyw2QkFBZjtBQUFBLGtCQUVJL0QsSUFBSSxDQUFDaUMsR0FBTCxDQUFTLFVBQUNxQixHQUFELEVBQU1wQixDQUFOO0FBQUEsOEJBQ1A7QUFBYSxxQkFBUyxFQUFDLG1CQUF2QjtBQUFBLG9DQUNFO0FBQUssaUJBQUcsRUFBRW9CLEdBQUcsQ0FBQ1AsS0FBZDtBQUFxQix1QkFBUyxFQUFDO0FBQS9CO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBREYsZUFFRTtBQUFHLHVCQUFTLEVBQUMseUJBQWI7QUFBQSxvQ0FBK0NPLEdBQUcsQ0FBQ2QsS0FBbkQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQUZGLGVBR0U7QUFBRyx1QkFBUyxFQUFDLHlCQUFiO0FBQUEsbUNBQThDYyxHQUFHLENBQUNOLElBQWxEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFIRixlQUlFO0FBQUcsdUJBQVMsRUFBQyx5QkFBYjtBQUFBLDBDQUFxRE0sR0FBRyxDQUFDTCxXQUF6RDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBSkYsZUFLRTtBQUFRLHVCQUFTLEVBQUMsNENBQWxCO0FBQStELHFCQUFPLEVBQUU7QUFBQSx1QkFBTUksTUFBTSxDQUFDQyxHQUFELENBQVo7QUFBQSxlQUF4RTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFMRjtBQUFBLGFBQVVwQixDQUFWO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBRE87QUFBQSxTQUFUO0FBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREY7QUFtQkQ7O0dBOUZ1QnBDLEk7O0tBQUFBLEkiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvaW5kZXguZjI2ZDE1N2FmODRjZjhjYzc5MWQuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGV0aGVycyB9IGZyb20gJ2V0aGVycydcbmltcG9ydCB7IHVzZUVmZmVjdCwgdXNlU3RhdGUgfSBmcm9tICdyZWFjdCdcbmltcG9ydCBheGlvcyBmcm9tICdheGlvcydcblxuaW1wb3J0IHdlYjMgZnJvbSAnd2ViMydcbmltcG9ydCBXZWIzTW9kYWwgZnJvbSBcIndlYjNtb2RhbFwiXG5pbXBvcnQgRm9ydG1hdGljIGZyb20gXCJmb3J0bWF0aWNcIjtcbmltcG9ydCBXYWxsZXRDb25uZWN0UHJvdmlkZXIgZnJvbSBcIkB3YWxsZXRjb25uZWN0L3dlYjMtcHJvdmlkZXJcIjtcblxuaW1wb3J0IHtcbiAgbmZ0bWFya2V0YWRkcmVzcywgbmZ0YWRkcmVzcywgbmZ0YWJpLCBuZnRtYXJrZXRhYmlcbn0gZnJvbSAnLi4vY29uZmlnJ1xuXG5cblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gSG9tZSgpIHtcbiAgY29uc3QgW25mdHMsIHNldE5mdHNdID0gdXNlU3RhdGUoW10pXG4gIGNvbnN0IFtsb2FkZWQsIHNldExvYWRlZF0gPSB1c2VTdGF0ZSgnbm90LWxvYWRlZCcpXG4gIHVzZUVmZmVjdCgoKSA9PiB7XG4gICAgbG9hZE5GVHMoKVxuICB9LCBbXSlcbiAgYXN5bmMgZnVuY3Rpb24gbG9hZE5GVHMoKSB7XG4gICAgY29uc3QgcHJvdmlkZXJPcHRpb25zID0ge1xuICAgICAgZm9ydG1hdGljOiB7XG4gICAgICAgIHBhY2thZ2U6IEZvcnRtYXRpYyxcbiAgICAgICAgb3B0aW9uczoge1xuICAgICAgICAgIC8vIE1pa2tvJ3MgVEVTVE5FVCBhcGkga2V5XG4gICAgICAgICAga2V5OiBcInBrX3Rlc3RfMzkxRTI2QTNCNDNBMzM1MFwiXG4gICAgICAgIH1cbiAgICAgIH0sXG4gICAgICB3YWxsZXRjb25uZWN0OiB7XG4gICAgICAgIHBhY2thZ2U6IFdhbGxldENvbm5lY3RQcm92aWRlciwgLy8gcmVxdWlyZWRcbiAgICAgICAgb3B0aW9uczoge1xuICAgICAgICAgIGluZnVyYUlkOiBcIklORlVSQV9JRFwiIC8vIHJlcXVpcmVkXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9O1xuICAgIGNvbnN0IHdlYjNNb2RhbCA9IG5ldyBXZWIzTW9kYWwoe1xuICAgICAgbmV0d29yazogXCJtYWlubmV0XCIsXG4gICAgICBjYWNoZVByb3ZpZGVyOiB0cnVlLFxuICAgICAgZGlzYWJsZUluamVjdGVkUHJvdmlkZXI6IGZhbHNlLFxuICAgICAgcHJvdmlkZXJPcHRpb25zOiBwcm92aWRlck9wdGlvbnMsIC8vIHJlcXVpcmVkXG4gICAgfSk7XG4gICAgY29uc3QgY29ubmVjdGlvbiA9IGF3YWl0IHdlYjNNb2RhbC5jb25uZWN0KClcbiAgICBjb25zdCBwcm92aWRlciA9IG5ldyBldGhlcnMucHJvdmlkZXJzLldlYjNQcm92aWRlcihjb25uZWN0aW9uKVxuICAgIGNvbnN0IHRva2VuQ29udHJhY3QgPSBuZXcgZXRoZXJzLkNvbnRyYWN0KG5mdGFkZHJlc3MsIG5mdGFiaSwgcHJvdmlkZXIpXG4gICAgY29uc3QgbWFya2V0Q29udHJhY3QgPSBuZXcgZXRoZXJzLkNvbnRyYWN0KG5mdG1hcmtldGFkZHJlc3MsIG5mdG1hcmtldGFiaSwgcHJvdmlkZXIpXG4gICAgY29uc3QgZGF0YSA9IGF3YWl0IG1hcmtldENvbnRyYWN0LmZldGNoTWFya2V0SXRlbXMoKVxuXG4gICAgY29uc3QgaXRlbXMgPSBhd2FpdCBQcm9taXNlLmFsbChkYXRhLm1hcChhc3luYyBpID0+IHtcbiAgICAgIGNvbnN0IHRva2VuVXJpID0gYXdhaXQgdG9rZW5Db250cmFjdC50b2tlblVSSShpLnRva2VuSWQpXG4gICAgICBjb25zdCBtZXRhID0gYXdhaXQgYXhpb3MuZ2V0KHRva2VuVXJpKVxuICAgICAgbGV0IHByaWNlID0gd2ViMy51dGlscy5mcm9tV2VpKGkucHJpY2UudG9TdHJpbmcoKSwgJ2V0aGVyJyk7XG4gICAgICBsZXQgaXRlbSA9IHtcbiAgICAgICAgcHJpY2UsXG4gICAgICAgIHRva2VuSWQ6IGkudG9rZW5JZC50b051bWJlcigpLFxuICAgICAgICBzZWxsZXI6IGkuc2VsbGVyLFxuICAgICAgICBvd25lcjogaS5vd25lcixcbiAgICAgICAgaW1hZ2U6IG1ldGEuZGF0YS5pbWFnZSxcbiAgICAgICAgbmFtZTogbWV0YS5kYXRhLm5hbWUsXG4gICAgICAgIGRlc2NyaXB0aW9uOiBtZXRhLmRhdGEuZGVzY3JpcHRpb24sXG4gICAgICB9XG4gICAgICByZXR1cm4gaXRlbVxuICAgIH0pKVxuICAgIGNvbnNvbGUubG9nKCdpdGVtczogJywgaXRlbXMpXG4gICAgc2V0TmZ0cyhpdGVtcylcbiAgICBzZXRMb2FkZWQoJ2xvYWRlZCcpXG4gIH1cbiAgYXN5bmMgZnVuY3Rpb24gYnV5TmZ0KG5mdCkge1xuICAgIGNvbnN0IHdlYjNNb2RhbCA9IG5ldyBXZWIzTW9kYWwoe1xuICAgICAgbmV0d29yazogXCJtYWlubmV0XCIsXG4gICAgICBjYWNoZVByb3ZpZGVyOiB0cnVlLFxuICAgIH0pO1xuICAgIGNvbnN0IGNvbm5lY3Rpb24gPSBhd2FpdCB3ZWIzTW9kYWwuY29ubmVjdCgpXG4gICAgY29uc3QgcHJvdmlkZXIgPSBuZXcgZXRoZXJzLnByb3ZpZGVycy5XZWIzUHJvdmlkZXIoY29ubmVjdGlvbilcbiAgICBjb25zdCBzaWduZXIgPSBwcm92aWRlci5nZXRTaWduZXIoKVxuICAgIGNvbnN0IGNvbnRyYWN0ID0gbmV3IGV0aGVycy5Db250cmFjdChuZnRtYXJrZXRhZGRyZXNzLCBuZnRtYXJrZXRhYmksIHNpZ25lcilcblxuICAgIGNvbnN0IHByaWNlID0gd2ViMy51dGlscy50b1dlaShuZnQucHJpY2UudG9TdHJpbmcoKSwgJ2V0aGVyJyk7XG5cbiAgICBjb25zb2xlLmxvZygncHJpY2U6ICcsIHByaWNlKTtcblxuICAgIGNvbnN0IHRyYW5zYWN0aW9uID0gYXdhaXQgY29udHJhY3QuY3JlYXRlTWFya2V0U2FsZShuZnRhZGRyZXNzLCBuZnQudG9rZW5JZCwge1xuICAgICAgdmFsdWU6IHByaWNlXG4gICAgfSlcbiAgICBhd2FpdCB0cmFuc2FjdGlvbi53YWl0KClcbiAgICBsb2FkTkZUcygpXG4gIH1cblxuICBpZiAobG9hZGVkID09PSAnbG9hZGVkJyAmJiAhbmZ0cy5sZW5ndGgpIHJldHVybiAoPGgxIGNsYXNzTmFtZT1cInAtMjAgdGV4dC00eGxcIj5ObyBORlRzITwvaDE+KVxuICByZXR1cm4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPVwiZmxleCBqdXN0aWZ5LWNlbnRlclwiPlxuICAgICAgPGRpdiBzdHlsZT17eyB3aWR0aDogOTAwIH19PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImdyaWQgZ3JpZC1jb2xzLTIgZ2FwLTQgcHQtOFwiPlxuICAgICAgICAgIHtcbiAgICAgICAgICAgIG5mdHMubWFwKChuZnQsIGkpID0+IChcbiAgICAgICAgICAgICAgPGRpdiBrZXk9e2l9IGNsYXNzTmFtZT1cImJvcmRlciBwLTQgc2hhZG93XCI+XG4gICAgICAgICAgICAgICAgPGltZyBzcmM9e25mdC5pbWFnZX0gY2xhc3NOYW1lPVwicm91bmRlZFwiIC8+XG4gICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwidGV4dC0yeGwgbXktNCBmb250LWJvbGRcIj5QcmljZToge25mdC5wcmljZX08L3A+XG4gICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwidGV4dC0yeGwgbXktNCBmb250LWJvbGRcIj5OYW1lOiB7bmZ0Lm5hbWV9PC9wPlxuICAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cInRleHQtMnhsIG15LTQgZm9udC1ib2xkXCI+RGVzY3JpcHRpb246IHtuZnQuZGVzY3JpcHRpb259PC9wPlxuICAgICAgICAgICAgICAgIDxidXR0b24gY2xhc3NOYW1lPVwiYmctZ3JlZW4tNjAwIHRleHQtd2hpdGUgcHktMiBweC0xMiByb3VuZGVkXCIgb25DbGljaz17KCkgPT4gYnV5TmZ0KG5mdCl9PkJ1eSBORlQ8L2J1dHRvbj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICApKVxuICAgICAgICAgIH1cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKVxufVxuIl0sInNvdXJjZVJvb3QiOiIifQ==